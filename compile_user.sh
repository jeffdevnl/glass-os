#!/bin/bash

echo "Building crtbegin..."
nasm user/crtbegin.asm -f elf32 -o user/crtbegin.o

echo "Compiling..."
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/test.o user/test.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/cat.o user/cat.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/serial.o user/serial.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/hexview.o user/hexview.c

echo "Compiling GUI..."
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/gui.o user/gui/gui.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/graphics.o user/gui/graphics.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/window.o user/gui/window.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/window_drawing.o user/gui/window_drawing.c
gcc -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/list.o user/gui/list.c

echo "Linking..."
ld -m elf_i386 -T user/link.ld -o user/test.elf user/test.o
ld -m elf_i386 -T user/link.ld -o user/cat user/cat.o
ld -m elf_i386 -T user/link.ld -o user/serialtest user/serial.o
ld -m elf_i386 -T user/link.ld -o user/hexview user/hexview.o

echo "Linking GUI..."
ld -m elf_i386 -T user/link.ld -o user/gui/gui user/gui/gui.o user/gui/window.o user/gui/window_drawing.o user/gui/graphics.o user/gui/list.o

echo "Cleanup"
rm user/*.o
rm user/gui/*.o