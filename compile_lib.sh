#!/bin/bash

echo "Building syscall system..."
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/syscall.o user/lib/syscall.c

echo "Compiling..."
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/stdlib.o user/lib/stdlib.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/unistd.o user/lib/unistd.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/string.o user/lib/string.c

echo "Linking..."
ld -m elf_i386 -T user/lib/link.ld -o user/lib/lib.o user/lib/stdlib.o user/lib/unistd.o user/lib/string.o

echo "Cleanup"
rm user/lib/stdlib.o
rm user/lib/syscall.o
rm user/lib/unistd.o
rm user/lib/string.o