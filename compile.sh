#!/bin/bash

echo "Assembling..."
nasm kernel/init/load.asm -f elf32 -o "build/pre/load.o"
nasm kernel/core/descriptors.asm -f elf32 -o "build/descriptors_asm.o"
nasm kernel/core/isr.asm -f elf32 -o "build/isr_asm.o"
nasm kernel/core/irq.asm -f elf32 -o "build/irq_asm.o"
nasm kernel/core/usermode.asm -f elf32 -o "build/usermode.o"
nasm kernel/int32.asm -f elf32 -o "build/int32.o"
nasm kernel/mm/paging.asm -f elf32 -o "build/paging_asm.o"

echo "Compiling..."
# init (boot)
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/main.o kernel/init/main.c

# core
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/cmos.o kernel/core/cmos.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/portio.o kernel/core/portio.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/isr.o kernel/core/isr.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/irq.o kernel/core/irq.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/panic.o kernel/core/panic.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/timer.o kernel/core/timer.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gdt.o kernel/core/gdt.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/idt.o kernel/core/idt.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gpf.o kernel/core/gpf.c

# UI
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/shell.o kernel/ui/shell.c

# memory management
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/kheap.o kernel/mm/kheap.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/paging.o kernel/mm/paging.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/memory.o kernel/mm/memory.c

# drivers
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/acpi.o kernel/drivers/power/acpi.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/serialport.o kernel/drivers/char/serialport.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/pcspeaker.o kernel/drivers/sound/pcspeaker.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/mouse.o kernel/drivers/char/mouse.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/keyboard.o kernel/drivers/char/keyboard.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/screen.o kernel/drivers/char/screen.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/vesa.o kernel/drivers/graphics/vesa.c

# CPU
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/cpuid.o kernel/core/cpu/cpuid.c

# process
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/syscalls.o kernel/core/process/syscalls.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gsyscalls.o kernel/core/process/gsyscalls.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/task.o kernel/core/process/task.c

# libs
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/math.o kernel/lib/math.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/list.o kernel/lib/list.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/stack.o kernel/lib/stack.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/string.o kernel/lib/string.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/tree.o kernel/lib/tree.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/sha256.o kernel/lib/sha256.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/stdio.o kernel/lib/stdio.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/debug.o kernel/lib/debug.c
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/fifo.o kernel/lib/fifo.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/serialshell.o kernel/lib/serialshell.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/lifo.o kernel/lib/lifo.c

# File System
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/fs.o kernel/fs/fs.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/initrd.o kernel/fs/initrd.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/file.o kernel/fs/file.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/vfs.o kernel/fs/vfs.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/devfs.o kernel/fs/devfs.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/nullfs.o kernel/fs/nullfs.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/procfs.o kernel/fs/procfs.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/dirent.o kernel/fs/dirent.c

# Loaders
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/loader.o kernel/exec/loader.c
gcc -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/elf_loader.o kernel/exec/elf_loader.c

# graphics drivers
gcc -std=gnu99 -m32 -Wall -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/vesa.o kernel/drivers/graphics/vesa.c

echo "Linking.."

ld -m elf_i386 -T kernel/link.ld -o kernel.bin build/pre/load.o build/*.o

sudo mount images/OS.img -o loop /media/glass
sudo cp kernel.bin /media/glass
sudo sync
sudo umount /media/glass