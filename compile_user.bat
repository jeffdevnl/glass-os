@echo off

echo Building crtbegin...
"tools/Nasm/nasm.exe" user/crtbegin.asm -f elf32 -o user/crtbegin.o

echo Compiling...
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/test.o user/test.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/cat.o user/cat.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/serial.o user/serial.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/hexview.o user/hexview.c

echo Compiling GUI...
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/gui.o user/gui/gui.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/graphics.o user/gui/graphics.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/window.o user/gui/window.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/window_drawing.o user/gui/window_drawing.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -Wextra -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -Wno-unused-function -Wno-unused-parameter -I./user/lib/header/ -c -o user/gui/list.o user/gui/list.c

echo Linking...
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/link.ld -o user/test.elf user/test.o
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/link.ld -o user/cat user/cat.o
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/link.ld -o user/serialtest user/serial.o
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/link.ld -o user/hexview user/hexview.o

echo Linking GUI...
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/link.ld -o user/gui/gui user/gui/gui.o user/gui/window.o user/gui/window_drawing.o user/gui/graphics.o user/gui/list.o

echo Cleanup
del user\*.o
del user\gui\*.o