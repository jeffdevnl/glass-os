@echo off

REM Assembling asm
echo Assembling...
"tools/Nasm/nasm.exe" kernel/init/load.asm -f elf32 -o "build/pre/load.o"
"tools/Nasm/nasm.exe" kernel/core/descriptors.asm -f elf32 -o "build/descriptors_asm.o"
"tools/Nasm/nasm.exe" kernel/core/isr.asm -f elf32 -o "build/isr_asm.o"
"tools/Nasm/nasm.exe" kernel/core/irq.asm -f elf32 -o "build/irq_asm.o"
"tools/Nasm/nasm.exe" kernel/core/usermode.asm -f elf32 -o "build/usermode.o"
"tools/Nasm/nasm.exe" kernel/int32.asm -f elf32 -o "build/int32.o"
"tools/Nasm/nasm.exe" kernel/mm/paging.asm -f elf32 -o "build/paging_asm.o"

REM building c files
echo Building...

rem init (boot)
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/main.o kernel/init/main.c

rem core
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/cmos.o kernel/core/cmos.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/portio.o kernel/core/portio.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/isr.o kernel/core/isr.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/irq.o kernel/core/irq.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/panic.o kernel/core/panic.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/timer.o kernel/core/timer.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gdt.o kernel/core/gdt.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/idt.o kernel/core/idt.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gpf.o kernel/core/gpf.c

rem UI
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/shell.o kernel/ui/shell.c

rem memory management
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/kheap.o kernel/mm/kheap.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/paging.o kernel/mm/paging.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/memory.o kernel/mm/memory.c

rem drivers
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/acpi.o kernel/drivers/power/acpi.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/serialport.o kernel/drivers/char/serialport.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/pcspeaker.o kernel/drivers/sound/pcspeaker.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/mouse.o kernel/drivers/char/mouse.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/keyboard.o kernel/drivers/char/keyboard.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/screen.o kernel/drivers/char/screen.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/fdc.o kernel/drivers/storage/fdc.c

rem CPU
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/cpuid.o kernel/core/cpu/cpuid.c

rem process
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/syscalls.o kernel/core/process/syscalls.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/task.o kernel/core/process/task.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/gsyscalls.o kernel/core/process/gsyscalls.c

rem libs
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/list.o kernel/lib/list.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/stack.o kernel/lib/stack.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/math.o kernel/lib/math.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/string.o kernel/lib/string.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/tree.o kernel/lib/tree.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/sha256.o kernel/lib/sha256.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/stdio.o kernel/lib/stdio.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/debug.o kernel/lib/debug.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/fifo.o kernel/lib/fifo.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/serialshell.o kernel/lib/serialshell.c

rem File System
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/fs.o kernel/fs/fs.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/initrd.o kernel/fs/initrd.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/file.o kernel/fs/file.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/vfs.o kernel/fs/vfs.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/devfs.o kernel/fs/devfs.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/nullfs.o kernel/fs/nullfs.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/procfs.o kernel/fs/procfs.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fno-stack-protector -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/dirent.o kernel/fs/dirent.c

rem loaders
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/loader.o kernel/exec/loader.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/elf_loader.o kernel/exec/elf_loader.c

rem graphics drivers
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -I./include -c -o build/vesa.o kernel/drivers/graphics/vesa.c


rem Linking it all together
echo Linking..

"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T kernel/link.ld -o kernel.bin build/pre/load.o build/*.o

copy kernel.bin D:\