#include <stdlib.h>

int main(int argc, char **argv) {
	// Nope.
	if(argc < 2) {
		printf("Usage: hexview filename\n");
		return -1;
	}

	// Open file
	FILE *file = fopen(argv[1], "r");
	if(!file) {
		printf("%s: file not found\n", argv[1]);
		return 1;
	}

	// Get the file size
	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fseek(file, 0, SEEK_SET);
	
	// Read file
	char *buf = (char *)malloc(size);
	fread(buf, size, 1, file);
	
	// Display
	int j = 0;
	while(j < size) {
		printf("%x ", buf[j]);
		j++;

		if((j % 0x10) == 0)
			putchar('\n');
	}

	putchar('\n');

	// Free buffer and file
	free(buf);
	fclose(file);
	
	return 0;
}