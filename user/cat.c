#include <stdlib.h>

int main(int argc, char **argv) {
	// Nope.
	if(argc < 2) {
		printf("Usage: cat filename\n");
		return -1;
	}

	// Open file
	FILE *file = fopen(argv[1], "r");
	if(!file) {
		printf("%s: file not found\n", argv[1]);
		return 1;
	}

	// Get the file size
	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fseek(file, 0, SEEK_SET);

	// Read file
	char *buf = (char *) malloc(size + 1);
	fread(buf, size, 1, file);
	buf[size] = '\0';
	
	// Display file
	puts(buf);
	putchar('\n');

	// Free buffer and file
	free(buf);
	fclose(file);
	
	return 0;
}