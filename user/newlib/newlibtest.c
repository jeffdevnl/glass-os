#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
	printf("Hello world!\n");

	FILE *fp = fopen(".HIDDEN", "r");
	if(fp) {
		printf("First char of .HIDDEN: %c\n", fgetc(fp));
		fclose(fp);
	}
	
	char line[256] = { 0 };
	if(fgets(line, sizeof(line), stdin))
		printf("You typed %s\n", line);

	return 0;
}