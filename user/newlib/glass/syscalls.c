#include <sys/stat.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/times.h>
#include <sys/errno.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdarg.h>

#include <errno.h>
#undef errno
extern int errno;

#define DEF_SYS0(name, num)										\
	int sys_##name() {											\
		int a;													\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num));	\
		return a;												\
	}

#define DEF_SYS1(name, num, t1)													\
	int sys_##name(t1 p1) {														\
		int a;																	\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1));	\
		return a;																\
	}

#define DEF_SYS2(name, num, t1, t2)															\
	int sys_##name(t1 p1, t2 p2) {															\
		int a;																				\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2));	\
		return a;																			\
	}

#define DEF_SYS3(name, num, t1, t2, t3)																		\
	int sys_##name(t1 p1, t2 p2, t3 p3)	{																	\
		int a;																								\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3));	\
		return a;																							\
	}

#define DEF_SYS4(name, num, t1, t2, t3, t4)																				\
	int sys_##name(t1 p1, t2 p2, t3 p3, t4 p4) {																		\
		int a;																											\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3), "S"((int)p4));	\
		return a;																										\
	}

#define DEF_SYS5(name, num, t1, t2, t3, t4, t5)																							\
	int sys_##name(t1 p1, t2 p2, t3 p3, t4 p4, t5 p5) {																					\
		int a;																															\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3), "S"((int)p4), "D"((int)p5));	\
		return a;																														\
	}

	
// Define the syscalls here
DEF_SYS0(shutdown, 0);
DEF_SYS1(exit, 1, int);
DEF_SYS1(putch, 12, char);
DEF_SYS2(fstat, 17, int, struct stat *);
DEF_SYS1(sbrk, 18, int);
DEF_SYS0(getpid, 19);
DEF_SYS3(write, 20, int, char *, int);
DEF_SYS3(read, 21, int, char *, int);
DEF_SYS1(close, 22, int);
DEF_SYS3(lseek, 23, int, int, int);
DEF_SYS2(stat, 24, const char *, struct stat *);
DEF_SYS3(open, 25, const char *, int, int);


// DEBUG
void debug_print(const char *str) {
	while(*str != '\0')
		sys_putch(*str++);
}

/**
 ==================================
 	Functions needed by newlib
 ==================================
**/

int getpid() {
	return sys_getpid();
}

caddr_t sbrk(int incr) {
	return (caddr_t) sys_sbrk(incr);
}

int write(int file, char *ptr, int len) {
	return sys_write(file, ptr, len);
}

int read(int file, char *ptr, int len) {
	return sys_read(file, ptr, len);
}

int fstat(int file, struct stat *st) {
	return sys_fstat(file, st);
}

int close(int file) {
	return sys_close(file);
}

int lseek(int file, int offset, int dir) {
	return sys_lseek(file, offset, dir);
}

int stat(const char *file, struct stat *st) {
	return sys_stat(file, st);
}

int open(const char *name, int flags, ...) {
	va_list args;
	int mode = 0;
	int create = ((flags & O_CREAT) == O_CREAT);

	va_start(args, flags);
	if(create)
		mode = va_arg(args, int);
	va_end(args);

	int ret = sys_open(name, flags, mode);
	if(ret == 2 && create)
		errno = EACCES;
	else if(ret == 2)
		errno = ENOENT;
	else if(ret == 1)
		errno = EMFILE;

	return (ret >= 0) ? ret : -1;
}

/**
 ==================================
 		TODO / Incomplete
 ==================================
**/

int _exit(int val) {
	for(;;);
}

char *__env[1] = { 0 };
char **environ = __env;

int execve(char *name, char **argv, char **env) {
	debug_print("execve(): not implemented\n");
	errno = ENOMEM;
	return -1;
}

int fork() {
	debug_print("fork(): not implemented\n");
	errno = EAGAIN;
	return -1;
}

int isatty(int file) {
	debug_print("isatty(): not implemented\n");
	return 1;
}

int kill(int pid, int sig) {
	debug_print("kill(): not implemented\n");
	errno = EINVAL;
	return -1;
}

int link(char *old, char *new) {
	debug_print("link(): not implemented\n");
	errno = EMLINK;
	return -1;
}

clock_t times(struct tms *buf) {
	debug_print("times(): not implemented\n");
	return -1;
}

int unlink(char *name) {
	debug_print("unlink(): not implemented\n");
	errno = ENOENT;
	return -1;
}

int wait(int *status) {
	debug_print("wait(): not implemented\n");
	errno = ECHILD;
	return -1;
}