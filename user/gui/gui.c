#include <stdint.h>
#include "../../include/vesa.h"

#include <stdlib.h>
#include "include/graphics.h"
#include "include/window.h"

extern int sys_request_video(int);
extern int sys_find_video_mode(unsigned short, unsigned short, unsigned short, unsigned short, unsigned char);

int main(int argc, char **argv) {
	// For the videomode, we like to have at least 24bits per pixel
	// width: 1024 - 1280
	// height: 768 - 1024

	// Find a mode that has those settings
	unsigned short modenum = (unsigned short) sys_find_video_mode(1024, 768, 1280, 1024, 24);
	if(modenum == 0) {
		printf("Can't find a good video mode!\n");
		return 1;
	}

	// Set mode
	vbe_mode_info_t *mode = (vbe_mode_info_t *) sys_request_video(modenum);
	if(mode == NULL) {
		printf("Could not initialise graphics mode\n");
		return 1;
	}

	// Get the information we want
	unsigned int lfb = mode->lfb_address;
	unsigned char bbp = mode->bits_per_pixel;
	unsigned short width = mode->width;
	unsigned short height = mode->height;
	free(mode);

	// Initialize!
	graphics_init((unsigned char *) lfb, width, height, bbp);
	window_manager_init(width, height);

	// Test
	window_create("Test window", 50, 50, 200, 100, 0);
	window_create("Test window", 500, 50, 200, 100, 0);
	window_create("Test window", 50, 500, 200, 100, 0);
	window_create("Test window", 500, 500, 200, 100, 0);

	window_manager_loop();

	return 0;
}