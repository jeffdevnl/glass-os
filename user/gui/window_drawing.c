#include "include/window.h"
#include "include/window_drawing.h"

void window_draw_line_fast(window_t *window, int x1, int y1, int x2, int y2, unsigned int color) {
	int dx = x2 - x1;
	int dy = y2 - y1;

	if(dx < 0)
		dx = -dx;

	if(dy < 0)
		dy = -dy;

	int D = 2 * dy - dx;
	window_plot_pixel(window, x1, y1, color);
	int y = y1;

	int x = x1 + 1;
	while(x < x2) {
		if(D > 0) {
			y++;
			window_plot_pixel(window, x, y, color);
			D += 2 * (dy - dx);
		} else {
			window_plot_pixel(window, x, y, color);
			D += 2 * dy;
		}
	}
}

void window_draw_circle(window_t *window, int x0, int y0, unsigned int radius, unsigned int color) {
	int x = radius;
	int y = 0;
	int radius_error = 1 - x;

	while(x >= y) {
		window_plot_pixel(window, x + x0, y + y0, color);
		window_plot_pixel(window, y + x0, x + y0, color);
		window_plot_pixel(window, -x + x0, y + y0, color);
		window_plot_pixel(window, -y + x0, x + y0, color);
		window_plot_pixel(window, -x + x0, -y + y0, color);
		window_plot_pixel(window, -y + x0, -x + y0, color);
		window_plot_pixel(window, x + x0, -y + y0, color);
		window_plot_pixel(window, y + x0, -x + y0, color);

		y++;
		if(radius_error < 0) {
			radius_error += 2 * y + 1;
		} else {
			x--;
			radius_error += 2 * (y - x + 1);
		}
	}
}

void window_fill_rectangle(window_t *window, int x, int y, int width, int height, unsigned int color) {
	int start_x = x;
	int end_x = x + width;
	int end_y = y + height;

	while(y < end_y) {
		x = start_x;
		while(x < end_x) {
			window_plot_pixel(window, x, y, color);
			x++;
		}

		y++;
	}
}

void window_plot_pixel(window_t *window, int x, int y, unsigned int color) {
	if(x < 0 || y < 0 || x >= window->width || y >= window->height)
		return;

	// If the alpha value is not 0xFF, then we need to blend the color
	unsigned int alpha1 = (color >> 24) & 0xFF;
	if(alpha1 == 0xFF) {
		window->data[(y * window->width) + x] = color;
	} else if(alpha1 > 0x00) {
		// Begin with blending
		unsigned int red1 = (color >> 16) & 0xFF;
		unsigned int green1 = (color >> 8) & 0xFF;
		unsigned int blue1 = (color) & 0xFF;

		unsigned int pixel = window->data[(y * window->width) + x];
		unsigned int red2 = (pixel >> 16) & 0xFF;
		unsigned int green2 = (pixel >> 8) & 0xFF;
		unsigned int blue2 = (pixel) & 0xFF;

		// Mix them
		unsigned int alpha2 = 0xFF - alpha1;
		unsigned int red = (red1 * alpha1 + red2 * alpha2)/ 0xFF;
		unsigned int green = (green1 * alpha1 + green2 * alpha2) / 0xFF;
		unsigned int blue = (blue1 * alpha1 + blue2 * alpha2) / 0xFF;

		window->data[(y * window->width) + x] = 0xFF000000 | (red << 16) | (green << 8) | (blue);
	}
}