#ifndef __GRAPHICS_H
#define __GRAPHICS_H

#define MOUSE_WIDTH		16
#define MOUSE_HEIGHT	21
#define WINDOW_BBP		4

#define RGB(red, green, blue)			(0xFF000000 | (red << 16) | (green << 8) | (blue))
#define RGBA(red, green, blue, alpha)	((alpha << 24) | (red << 16) | (green << 8) | (blue))

void graphics_plot_pixel(int x, int y, unsigned int color);
void graphics_plot_pixel_rgb(int x, int y, unsigned char r, unsigned char g, unsigned char b);
void graphics_fill_rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color);
void graphics_draw_line(int x1, int y1, int x2, int y2, unsigned int color);
void graphics_init(unsigned char *vidmem, unsigned short width, unsigned short height, unsigned char bbp);

#endif