#ifndef __WINDOW_H
#define __WINDOW_H

typedef struct {
	char *title;

	unsigned int parent;
	unsigned int id;

	int x;
	int y;
	int z;
	int width;
	int height;

	unsigned int *data;
	unsigned char needs_repaint : 1;
	unsigned char movable : 1;
} window_t;

#define IS_MOUSE_IN_RECTANGLE(x, y, width, height)			(x < mouse_x && (x + width) > mouse_x && y < mouse_y && (y + height) > mouse_y)
#define IS_POINT_IN_RECTANGLE(px, py, x, y, width, height)	(x < px && (x + width) > px && y < py && (y + height) > py)

#define TITLE_BAR_HEIGHT	20

void window_destroy(window_t *window);
window_t *window_create(char *title, int x, int y, int width, int height, unsigned int parent);
void window_manager_init();
void window_manager_repaint_area(int x, int y, int width, int height);
void window_send_mouse_signal();
void window_manager_loop();

#endif