#ifndef __WINDOW_DRAWING_H
#define __WINDOW_DRAWING_H

void window_plot_pixel(window_t *window, int x, int y, unsigned int color);
void window_fill_rectangle(window_t *window, int x, int y, int width, int height, unsigned int color);
void window_draw_line_fast(window_t *window, int x1, int y1, int x2, int y2, unsigned int color);
void window_draw_circle(window_t *window, int x0, int y0, unsigned int radius, unsigned int color);

#endif