#include "../../include/stdint.h"
#include "include/list.h"
#include <stdlib.h>

list_t *list_create() {
	list_t* list = (list_t *) malloc(sizeof(list_t));

	list->length = 0;
	list->end = NULL;
	list->begin = NULL;

	return list;
}

void list_destroy(list_t *list) {
	node_t *node = list->begin;
	node_t *next;

	while(node != NULL) {
		next = node->next;
		free(node->value);
		free(node);

		node = next;
	}

	free(list);
}

void list_add_item(list_t *list, node_t *node) {
	node->next = NULL;

	if(list->end != NULL) {
		list->end->next = node;
		node->previous = list->end;
	} else {
		list->begin = node;
	}

	list->end = node;
	list->length++;
}

node_t *list_get(list_t *list, uint32_t index) {
	if(index >= list->length)
		return NULL;
	
	uint32_t i = 0;
	foreach_in_list(item, list) {
		if(i == index) {
			return item;
		}
		
		i++;
	}
	
	return NULL;
}

node_t *list_find(list_t *list, void *value) {
	foreach_in_list(item, list) {
		if(item->value == value)
			return item;
	}

	return NULL;
}

void list_delete_node(list_t *list, node_t *node) {
	if(node->previous)
		node->previous->next = node->next;

	if(node->next)
		node->next->previous = node->previous;

	if(node == list->begin)
		list->begin = node->next;

	if(node == list->end)
		list->end = node->previous;

	list->length--;
}

void list_delete_item(list_t *list, uint32_t index) {
	if(index >= list->length)
		return;

	uint32_t i = 0;
	node_t *node = list->begin;
	while(i < index) {
		node = node->next;
		i++;
	}

	list_delete_node(list, node);
}