#include <stdlib.h>
#include <string.h>

#include "include/graphics.h"
#include "include/window.h"
#include "include/window_drawing.h"
#include "include/list.h"

#define min(a, b) (a < b ? a : b)

typedef struct {
	unsigned int x;
	unsigned int y;
	char button;
} mouse_packet_t;

#define MOUSE_NONE		0x00
#define	MOUSE_LEFT		0x01
#define	MOUSE_RIGHT		0x02

list_t *windows;
window_t *root_window;
unsigned int *window_top_map;
unsigned int window_last_id = 0;

int previous_mouse_x;
int previous_mouse_y;
int mouse_x;
int mouse_y;
unsigned char mouse_button;

extern unsigned int *videomem;
extern unsigned short screen_width;
extern unsigned short screen_height;

void window_manager_init() {
	// Set mouse
	previous_mouse_x = 0;
	previous_mouse_y = 0;
	mouse_button = 0;

	// Prepare windows list
	windows = list_create();

	// Prepare window mapping
	window_top_map = (unsigned int *) malloc(sizeof(unsigned int) * screen_width * screen_height);

	// Create root window
	window_t *window = window_create("root", 0, 0, screen_width, screen_height, 0);
	window->movable = 0;

	// Clear
	memsetd(window->data, RGB(0x99, 0xCC, 0xFF), screen_width * screen_height);

	// Panel on top
	window_fill_rectangle(window, 0, 0, screen_width, 21, RGBA(0, 0, 0, 0x80));
	window_fill_rectangle(window, 0, 21, screen_width, 1, RGBA(0, 0, 0, 0x90));
	window_fill_rectangle(window, 0, 22, screen_width, 1, RGBA(0, 0, 0, 0x70));
	window_fill_rectangle(window, 0, 23, screen_width, 1, RGBA(0, 0, 0, 0x40));
	window_fill_rectangle(window, 0, 24, screen_width, 1, RGBA(0, 0, 0, 0x10));
}

void window_destroy(window_t *window) {
	// Root window can't be destroyed
	if(window->id == 0)
		return;

	// Free data
	free(window->data);
	list_delete_node(windows, list_find(windows, window));
	free(window);
}

window_t *window_create(char *title, int x, int y, int width, int height, unsigned int parent) {
	// Allocate window & window node
	window_t *window = (window_t *) malloc(sizeof(window_t));
	node_t *window_node = (node_t *) malloc(sizeof(node_t));

	// Set the window data
	window->id = ++window_last_id;
	window->x = x;
	window->y = y;
	window->z = 1;
	window->width = width;
	window->height = height;
	window->parent = parent;
	window->title = title;
	window->data = (unsigned int *) malloc(WINDOW_BBP * width * height);
	window->needs_repaint = 1;
	window->movable = 1;

	// Make the window node data
	window_node->value = window;

	// The content of the window is completely white
	memsetd(window->data, 0xFFFFFFFF, width * height);

	// But render a title bar above it
	unsigned int color = 0xFF4C4C4C;
	unsigned int i = 0;

	unsigned int yy = 0;
	unsigned int yyto = min(height, TITLE_BAR_HEIGHT);
	while(yy < yyto) {
		window_fill_rectangle(window, 0, yy, window->width, 1, color);

		i++;
		if(i >= 7)
			color -= 0x010101;

		yy++;
	}

	// Add window to the list
	list_add_item(windows, window_node);

	// Apply changes to the map
	yy = window->y;
	yyto = window->y + window->height;
	while(yy < yyto) {
		memsetd(window_top_map + yy * screen_width + window->x, (unsigned int) window, window->width);
		yy++;
	}

	return window;
}

window_t *window_get_top(int x, int y) {
	window_t *found = NULL;
	foreach_in_list(window_node, windows) {
		window_t *window = (window_t *) window_node->value;

		if(IS_POINT_IN_RECTANGLE(x, y, window->x, window->y, (int) window->width, (int) window->height)) {
			if(!found || window->z >= found->z)
				found = window;
		}
	}

	return found;
}

void window_manager_repaint_area(int x, int y, int width, int height) {
	int xto = x + width;
	int yto = y + height;
	if(xto >= screen_width) {
		// x + width = screen_width;
		//  => width = screen_width - x;
		width = screen_width - x;
		xto = screen_width;
	}

	if(yto >= screen_height) {
		height = screen_height - y;
		yto = screen_height;
	}

	int yy = y;
	while(yy < yto) {
		int xx = x;
		while(xx < xto) {
			window_t *window = (window_t *) window_top_map[yy * screen_width + xx];
			if(window) {
				int window_x = xx - window->x;
				int window_y = yy - window->y;

				graphics_plot_pixel(xx, yy, window->data[window_y * window->width + window_x]);
			}

			xx++;
		}

		yy++;
	}
}

void window_manager_loop() {
	FILE *mouse_pipe = fopen("/dev/mouse", "r");
	if(!mouse_pipe) {
		printf("No mouse pipe available!\n");
		return;
	}

	mouse_packet_t begin_mouse;
	begin_mouse.x = (screen_width - MOUSE_WIDTH) / 2;
	begin_mouse.y = (screen_height - MOUSE_HEIGHT) / 2;
	begin_mouse.button = MOUSE_NONE;
	fwrite(&begin_mouse, sizeof(mouse_packet_t), 1, mouse_pipe);

	mouse_packet_t *mouse_buffer = (mouse_packet_t *) malloc(sizeof(mouse_packet_t));

	while(1) {
		// Update windows if needed
		foreach_in_list(window_node, windows) {
			window_t *window = window_node->value;
			if(!window->needs_repaint)
				continue;

			window->needs_repaint = 0;
			window_manager_repaint_area(window->x, window->y, window->width, window->height);
		}

		// Let's get the mouse position
		unsigned int size = fread(mouse_buffer, sizeof(mouse_packet_t), 1, mouse_pipe);

		// Mouse state changed
		if(size > 0) {
			previous_mouse_x = mouse_x;
			previous_mouse_y = mouse_y;
			mouse_x = mouse_buffer->x;
			mouse_y = mouse_buffer->y;
			mouse_button = mouse_buffer->button;

			// Draw
			if(previous_mouse_x != mouse_x || previous_mouse_y != mouse_y) {
				window_manager_repaint_area(previous_mouse_x, previous_mouse_y, MOUSE_WIDTH, MOUSE_HEIGHT);
				graphics_fill_rectangle(mouse_x, mouse_y, MOUSE_WIDTH, MOUSE_HEIGHT, 0xFF444444);

				// Process mouse signal
				window_send_mouse_signal();
			}
		}
	}

	free(mouse_buffer);
	fclose(mouse_pipe);
}

int window_grabbed_x = 0;
int window_grabbed_y = 0;
window_t *grabbed_window = NULL;

void window_send_mouse_signal() {
	// Check if moving a window
	if((mouse_button & MOUSE_LEFT) == MOUSE_LEFT) {
		if(grabbed_window) {
			unsigned int previous_x = grabbed_window->x;
			unsigned int previous_y = grabbed_window->y;
			unsigned int new_x = window_grabbed_x + mouse_x;
			unsigned int new_y = window_grabbed_y + mouse_y;

			if(new_x != previous_x || new_y != previous_y) {
				int yy, yyto;

				// Place window at new position
				grabbed_window->x = new_x;
				grabbed_window->y = new_y;

				// Remove old window location from map
				yy = previous_y;
				yyto = previous_y + grabbed_window->height;
				int yoff = yy * screen_width;
				while(yy < yyto) {
					int xx = previous_x;
					int xxto = previous_x + grabbed_window->width;
					while(xx < xxto) {
						window_t *top = window_get_top(xx, yy);
						window_top_map[yoff + xx] = (unsigned int) top;
						xx++;
					}
					
					yy++;
					yoff += screen_width;
				}

				// Apply new window location changes to the map
				yy = new_y;
				yyto = new_y + grabbed_window->height;
				while(yy < yyto) {
					memsetd(window_top_map + yy * screen_width + new_x, (unsigned int) grabbed_window, grabbed_window->width);
					yy++;
				}

				// Update screen
				window_manager_repaint_area(previous_x, previous_y, grabbed_window->width, grabbed_window->height);
				window_manager_repaint_area(new_x, new_y, grabbed_window->width, grabbed_window->height);
			}
		} else {
			foreach_in_list(window_node, windows) {
				window_t *window = (window_t *) window_node->value;
				if(!window->movable)
					continue;

				if(IS_MOUSE_IN_RECTANGLE(window->x, window->y, window->width, TITLE_BAR_HEIGHT)) {
					window_grabbed_x = window->x - mouse_x;
					window_grabbed_y = window->y - mouse_y;
					grabbed_window = window;
					window->z++;

					break;
				}
			}
		}
	} else {
		grabbed_window = NULL;
	}
}