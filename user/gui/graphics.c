#include "include/graphics.h"

unsigned char *videomem;
unsigned char bytes_per_pixel;
unsigned short screen_width;
unsigned short screen_height;

void graphics_init(unsigned char *vidmem, unsigned short width, unsigned short height, unsigned char bbp) {
	videomem = vidmem;
	screen_width = width;
	screen_height = height;
	bytes_per_pixel = bbp / 8;
}

void graphics_plot_pixel(int x, int y, unsigned int color) {
	if(x < 0 || y < 0 || x >= screen_width || y >= screen_height)
		return;

	unsigned int index = (y * screen_width + x) * bytes_per_pixel;
	videomem[index] = (color >> 16) & 0xFF;
	videomem[index + 1] = (color >> 8) & 0xFF;
	videomem[index + 2] = color & 0xFF;
}

void graphics_plot_pixel_rgb(int x, int y, unsigned char r, unsigned char g, unsigned char b) {
	if(x < 0 || y < 0 || x >= screen_width || y >= screen_height)
		return;

	unsigned int index = (y * screen_width + x) * bytes_per_pixel;
	videomem[index] = r;
	videomem[index + 1] = g;
	videomem[index + 2] = b;
}

void graphics_draw_line(int x1, int y1, int x2, int y2, unsigned int color) {
	int x_delta = x1 - x2;
	int y_delta = y1 - y2;

	unsigned char r = (color >> 16) & 0xFF;
	unsigned char g = (color >> 8) & 0xFF;
	unsigned char b = (color) & 0xFF;

	int x = x1;
	while(x < x2) {
		graphics_plot_pixel_rgb(x, y1 + y_delta * (x - x1) / x_delta, r, g, b);
		x++;
	}
}

void graphics_fill_rectangle(int x, int y, unsigned int width, unsigned int height, unsigned int color) {
	if(x < 0 || y < 0 || x >= screen_width || y >= screen_height)
		return;

	unsigned int yy = y;
	unsigned int yto = y + height;

	unsigned char r = (color >> 16) & 0xFF;
	unsigned char g = (color >> 8) & 0xFF;
	unsigned char b = (color) & 0xFF;

	while(yy < yto) {
		unsigned int xx = x;
		unsigned int xxto = x + width;
		while(xx < xxto) {
			graphics_plot_pixel_rgb(xx, yy, r, g, b);
			xx++;
		}
		yy++;
	}
}