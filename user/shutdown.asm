[BITS 32]

section .text
global _start

_start:
	mov eax, 0x01
	int 0x7F

	mov eax, 0x00
	mov ebx, 0x00
	int 0x7F

.loop:
	; No HLT in usermode :(
	;hlt
	jmp .loop