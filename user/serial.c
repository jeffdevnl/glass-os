#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
	FILE *file = fopen("/dev/ttyS1", "r");
	if(!file) {
		printf("serialport not found\n");
		return 1;
	}

	int buf_size = 1024; // This is the size of the fifo buffer to
	char *buf = (char *) malloc(buf_size + 1);

	// There can be everything on this memory, clear it out!
	memset(buf, 0, buf_size + 1);

	// Read
	fread(buf, buf_size, 1, file);

	// Display
	puts(buf);
	putchar('\n');

	// Free stuff
	free(buf);
	fclose(file);
	
	return 0;
}