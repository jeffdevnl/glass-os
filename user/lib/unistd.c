#include "header/unistd.h"
#include "syscall.h"

int chdir(const char *path) {
	return sys_chdir((char *) path);
}

char *getwd(char *buffer) {
	sys_getwd(buffer);
	
	return buffer;
}

char *getcwd() {
	return (char *) sys_getcwd();
}