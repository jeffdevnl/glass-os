#include "header/string.h"
#include "syscall.h"

void *memcpy(void *dest, void *src, size_t n) {
    // Something to copy?
    if(n == 0)
        return dest;

    // Copy sizeof(uint64_t) bytes per loop
    while(n >= sizeof(unsigned long)) {
        *(unsigned long *)dest = *(unsigned long *)src;

        dest += sizeof(unsigned long);
        src += sizeof(unsigned long);
        n -= sizeof(unsigned long);
    }

    // Copy sizeof(uint32_t) bytes per loop
    while(n >= sizeof(unsigned int)) {
        *(unsigned int *)dest = *(unsigned int *)src;

        dest += sizeof(unsigned int);
        src += sizeof(unsigned int);
        n -= sizeof(unsigned int);
    }

    // Copy sizeof(uint16_t) bytes per loop
    while(n >= sizeof(unsigned short)) {
        *(unsigned short *)dest = *(unsigned short *)src;

        dest += sizeof(unsigned short);
        src += sizeof(unsigned short);
        n -= sizeof(unsigned short);
    }

    // Copy sizeof(uint8_t) byte per loop
    while(n >= sizeof(unsigned char)) {
        *(unsigned char *)dest = *(unsigned char *)src;

        dest += sizeof(unsigned char);
        src += sizeof(unsigned char);
        n -= sizeof(unsigned char);
    }

    return dest;
}

int memcmp(const void *s1, const void *s2, size_t n) {
    unsigned char *a1 = (unsigned char *) s1;
    unsigned char *b1 = (unsigned char *) s2;

    unsigned int index = 0;
    while(index < n) {
        if(a1[index] > b1[index])
            return -1;
        else if(b1[index] > a1[index])
            return 1;
        else if(a1[index] == '\0' && b1[index] == '\0')
            return 0;

        index++;
    }

    return 0;
}

void *memset(void *s, int c, size_t n) {
    __asm__ __volatile__("cld; rep stosb" : "+c" (n), "+D" (s) : "a" (c) : "memory");
    return s;
}

void *memsetw(void *s, int c, size_t n) {
    __asm__ __volatile__("cld; rep stosw" : "+c" (n), "+D" (s) : "a" (c) : "memory");
    return s;
}

void *memsetd(void *s, int c, size_t n) {
    __asm__ __volatile__("cld; rep stosl" : "+c" (n), "+D" (s) : "a" (c) : "memory");
    return s;
}