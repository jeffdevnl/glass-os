#include "syscall.h"

// Define all the syscalls here
DEF_SYS0(shutdown, 0);
DEF_SYS1(exit, 1, int);
DEF_SYS1(print, 2, const char *);
DEF_SYS1(fopen, 3, const char *);
DEF_SYS3(fread, 4, void *, int, void *);
DEF_SYS3(fwrite, 5, const void *, int, void *);
DEF_SYS1(fclose, 6, void *);
DEF_SYS1(malloc, 7, int);
DEF_SYS1(free, 8, void *);
DEF_SYS1(getwd, 9, char *);
DEF_SYS0(getcwd, 10); // Wrong implementation!
DEF_SYS1(chdir, 11, char *);
DEF_SYS1(putch, 12, char);
DEF_SYS1(ftell, 13, void *);
DEF_SYS3(fseek, 14, void *, int, int);
DEF_SYS1(request_video, 15, int);
DEF_SYS5(find_video_mode, 16, unsigned short, unsigned short, unsigned short, unsigned short, unsigned char);