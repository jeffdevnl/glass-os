#include "header/stdlib.h"
#include "header/stdarg.h"
#include "syscall.h"

void *malloc(size_t size) {
	if(size == 0)
		return NULL;

	return (void *) sys_malloc(size);
}

void *calloc(size_t nmemb, size_t size) {
	if(size == 0 || nmemb == 0)
		return NULL;

	return (void *) sys_malloc(size * nmemb);
}

void free(void *ptr) {
	if(ptr == NULL)
		return;
	
	sys_free(ptr);
}

int putchar(int c) {
	sys_putch((char)c);
	return 0;
}

void printf_write_hex(unsigned int val) {
	if(val == 0) {
		sys_putch('0');
		return;
	}

	int tmp;
	char noZeroes = 1;

	int i = 28;
	for(; i >= 0; i -= 4) {
		tmp = (val >> i) & 0x0F;
		if(tmp == 0 && noZeroes != 0)
			continue;
		
		noZeroes = 0;
		if(tmp >= 0x0A)
			sys_putch(tmp - 0x0A + 'A');
		else
			sys_putch(tmp + '0');
	}
}

void printf_write_dec(int val) {
	if(val == 0) {
		sys_putch('0');
		return;
	}

	if(val < 0) {
		val = -val;
		sys_putch('-');
	}

	int v = val / 10;
	int n = val % 10;
	
	if(n < 0) {
		n--;
		v += 10;
	}
	
	if(val >= 10)
		printf_write_dec(v);
	
	sys_putch((char)(n + '0'));
}

void printf(const char *format, ...) {
	va_list args;
	va_start(args, format);
	
	unsigned int i = 0;
	for(; format[i] != '\0'; i++) {
		if(format[i] != '%') {
			sys_putch(format[i]);
		} else {
			i++;
			switch(format[i]) {
				case 's':
					sys_print((char *)va_arg(args, char *));
					break;
					
				case 'x':
					printf_write_hex((unsigned int)va_arg(args, unsigned int));
					break;
					
				case 'd':
					printf_write_dec((int)va_arg(args, int));
					break;
				
				case 'c':
					sys_putch((char)va_arg(args, int));
					break;
				
				case '%':
					sys_putch('%');
					break;
				
				default:
					sys_putch(format[i]);
					break;
			}
		}
	}
	
	va_end(args);
}

int puts(const char *s) {
	sys_print(s);
	return 0;
}

FILE *fopen(const char *path, const char *mode) {
	// TODO: mode is unused atm
	return (FILE *) sys_fopen(path);
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	return sys_fread(ptr, size * nmemb, stream);
}

size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
	return sys_fwrite(ptr, size * nmemb, stream);
}

int fclose(FILE *fp) {
	return sys_fclose(fp);
}

int fseek(FILE *stream, long offset, int whence) {
	return sys_fseek(stream, offset, whence);
}

long ftell(FILE *stream) {
	return sys_ftell(stream);
}