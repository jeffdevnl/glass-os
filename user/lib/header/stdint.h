#ifndef __STDINT_H
#define __STDINT_H

typedef char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned int uint32_t;
typedef long int64_t;
typedef unsigned long uint64_t;

#define UINT8_MAX	0xFF
#define UINT16_MAX	0xFFFF
#define UINT32_MAX	0xFFFFFFFF
#define UINT64_MAX	0xFFFFFFFFFFFFFFFF
#define INT8_MAX	0x7f
#define INT8_MIN   	(-INT8_MAX - 1)
#define INT16_MAX   0x7fff 
#define INT16_MIN   (-INT16_MAX - 1)
#define INT32_MAX   0x7fffffffL
#define INT32_MIN   (-INT32_MAX - 1L)
#define INT64_MAX   0x7fffffffffffffffLL
#define INT64_MIN   (-INT64_MAX - 1LL)

#endif