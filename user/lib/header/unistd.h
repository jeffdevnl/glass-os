#ifndef __UNISTD_H
#define __UNISTD_H

int chdir(const char *);
char *getwd(char *);
char *getcwd();

#endif