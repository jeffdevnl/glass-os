#ifndef __STRING_H
#define __STRING_H

#ifndef __SIZE_T_DEF
#define __SIZE_T_DEF
typedef int	size_t;
#endif

void *memcpy(void *dest, void *src, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);
void *memset(void *s, int c, size_t n);
void *memsetw(void *s, int c, size_t n);
void *memsetd(void *s, int c, size_t n);

#endif