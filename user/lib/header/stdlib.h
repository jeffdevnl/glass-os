#ifndef __STDLIB_H
#define __STDLIB_H

#ifndef NULL
#define NULL		(void *) 0
#endif

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2

#ifndef __SIZE_T_DEF
#define __SIZE_T_DEF
typedef int	size_t;
#endif

typedef struct {
	unsigned int offset;
} FILE;

void *malloc(size_t size);
void *calloc(size_t nmemb, size_t size);
void free(void *ptr);

FILE *fopen(const char *path, const char *mode);
size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
int fclose(FILE *fp);
long ftell(FILE *stream);
int fseek(FILE *stream, long offset, int whence);

void printf(const char *format, ...);
int puts(const char *s);
int putchar(int c);

#endif