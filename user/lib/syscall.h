#ifndef __SYSCALL_H
#define __SYSCALL_H

#define DEC_SYS0(name)					int sys_##name()
#define DEC_SYS1(name, a)				int sys_##name(a)
#define DEC_SYS2(name, a, b)			int sys_##name(a, b)
#define DEC_SYS3(name, a, b, c)			int sys_##name(a, b, c)
#define DEC_SYS4(name, a, b, c, d)		int sys_##name(a, b, c, d)
#define DEC_SYS5(name, a, b, c, d, e)	int sys_##name(a, b, c, d, e)

#define DEF_SYS0(name, num)										\
	int sys_##name() {											\
		int a;													\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num));	\
		return a;												\
	}

#define DEF_SYS1(name, num, t1)													\
	int sys_##name(t1 p1) {														\
		int a;																	\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1));	\
		return a;																\
	}

#define DEF_SYS2(name, num, t1, t2)															\
	int sys_##name(t1 p1, t2 p2) {															\
		int a;																				\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2));	\
		return a;																			\
	}

#define DEF_SYS3(name, num, t1, t2, t3)																		\
	int sys_##name(t1 p1, t2 p2, t3 p3)	{																	\
		int a;																								\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3));	\
		return a;																							\
	}

#define DEF_SYS4(name, num, t1, t2, t3, t4)																				\
	int sys_##name(t1 p1, t2 p2, t3 p3, t4 p4) {																		\
		int a;																											\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3), "S"((int)p4));	\
		return a;																										\
	}

#define DEF_SYS5(name, num, t1, t2, t3, t4, t5)																							\
	int sys_##name(t1 p1, t2 p2, t3 p3, t4 p4, t5 p5) {																					\
		int a;																															\
		__asm__ __volatile__("int $0x7F" : "=a"(a) : "0"(num), "b"((int)p1), "c"((int)p2), "d"((int)p3), "S"((int)p4), "D"((int)p5));	\
		return a;																														\
	}

	
// Declare all the syscalls here
DEC_SYS0(shutdown);
DEC_SYS1(exit, int);
DEC_SYS1(print, const char *);
DEC_SYS1(fopen, const char *);
DEC_SYS1(fclose, void *);
DEC_SYS3(fread, void *, int, void *);
DEC_SYS3(fwrite, const void *, int, void *);
DEC_SYS1(malloc, int);
DEC_SYS1(free, void *);
DEC_SYS1(getwd, char *);
DEC_SYS0(getcwd);
DEC_SYS1(chdir, char *);
DEC_SYS1(putch, char);
DEC_SYS1(ftell, void *);
DEC_SYS3(fseek, void *, int, int);
DEC_SYS1(request_video, int);
DEC_SYS5(find_video_mode, unsigned short, unsigned short, unsigned short, unsigned short, unsigned char);

#endif