#include <stdlib.h>

int main(int argc, char **argv) {
	char *test_string = "Hello, I'm a C program!\n";
	printf(test_string);

	char *test = "Number of arguments: %d\n";
	printf(test, argc);
	
	// Read .HIDDEN file in the initrd
	FILE *stream = fopen("/initrd/.HIDDEN", "r");
	if(stream != NULL) {
		printf("Reading file from initrd..\n");
		char *toast = "  \n";
		fread(toast, 2, 1, stream);
		printf(toast);
		
		fclose(stream);
	}
	
	// Write example to serialport com2 :D
	stream = fopen("/dev/serial/COM2", "w");
	if(stream != NULL) {
		printf("Writing to COM2..!\n");
		char *message = "Testing..\n";
		fwrite(message, 19, 1, stream);
		
		fclose(stream);
	}
	
	// Where am I working?
	//char *working_directory = getcwd();
	//printf(working_directory);
	
	return 0;
}