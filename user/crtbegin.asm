[BITS 32]

extern main
global _start

; This is the global entry point
_start:
	; C main function
	call main

	; EBX should contain errorcode, but EAX contains errorcode from the main function
	mov ebx, eax
	mov eax, 0x00
	int 0x7F

.loop:
	; We can't HLT in usermode :(
	;hlt
	jmp .loop