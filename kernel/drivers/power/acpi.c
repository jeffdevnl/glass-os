#include <system.h>

extern page_directory_t *kernel_directory;

RSDP_t *rdsp = 0;
RSDT_t *rsdt = 0;
FADT_t *fadt = 0;

uint16_t SLP_TYPa;
uint16_t SLP_TYPb;
const uint32_t SLP_EN = 1 << 13;

void acpi_find() {

	// Finding the RSDP
	// First attempt to bios data 
	uint8_t *biosp = (uint8_t *) 0x000E0000;

	while((uint32_t) biosp < 0x000FFFFF) {
		if(!memcmp(biosp, "RSD PTR ", 8) && acpi_check_sum((uint32_t *)biosp, sizeof(RSDP_t))) {
		
			rdsp = (RSDP_t *)biosp;
			break; // Found it!
		}

		biosp += 16;
	}
	
	// Search the rdsp through bios data when the rdsp is not found in the ebda
	if(rdsp == 0) {

		uint8_t *ebdap = (uint8_t *) ((*(uint16_t *) 0x040E) << 4); 

		// Second attempt though edda
		while((uint32_t) ebdap < 0x000A0000) {
			if(!memcmp(ebdap, "RSD PTR ", 8) && acpi_check_sum((uint32_t *) ebdap, sizeof(RSDP_t))) {
				rdsp = (RSDP_t *)ebdap;
				break; // Found it!
			}
		
			ebdap += 16;
		}

		ASSERT(rdsp != 0, "Could not find RDSP");
	}
	
	rsdt = (RSDT_t *)rdsp->RsdtAddress;
	ASSERT(rsdt != 0, "Could not find RSDT");
	fadt = acpi_find_entry("FACP");
	ASSERT(fadt != 0, "Could not find FACP");
}

void acpi_parse_s5() {
	uint8_t s5found = 0;
	uint32_t dsdtLength= (fadt->Dsdt + 1) - 36;
	uint8_t *S5Addr = (uint8_t *)fadt->Dsdt + 36;

	if(dsdtLength > 0) {
		while(dsdtLength > 0) {
			if(memcmp(S5Addr, "_S5_", 4) == 0) {
				s5found = 1;
				break;
			}
			  
			S5Addr++;
		}
		
		ASSERT(s5found, "Could not found S5 object");
		
		if((*(S5Addr - 1) == 0x08 || (*(S5Addr - 2) == 0x08 && *(S5Addr - 1) == '\\')) && *(S5Addr + 4) == 0x12) {
			S5Addr += 5;
            S5Addr += ((*S5Addr & 0xC0) >> 6) + 2;   

            if (*S5Addr == 0x0A)
                S5Addr++; 
				
            SLP_TYPa = *(S5Addr) << 10;
			
			S5Addr++;
			
			if(*S5Addr == 0x0A) 
				S5Addr++;
				
			SLP_TYPb = *(S5Addr) << 10;
		} else {
			PANIC("S5 object has not the right structure");
		}
	}
}

uint8_t acpi_check_sum(void *address, uint32_t length) {
    char *bptr = (char *) address;
    char check = 0;
    uint32_t i;   
   
    for(i = 0; i < length; i++) {
       check += *bptr;
       bptr++;
    }

    if(check == 0)
       return 1;         
       
    return 0;
}

void *acpi_find_entry(char *signature) {
	if(rsdt == 0)
		return NULL;

	uint32_t n = (rsdt->header.Length - sizeof(rsdt->header)) / 4;
	
	for(uint32_t i = 0; i < n; i++) {
		rsdth_t *header = (rsdth_t *) (rsdt->firstSDT + i);
		
		if(!memcmp((void *)header, (void *)signature, 4))
			if(acpi_check_sum((uint32_t *)header, header->Length))
				return (void *)header;
	}
	
	return NULL;
}

void acpi_enable() {
	outportb(fadt->SMI_CommandPort,fadt->AcpiEnable);
}

void acpi_disable() {
	outportb(fadt->SMI_CommandPort, fadt->AcpiDisable);
}

void acpi_print() {
	kprintf("ACPI: \n\nEnabled: %x", inportw((uint32_t) fadt->PM1aControlBlock));
}

void acpi_init() {
	acpi_find();
	acpi_parse_s5();
	
	acpi_enable();
}

// Making it work under paging, not giving a pagefault
void acpi_enable_paging() {
	uint32_t address = (uint32_t)fadt;
	uint32_t end = address + sizeof(FADT_t);
	while(address < end) {
		paging_direct_mem_access(paging_get_page(address, 1, kernel_directory), 1, 1, address);
		address += 0x1000;
	}
}

void acpi_reset() {
	// TODO: Write RESET_VALUE to RESET_REG
	kprintf("%x", fadt->ResetReg);
}

void acpi_shutdown() {
	// try 1 through the pm1a control block
	outportw(fadt->PM1aControlBlock, SLP_TYPa | SLP_EN);
	// try 2 through the pm1b control block
	outportw(fadt->PM1bControlBlock, SLP_TYPb | SLP_EN);
}