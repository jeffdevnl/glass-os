#include <system.h>

#define FDC_IRQ 				0x6
#define FDC_DMA_CHANNEL 		2
#define FLPY_SECTORS_PER_TRACK 	18
#define DMA_BUFFER 				0x1000
#define FDC_DOR  				0x3f2 
#define FDC_MSR  				0x3f4
#define FDC_DRS  				0x3f4  
#define FDC_DATA 				0x3f5 
#define FDC_CCR					0x3f7
#define CMD_SPECIFY 			0x03

uint8_t prim_avail = 0;
uint8_t second_avail = 0;
uint8_t fdc_int = 0;
uint8_t fdc_drive = 0;

void fdc_irq(registers_t *reg) {
	fdc_int = 1;
}

uint8_t fdc_status() {
	return inportb(FDC_MSR);
}

void fdc_wait_interrupt() {
	while(!fdc_int);
	fdc_int = 0;
}

void fdc_parse_cmos(uint8_t primary, uint8_t second) {
	if(primary != 0)
		prim_avail = 1;
	
	if(second != 0)
		second_avail = 1;
}

void fdc_write_dor(uint8_t cmd) {
	outportb(FDC_DOR, cmd);
}

void fdc_write_ccr(uint8_t val) {
	outportb(FDC_CCR, val);
}

// INTEL MANUAL (L)
void fdc_sendbyte(int byte) {
	volatile int msr;

	int tmo = 0;
	for(; tmo < 128; tmo++) {
		msr = inportb(FDC_MSR);
		if((msr & 0xc0) == 0x80) {
			outportb(FDC_DATA,byte);
			return;
		}

		// Delay
		inportb(0x80);
   }
}
 
uint8_t fdc_read_data() {
	for (int i = 0; i < 500; i++ )
		if ( fdc_status() & 128 )
			return inportb(FDC_DATA); 
			
	return 0;
}

void fdc_send_cmd(uint8_t cmd) {
	int i;
	for (i = 0; i < 200; i++ ) {
		if ( fdc_status() & 128 )
			return outportb(FDC_DATA, cmd);
	}
}
void fdc_check_int(uint32_t *status, uint32_t *cylinder) {
	fdc_send_cmd(8); // CHECK_INT
	
	*status = fdc_read_data();
	*cylinder = fdc_read_data();
}

void fdc_controller_enable() {
	fdc_write_dor(4 | 8); // Enable controller and put it in DMA mode
}

void fdc_controller_disable() {
	fdc_write_dor(0);
}

void fdc_dma_init() {
	outportb(0x0a,0x06);
	outportb(0xd8,0xff);
	outportb(0x04, 0);
	outportb(0x04, 0x10);
	outportb(0xd8, 0xff);
	outportb(0x05, 0xff);
	outportb(0x05, 0x23);
	outportb(0x80, 0);
	outportb(0x0a, 0x02);
}

void fdc_dma_read() {
	outportb(0x0a, 0x06);
	outportb(0x0b, 0x56);
	outportb(0x0a, 0x02);
}

void fdc_dma_write() {
	outportb(0x0a, 0x06);
	outportb(0x0b, 0x5a);
	outportb(0x0a, 0x02);
}

void fdc_calibrate() {
	uint32_t c_status;
	uint32_t c_cylinder;
	
	int i;
	for(i = 0; i < 5; i++) {
		fdc_send_cmd(7);
		fdc_send_cmd(fdc_drive);
		fdc_wait_interrupt();
		
		fdc_check_int(&c_status, &c_cylinder);
		
		if(!c_cylinder)
			return;
	}
}


int fdc_seek(uint32_t cylinder, uint32_t head) {
	uint32_t c_status;
	uint32_t c_cylinder;
	
	int i;
	// Let's give um 5 trys
	for(i = 0; i < 5; i++) {
		fdc_send_cmd(0xF); // FDC_SEEK
		fdc_send_cmd((head) << 2 | fdc_drive);
		fdc_send_cmd(cylinder);
		
		fdc_wait_interrupt();
		fdc_check_int(&c_status, &c_cylinder);
		
		if(c_cylinder == cylinder)
			return 0; // Houston, we have arrived!
	}
	
	return -1; // Houston, we got a problem
}

void fdc_specify(uint32_t steprate, uint32_t loadtime, uint32_t unloadtime) {
	uint32_t data; 
	
	fdc_send_cmd(3); // FDC_SPECIFY
	
	// Steprate | unloadtime
	data = (steprate << 4) | unloadtime;
	fdc_send_cmd(data);
	
	// Loadtime | DMA enabled? (Always yes!)
	data = (unloadtime << 4) | 1;
	fdc_send_cmd(data);
}

void fdc_read_sector_i(uint8_t head, uint8_t track, uint8_t sector) {
	uint32_t c_status;
	uint32_t c_cylinder;
	
	// Set the DMA to read mode
	fdc_dma_read();
	
	// Do command =D
	fdc_send_cmd(6 | 0x80 | 0x20 | 0x40);
	fdc_send_cmd((head << 2) | fdc_drive);
	fdc_send_cmd(track);
	fdc_send_cmd(head);
	fdc_send_cmd(sector);
	fdc_send_cmd(2);
	fdc_send_cmd(( ( sector + 1 ) >= 80 )
			? 80 : sector + 1 );
	fdc_send_cmd(27);
	fdc_send_cmd(0xFF);
	
	fdc_wait_interrupt();
	
	int i;
	for(i = 0; i < 7; i++)
		fdc_read_data(); // we dont need that D=
		
	fdc_check_int(&c_status, &c_cylinder);
}

void fdc_lbachs(uint32_t lba, uint32_t *head, uint32_t *track, uint32_t *sector) {
	*head = ((lba % 160) / 80);
	*track = lba / 160;
	*sector = lba % 80 + 1;
}

uint8_t *fdc_read_sector(uint32_t sectori) {
	uint32_t head;
	uint32_t track;
	uint32_t sector;
	
	fdc_lbachs(sectori, &head, &track, &sector);

	// Seek the drive to the good location :)
	if(fdc_seek(track, head) == -1)
		return 0;
		
	fdc_read_sector_i(head, track, sector);
	
	return (uint8_t *) 0x1000;
}

void fdc_reset() {
	uint32_t st0, cyl;
	
	fdc_controller_disable();
	fdc_controller_enable();
	outportb(FDC_DRS, 0x00);
	
	
	// wait for interrupt
	fdc_wait_interrupt();
	
	fdc_write_ccr(0); 
	for (int i=0; i<4; i++)
		fdc_check_int(&st0,&cyl);
	
	// Fix drive times
	fdc_sendbyte(CMD_SPECIFY);
	fdc_sendbyte(0xdf);  /* SRT = 3ms, HUT = 240ms */
	fdc_sendbyte(0x02);  /* HLT = 16ms, ND = 0 */
	
	fdc_calibrate();
}

void fdc_init() {
	// Parse CMOS to check if there is a FDC
	outportb(0x70, 0x10);
	uint8_t cmos = inportb(0x71);
	fdc_parse_cmos((cmos&0xf0) >> 4, cmos&0x0f);
	
	if(!prim_avail) return;
	
	// Install FDC interrupt
	interrupt_install_handler(FDC_IRQ, fdc_irq);
	
	fdc_dma_init();
	
	// :D
	fdc_reset();
	fdc_specify(13, 1, 0xF);
	
	uint8_t *sector = fdc_read_sector(0);
	kprintf("%x\n", (uint32_t) sector);
	int i;
	for(i = 0; i < 512; i++)
		kprintf("%x ", *sector++);
}