#include <system.h>

vbe_info_block_t *vesa_vbeinfo;
uint16_t *supported_modes;
uint8_t vesa_supported;

void vesa_init() {
   // Request info via VBE, put info at 0x500 because we are 100% sure that address is free
   // ( see memory map on http://wiki.osdev.org/Memory_Map_%28x86%29 )
   regs16_t regs;
   regs.ax = 0x4F00;
   regs.di = 0x500;
   regs.es = 0x00;
   int32(0x10, &regs);

   // Save return value
   uint8_t al = regs.ax & 0xFF;
   uint8_t ah = (regs.ax >> 8) & 0xFF;

   // Check
   vesa_supported = 0;
   if(al == 0x4F) {
      if(ah == 0x00) {
         vesa_supported = 1;
      } else if(ah == 0x01) {
         kprintf("VESA: failed to get info\n");
      } else if(ah == 0x02) {
         kprintf("VESA: not supported by current hardware configuration\n");
      } else if(ah == 0x03) {
         kprintf("VESA: info function invalid in current videomode\n");
      }
   } else {
      kprintf("VESA: not supported!\n");
   }

   // If VESA is available
   if(vesa_supported) {
      // Copy over
      vesa_vbeinfo = (vbe_info_block_t *) malloc(sizeof(vbe_info_block_t));
      memcpy(vesa_vbeinfo, (void *) 0x500, sizeof(vbe_info_block_t));

      // Copy supported modes
      uint16_t *listptr = (uint16_t *) vesa_vbeinfo->supported_modes;
      uint32_t index = 0;
      while(1) {
         uint16_t mode = listptr[index++];
         if(mode == 0xFFFF)
            break;
      }

      supported_modes = (uint16_t *) malloc(sizeof(uint16_t) * index);
      memcpy(supported_modes, listptr, sizeof(uint16_t) * index);

      // VBE2.0 is required at least for the LFB
      if(vesa_vbeinfo->version < 0x200) {
         vesa_supported = 0;
         kprintf("VESA: VBE2.0 or higher is required, version is %x!\n", vesa_vbeinfo->version);
      }
   }
}

vbe_mode_info_t *vesa_get_mode(uint16_t mode, uint8_t lfb) {
   if(!vesa_supported)
      return NULL;

   // Set bit 14 is LFB
   if(lfb)
      mode |= 1 << 14;

   // Temporary disable paging
   paging_disable();

   // Request info via VBE, put info at 0x500 because we are 100% sure that address is free
   // ( see memory map on http://wiki.osdev.org/Memory_Map_%28x86%29 )
   regs16_t regs;
   regs.ax = 0x4F01;
   regs.cx = mode;
   regs.di = 0x500;
   regs.es = 0x00;
   int32(0x10, &regs);

   // Copy over
   vbe_mode_info_t *mode_info = (vbe_mode_info_t *) malloc(sizeof(vbe_mode_info_t));
   memcpy(mode_info, (void *) 0x500, sizeof(vbe_mode_info_t));

   // Reenable paging
   paging_enable();

   // Save return value
   uint8_t al = regs.ax & 0xFF;
   uint8_t ah = (regs.ax >> 8) & 0xFF;

   // Check
   if(al != 0x4F || ah != 0x00) {
      free(mode_info);
      return NULL;
   }

   return mode_info;
}

uint16_t vesa_find_mode(uint16_t min_width, uint16_t min_height, uint16_t max_width, uint16_t max_height, uint8_t min_bbp) {
   uint32_t index = 0;
   uint16_t current = 0;
   uint16_t current_width = 0;
   uint16_t current_height = 0;
   uint16_t current_bbp = 0;

   while(1) {
      uint16_t mode = supported_modes[index++];
      if(mode == 0xFFFF)
         break;

      vbe_mode_info_t *info = vesa_get_mode(mode, 1);

      // Invalid dimensions?
      if(info->width < min_width || info->height < min_height || info->width > max_width || info->height > max_height) {
         free(info);
         continue;
      }

      // Invalid bbp?
      if(info->bits_per_pixel < min_bbp) {
         free(info);
         continue;
      }

      // Okay?
      if(current_width <= info->width && current_height <= info->height && current_bbp <= info->bits_per_pixel) {
         current = mode;
         current_width = info->width;
         current_height = info->height;
         current_bbp = info->bits_per_pixel;
      }

      // Cleanup
      free(info);
   }

   return current;
}

vbe_mode_info_t *vesa_set_mode(uint16_t mode, uint8_t lfb, uint8_t page) {
   vbe_mode_info_t *mode_info = vesa_get_mode(mode, lfb);
   if(mode_info == NULL)
      return NULL;

   // Page the area?
   if(page) {
      extern page_directory_t *current_directory;

      // Video memory
      uint32_t vidmem_start = mode_info->lfb_address;
      uint32_t vidmem_end = vidmem_start + (mode_info->width * mode_info->height * mode_info->bits_per_pixel / 8);
      uint32_t address = vidmem_start;
      while(address < vidmem_end) {
         // Use paging_direct_mem_access because it's not the real RAM, it's video memory
         paging_direct_mem_access(paging_get_page(address, 1, current_directory), 0, 1, address);
         address += 0x1000;
      }
   }

   // Temporary disable paging
   paging_disable();

   // If everything is okay, actually perform the switch
   regs16_t regs;
   regs.ax = 0x4F02;
   regs.bx = mode;
   int32(0x10, &regs);

   // Reenable paging
   paging_enable();

   // Save return value
   uint8_t al = regs.ax & 0xFF;
   uint8_t ah = (regs.ax >> 8) & 0xFF;

   // Check
   if(al != 0x4F || ah != 0x00) {
      free(mode_info);
      return NULL;
   }

   return mode_info;
}