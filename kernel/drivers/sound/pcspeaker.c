#include <system.h>

void pcspeaker_play(uint32_t freq) {
	// Set PIT to the desired frequency
	uint32_t dfreq;
	dfreq = 1193180 / freq;
	outportb(0x43, 0xB6);
	outportb(0x42, (uint8_t) dfreq);
	outportb(0x42, (uint8_t) (dfreq >> 8));
	
	// Play the sound
	uint8_t tmp;
	tmp = inportb(0x61);
	if(tmp != (tmp | 3)) 
		outportb(0x61, tmp | 3);
}

void pcspeaker_stop() {
	outportb(0x61, (inportb(0x61) & 0xFC));
}