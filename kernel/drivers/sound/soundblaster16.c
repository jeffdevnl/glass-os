#include <system.h>

void SB16_setup() {
	DSP_Reset();
	SB16_DSPWrite(0xD1); // Turn on :D
}

void SB16_DSPWait() {
	while((inportb(SB16_DSPWriteStatus) & 0x80) != 0)
		__asm__ __volatile__("hlt");
}

void SB16_DSPWrite(uint8_t value) {
	SB16_DSPWait();
	outportb(SB16_DSPWrite, (uint8_t)value);
}

void SB16_SetSampling(uint16_t rate) {
	SB16_DSPWrite(14);
	SB16_DSPWrite((uint8_t)(rate << 8));
	SB16_DSPWrite((uint8_t)rate);
}

uint8_t SB16_read() {
	SB16_DSPAvailable();
	return inportb(DSPRead);
}

void SB16_DSPAvailable() {
	while((inportb(SB16_DSPstat) & 0x80) == 0)
		__asm__ __volatile__("hlt");
}

void SB16_DSPreset() {
	SB16_DSPWrite(SB16_DSPReset, 0x01);

	// TODO: wait for 3 micro sec here
	int i;
	for(int i = 0; i < 30; i++);

	SB16_DSPReset(SB16_DSPreset, 0x00);
	SB16_DSPAvailable();
	for(int p = 0; p < 1000; p++) { 
		if((inp(SB16_DSPReadStatus)) == DSP_READY) {
			// Resetted
			return;
		}	
	}
	
	// Not reset
	kprinf("Could not reset DSP");
}