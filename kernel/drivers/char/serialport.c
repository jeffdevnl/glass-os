#include <system.h>

typedef struct {
	char *name;
	int address;
	fifo_t *buffer;
} comport_t;

comport_t serial_comports[]  = {
	{ "COM1", 0, 0 },
	{ "COM2", 0, 0 },
	{ "COM3", 0, 0 },
	{ "COM4", 0, 0 }
};

int comports = sizeof( serial_comports ) / sizeof( comport_t );

int serial_transmit_empty(int port) {
	return inportb(port + 0x05) & 0x20;
}

int serial_received(int port) {
   return inportb(port + 0x05) & 1;
}

void serial_write(char d, int port) {
	while(serial_transmit_empty(port) == 0);

	outportb(port, d);
}

void serial_writestring(char *string, int port) {
	int length = strlen(string);
	
	int i = 0;
	while(i < length) {
		serial_write(string[i], port);
		i++;
	}
	
	serial_write('\0', port);
}

uint32_t serial_writefs(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	if(node->inode >= comports)
		return 0;
	
	int i = 0;
	while(i < size)  {
		serial_write(buffer[i], serial_comports[node->inode].address);
		
		i++;
	}
	
	return 1;
}

uint32_t serial_readfs(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	int bytes_read = 0;
	int comid = node->inode;
	
	bytes_read = fifo_read(serial_comports[comid].buffer, buffer, size);
	
	return bytes_read;
}

char serial_read(int port) {
   while (serial_received(port) == 0);
 
   return inportb(port);
}

void serialport_handler_13(registers_t *r) {
	comport_t *port = NULL;
	
	if(serial_comports[0].address && serial_received(serial_comports[0].address) != 0) {
		port = &serial_comports[0];
	} else {
		port = &serial_comports[2];
	}
	
	if(port->address == 0)
		return;

	while(serial_received(port->address) != 0) {
		fifo_write_byte(port->buffer, serial_read(port->address));
	}
}

void serialport_handler_24(registers_t *r) {
	comport_t *port = NULL;
	
	if(serial_comports[1].address && serial_received(serial_comports[1].address) != 0) {
		port = &serial_comports[1];
	} else {
		port = &serial_comports[3];
	}
	
	if(port->address == 0)
		return;

	while(serial_received(port->address) != 0) {
		fifo_write_byte(port->buffer,  serial_read(port->address));
	}
}

fs_node_t *serial_port_node(int port) {
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = 0;
	node->finddir = 0;
	node->read = &serial_readfs;
	node->write = &serial_writefs;
	node->inode = port;
	node->open = 0;
	node->close = 0;
	node->flags = FS_FILE;
	
	return node;
}

void device_init(int id) {
	if(id == 0 || serial_comports[id].address == 0)
		return;

	int port = serial_comports[id].address;
	
	outportb(port + 1, 0x00); // We just want the data interrupt :)
	outportb(port + 3, 0x80); // Enable DLAB
	outportb(port + 0, 0x01); // Set baud rate to 115200 (lo)
	outportb(port + 1, 0x00); // (hi)
	outportb(port + 3, 0x03);
	outportb(port + 2, 0xC7); // Enable FIFO and clear
	outportb(port + 4, 0x0B); // IRQs enabled, RTS/DSR set
	outportb(port + 1, 0x01); // We just want the data interrupt :)
	
	serial_comports[id].buffer = fifo_create(SERIAL_FIFO_SIZE, 0);
}

void serial_readbda() {
	uint16_t *bda = (uint16_t *) 0x00000400;
	serial_comports[0].address = *bda; 			// COM1
	serial_comports[1].address = *(bda + 1);	// COM2
	serial_comports[2].address = *(bda + 2);	// COM3
	serial_comports[3].address = *(bda + 3);	// COM4
}

void serial_init() {
	// Read Bios Data Area for COM ports
	serial_readbda();
	
	// Init serial ports
    device_init(0);
    device_init(1);
    device_init(2);
    device_init(3);
   
	interrupt_install_handler(3, serialport_handler_24);
	interrupt_install_handler(4, serialport_handler_13);
}