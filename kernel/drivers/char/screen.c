#include <system.h>

uint16_t *textmemptr = (uint16_t *) 0xB8000;
uint16_t attrib = 0x07;
uint16_t csr_x = 0, csr_y = 0;

void move_cursor() {
   uint16_t cursor_location = csr_y * 80 + csr_x;
   outportb(0x3D4, 14);                  
   outportb(0x3D5, (cursor_location >> 8) & 0xFF); 
   outportb(0x3D4, 15);                
   outportb(0x3D5, (cursor_location) & 0xFF);
}

void screen_change_color(uint8_t fg, uint8_t bg) {
	attrib = (bg << 4) | (fg & 0x0F);
}

void screen_clear() {
	csr_x = 0;
	csr_y = 0;

	move_cursor();

	uint32_t i = 0;
	unsigned blank = 0x20 | (attrib << 8);
	for(; i < 80 * 25; i++)
		textmemptr[i] = blank;
}

void screen_scroll() {
	unsigned blank, temp;
	
	blank = 0x20 | (attrib << 8);
	
	if(csr_y >= 25) {
		temp = csr_y - 24;
		
		memcpy(textmemptr, (void *) (textmemptr + temp * 80), (25 - temp) * 80 * 2);
		memsetw(textmemptr + (25 - temp) * 80, blank, 80);
		
		csr_y = 24;
	}
}

uint16_t screen_get_x() {
	return csr_x;
}

uint16_t screen_get_y() {
	return csr_y;
}

void screen_move_cursor_to(uint16_t x, uint16_t y) {
	csr_x = x;
	csr_y = y;
	move_cursor();
}

void putch(char c) {
    uint16_t *where;
    unsigned att = attrib << 8;

    if(c == '\b') {
        if(csr_x != 0) {
			where = textmemptr + (csr_y * 80 + csr_x - 1);
			*where = ' ' | att;	
			csr_x--;
		}
    } else if(c == '\t') {
        csr_x = (csr_x + 8) & ~(8 - 1);
    } else if(c == '\r') {
        csr_x = 0;
    } else if(c == '\n') {
        csr_x = 0;
        csr_y++;
    } else if(c >= ' ') {
        where = textmemptr + (csr_y * 80 + csr_x);
        *where = c | att;	
        csr_x++;
    }

    if(csr_x >= 80) {
        csr_x = 0;
        csr_y++;
    }
	
	screen_scroll();
	move_cursor();
}

void screen_writeline(char *text) {
	screen_write(text);
	putch('\n');
}

void screen_write(char *text) {
    uint32_t i = 0;
    while(text[i] != '\0')
    	putch(text[i++]);
}

void screen_writel(char *src, uint32_t len) {
	while(len > 0) {
		putch(*src++);
		len--;
	}
}

void kprintf(const char *str, ...) {
	va_list args;
	va_start(args, str);
	
	uint32_t i = 0;
	for(; str[i] != '\0'; i++) {
		if(str[i] != '%') {
			putch(str[i]);
		} else {
			i++;
			switch(str[i]) {
				case 's':
					screen_write((char *) va_arg(args, char *));
					break;
					
				case 'x':
					screen_write_hex((uint64_t) va_arg(args, uint64_t));
					break;
					
				case 'd':
					screen_write_dec((uint64_t) va_arg(args, uint64_t));
					break;
				
				case 'c':
					putch((char) va_arg(args, int));
					break;
				
				case '%':
					putch('%');
					break;
				
				default:
					putch(str[i]);
					break;
			}
		}
	}
	
	va_end(args);
}

void screen_write_hex(uint32_t val) {
	if(val == 0) {
		putch('0');
		return;
	}

	uint16_t tmp;
	char noZeroes = 1;

	int32_t i = 28;
	for(; i >= 0; i -= 4) {
		tmp = (val >> i) & 0x0F;
		if(tmp == 0 && noZeroes != 0)
			continue;
		
		noZeroes = 0;
		if(tmp >= 0x0A)
			putch(tmp - 0x0A + 'A');
		else
			putch(tmp + '0');
	}
}

void screen_write_dec(uint32_t val) {
	if(val == 0) {
		putch('0');
		return;
	}

	uint32_t v = val / 10;
	uint32_t n = val % 10;
	
	if(n < 0) {
		n--;
		v += 10;
	}
	
	if(val >= 10)
		screen_write_dec(v);
	
	putch(n + '0');
}

uint32_t stdoutfs_write(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
    uint32_t i = 0;
    while(i < size) {
    	putch(buffer[i]);
    	i++;
    }

    return size;
}

fs_node_t *stdout_fs_init() {
    fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
    node->gid = 0;
    node->length = 0;
    node->impl = 0;
    node->readdir = 0;
    node->finddir = 0;
    node->read = 0;
    node->write = &stdoutfs_write;
    node->inode = 0;
    node->open = 0;
    node->close = 0;
    node->flags = FS_FILE | FS_CHARDEVICE;
    node->references = 1;

    return node;
}