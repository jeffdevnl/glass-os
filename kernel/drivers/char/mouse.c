#include <system.h>

fifo_t *mouse_pipe = NULL;

int32_t mouse_x = 0;
int32_t mouse_y = 0;

int8_t mouse_byte[] = { 0, 0, 0 };
uint8_t mouse_cycle = 0;

void mouse_wait_data() {
	int32_t i = 0;
	for(; i < (1000 & ((inportb(0x64) & 0x01) == 0x01)); i++);
}

void mouse_wait_signal() {
	int32_t i = 0;
	for(; i < (1000 & ((inportb(0x64) & 0x02) != 0x00)); i++);
}

void mouse_write(uint8_t data) {
	mouse_wait_signal();
	outportb(0x64, 0xD4);
	mouse_wait_signal();
	outportb(0x60, data);
}

uint8_t mouse_read() {
	mouse_wait_data();
	return inportb(0x60);
}

void mouse_irq_handler(registers_t *regs) {
	if(mouse_cycle == 0) {
		mouse_byte[0] = inportb(0x60);
		if((mouse_byte[0] & 0x08) == 0x08)
			mouse_cycle++;
	} else if(mouse_cycle == 1) {
		mouse_byte[1] = inportb(0x60);
		mouse_cycle++;
	} else if(mouse_cycle == 2) {
		mouse_byte[2] = inportb(0x60);
		mouse_cycle = 0;

		mouse_x += mouse_byte[1];
		mouse_y -= mouse_byte[2];

		// Write to pipe
		if(mouse_pipe) {
			mouse_packet_t packet;
			packet.x = mouse_x;
			packet.y = mouse_y;
			packet.button = mouse_byte[0];
			fifo_write(mouse_pipe, &packet, sizeof(mouse_packet_t));
		}
	}
}

void mouse_install() {
	mouse_wait_signal();
	outportb(0x64, 0xA8);

	mouse_wait_signal();
	outportb(0x64, 0x20);
	mouse_wait_data();

	uint8_t status = inportb(0x60) | 0x02;
	mouse_wait_signal();
	outportb(0x64, 0x60);
	mouse_wait_signal();
	outportb(0x60, status);

	mouse_write(0xF6);
	mouse_read();
	mouse_write(0xF4);
	mouse_read();

	interrupt_install_handler(12, mouse_irq_handler);
}

uint32_t mousefs_read(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
    return fifo_read(mouse_pipe, buffer, size);
}

uint32_t mousefs_write(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
    mouse_packet_t *packet = (mouse_packet_t *) buffer;
    mouse_x = packet->x;
    mouse_y = packet->y;
    mouse_byte[0] = packet->button;

    return fifo_write(mouse_pipe, buffer, size);
}

fs_node_t *mouse_pipe_init() {
    mouse_pipe = fifo_create(MOUSE_PIPE_SIZE, 0);

    fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
    node->gid = 0;
    node->length = MOUSE_PIPE_SIZE;
    node->impl = 0;
    node->readdir = 0;
    node->finddir = 0;
    node->read = &mousefs_read;
    node->write = &mousefs_write;
    node->inode = 0;
    node->open = 0;
    node->close = 0;
    node->flags = FS_FILE | FS_CHARDEVICE;
    node->references = 1;

    return node;
}