#include <system.h>

uint8_t caps = 0;
uint8_t shift = 0;
uint8_t leds = 0;
uint8_t readchar;
volatile uint8_t readingchar = 0;

char *get_keys_buffer;
uint8_t get_keys_amount;
uint8_t get_keys_max;
char get_keys_return;

void (*handler)(char) = NULL;
fifo_t *keyboard_pipe = NULL;

uint8_t kbdus[128] = {
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
    '9', '0', '-', '=', '\b',	/* Backspace */
    '\t',			/* Tab */
    'q', 'w', 'e', 'r',	/* 19 */
    't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
    '\'', '`',   0,		/* Left shift */
    '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
    'm', ',', '.', '/',   0,				/* Right shift */
    '*',
    0,	/* Alt */
    ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
    '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
    '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

uint8_t kbsdus[128] = {
    0,  27, '!', '@', '#', '$', '%', '^', '&', '*',	/* 9 */
    '(', ')', '_', '+', '\b',	/* Backspace */
    '\t',			/* Tab */
    'Q', 'W', 'E', 'R',	/* 19 */
    'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
    'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':',	/* 39 */
    '\"', '~',   0,		/* Left shift */
    '|', 'Z', 'X', 'C', 'V', 'B', 'N',			/* 49 */
    'M', '<', '>', '?',   0,				/* Right shift */
    '*',
    0,	/* Alt */
    ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
    '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
    '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

uint8_t transform_key(uint8_t charcode) {
    uint8_t keycode;
    if(shift)
        keycode = kbsdus[charcode];
    else
        keycode = kbdus[charcode];

    if(caps) {
        if(keycode >= 'a' && keycode <= 'z') {
            keycode = ('A' + keycode - 'a');
        } else if(keycode >= 'A' && keycode <= 'Z') {
            keycode = (keycode - 'A') + 'a';
        }
    }

    return keycode;
}

void setleds() {
    outportb(0x60, 0xED);
    while(inportb(0x64) & 2);
    outportb(0x60, leds);
    while(inportb(0x64) & 2);
}

void keyboard_handler(registers_t *r) {
    uint8_t scancode = inportb(0x60);

    // Get some information
    if(scancode & 0x80) {
        if(scancode == 0xAA)
            shift &= 0x02;
		else if(scancode == 0xB6)
			shift &= 0x01;
    } else {
        readchar = transform_key(scancode);
        readingchar = 0; 
        if(scancode == 0x3A) {
            if(caps) {
                caps = 0;
                leds = 0;
            } else {
                caps = 1;
                leds = 4;
            }

            setleds();
        } else if(scancode == 0x2A)
            shift |= 0x01;
		else if(scancode == 0x36)
			shift |= 0x02;

        if(readchar > 0) {
            // Call handler
            if(handler)
                (*handler)(readchar);

            // Write character
            if(keyboard_pipe) {
                if(!handler)
                    putch(readchar);

                fifo_write_byte(keyboard_pipe, readchar);
            }
        }
    }
}

void install_keyboard() {
	interrupt_install_handler(1, keyboard_handler);
}

uint32_t keyboardfs_read(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
    return fifo_read(keyboard_pipe, buffer, size);
}

uint32_t keyboardfs_write(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
    return fifo_write(keyboard_pipe, buffer, size);
}

fs_node_t *keyboard_pipe_init() {
    keyboard_pipe = fifo_create(KEYBOARD_PIPE_SIZE, 1);

    fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
    node->gid = 0;
    node->length = KEYBOARD_PIPE_SIZE;
    node->impl = 0;
    node->readdir = 0;
    node->finddir = 0;
    node->read = &keyboardfs_read;
    node->write = &keyboardfs_write;
    node->inode = 0;
    node->open = 0;
    node->close = 0;
    node->flags = FS_FILE | FS_CHARDEVICE;
    node->references = 1;

    stdin = node;

    return node;
}

char get_char() {
	readingchar = 1;
	while(readingchar)
        __asm__ __volatile__("hlt");
	
	return readchar;
}

char get_chard() {
	return readchar;
}

void get_keys_handler(char c) {
    if(c == '\b') {
        if(get_keys_amount > 0) {
            putch('\b');
            get_keys_buffer[get_keys_amount] = '\0';
            get_keys_amount--;
        }
    } else if(c == '\n' || c =='\r') {
        putch('\n');
        get_keys_return = 1;
    } else {
        putch(c);

        if(get_keys_amount < get_keys_max) {
            get_keys_buffer[get_keys_amount] = c;
            get_keys_amount++;
        }
    }
}

uint32_t get_keys(char *buffer, uint32_t max) {
    get_keys_buffer = buffer;
    get_keys_amount = 0;
    get_keys_max = max;
    get_keys_return = 0;
    handler = get_keys_handler;

    while(get_keys_return == 0 && get_keys_amount < get_keys_max)
        __asm__ __volatile__("hlt");

    get_keys_buffer[get_keys_amount] = '\0';
    handler = NULL;

    return get_keys_amount;
}