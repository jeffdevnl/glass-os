global paging_copy_page_phys
paging_copy_page_phys:
	push ebx
	pushf
	cli

	; Source address
	mov ebx, [esp + 12]
	; Destination address
	mov ecx, [esp + 16]

	; Temp disable paging
	mov edx, cr0
	and edx, 0x7FFFFFFF
	mov cr0, edx

	; Copy 1024 times 4 bytes
	mov edx, 1024
.loop:
	mov eax, [ebx]
	mov [ecx], eax
	add ebx, 4
	add ecx, 4
	dec edx
	jnz .loop
.finish:
	; Enable paging again
	mov edx, cr0
	or edx, 0x80000000
	mov cr0, edx

	popf
	pop ebx
	ret