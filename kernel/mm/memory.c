#include <system.h>

void memcpy(void *dest, void *src, size_t count) {
    // Something to copy?
    if(count == 0)
        return;

    // Copy sizeof(uint64_t) bytes per loop
    while(count >= sizeof(uint64_t)) {
        *(uint64_t *)dest = *(uint64_t *)src;

        dest += sizeof(uint64_t);
        src += sizeof(uint64_t);
        count -= sizeof(uint64_t);
    }

    // Copy sizeof(uint32_t) bytes per loop
    while(count >= sizeof(uint32_t)) {
        *(uint32_t *)dest = *(uint32_t *)src;

        dest += sizeof(uint32_t);
        src += sizeof(uint32_t);
        count -= sizeof(uint32_t);
    }

    // Copy sizeof(uint16_t) bytes per loop
    while(count >= sizeof(uint16_t)) {
        *(uint16_t *)dest = *(uint16_t *)src;

        dest += sizeof(uint16_t);
        src += sizeof(uint16_t);
        count -= sizeof(uint16_t);
    }

    // Copy sizeof(uint8_t) byte per loop
    while(count >= sizeof(uint8_t)) {
        *(uint8_t *)dest = *(uint8_t *)src;

        dest += sizeof(uint8_t);
        src += sizeof(uint8_t);
        count -= sizeof(uint8_t);
    }
}

int memcmp(void *a, void *b, size_t count) {
    uint8_t *a1 = (uint8_t *)a;
    uint8_t *b1 = (uint8_t *)b;

    uint32_t index = 0;
    while(index < count) {
        if(a1[index] > b1[index])
            return -1;
        else if(b1[index] > a1[index])
            return 1;
        else if(a1[index] == '\0' && b1[index] == '\0')
            return 0;

        index++;
    }

    return 0;
}

void memset(void *dest, const uint8_t val, size_t count) {
    __asm__ __volatile__("cld; rep stosb" : "+c" (count), "+D" (dest) : "a" (val) : "memory");
}

void memsetw(void *dest, const uint16_t val, size_t count) {
    __asm__ __volatile__("cld; rep stosw" : "+c" (count), "+D" (dest) : "a" (val) : "memory");
}

void memsetd(void *dest, const uint32_t val, size_t count) {
    __asm__ __volatile__("cld; rep stosl" : "+c" (count), "+D" (dest) : "a" (val) : "memory");
}