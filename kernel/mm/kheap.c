#include <system.h>

#define HEAP_BLOCKS			0x10000
#define HEAP_INITIAL_SIZE	32		// Initial heap size in MB

uint32_t heap_end;
uint32_t heap_start;
uint32_t block_location;
memory_block_t *blocks;

uint32_t malloc_func(int32_t size, int32_t align, uint32_t *physical) {
	if(size <= 0)
		return (uint32_t) NULL;

	// Search for a hole that's big enough
	uint32_t index = 0;
	while(index < HEAP_BLOCKS) {
		// Unused block?
		if(!blocks[index].used) {
			uint32_t new_size = 0;
			uint32_t placement_address;

			// Get the maximum size for the block
			if(index > 0 && index < HEAP_BLOCKS - 1 && blocks[index - 1].used && blocks[index + 1].used) {
				uint32_t previousAddress = blocks[index - 1].address;
				uint32_t nextAddress = blocks[index + 1].address;

				new_size = nextAddress - previousAddress;
			} else {
				if(blocks[index].size == 0) {
					// Not used before
					new_size = size;
				} else {
					// The block was used before, use old size
					new_size = blocks[index].size;
				}
			}

			// Big enough for our allocation request?
			if(size <= new_size) {
				// Was there a preceding element?
				if(index > 0) {
					// Then the address is: previous_address + previous_size
					placement_address = blocks[index - 1].address + blocks[index - 1].size;
				} else {
					// The first index is placed at the heap start
					placement_address = heap_start;
				}

				// Check alignment stuff
				if(align && (placement_address & 0xFFFFF000)) {
					// Align the address and size
					placement_address &= 0xFFFFF000;
					placement_address += 0x1000;

					// Does it still fit?
					if(index < HEAP_BLOCKS - 1 && placement_address + new_size > blocks[index + 1].address && blocks[index + 1].used) {
						index++;
						continue;
					}
				}

				// Register block
				blocks[index].address = placement_address;
				blocks[index].size = new_size;
				blocks[index].used = 1;

				// Check if the heap size should get increased
				if(placement_address + new_size >= heap_end)
					sbrk((placement_address + new_size) - heap_end);

				// Map physical address?
				if(physical)
					*physical = blocks[index].address;

				// We got everything we need
				// return the address
				return blocks[index].address;
			}
		}

		index++;
	}

	// No more free blocks
	return (uint32_t) NULL;
}

void free(void *ptr) {
	if(ptr == NULL)
		return;

	uint32_t address = (uint32_t) ptr;

	uint32_t index = 0;
	while(index < HEAP_BLOCKS) {
		// Found the corresponding block?
		if(blocks[index].address == address) {
			blocks[index].used = 0;

			// Done
			return;
		}

		index++;
	}
}

uint32_t malloc(int size) {
	return malloc_func(size, 0, NULL);
}

uint32_t malloc_a(int size) {
	return malloc_func(size, 1, NULL);
}

uint32_t malloc_p(int size, uint32_t *physical) {
	return malloc_func(size, 0, physical);
}

uint32_t malloc_ap(int size, uint32_t *physical) {
	return malloc_func(size, 1, physical);
}

void sbrk(uint32_t increment) {
	if(increment % 0x1000 != 0) {
		increment &= 0xFFFFF000;
		increment += 0x1000;
	}
	debug_print(DEBUG_INFO, "sbrk: increasing heap size with %x", increment);

	// New end
	uint32_t address = heap_end;
	heap_end += increment;

	// Only alloc page frames when paging is initialized
	extern uint8_t paging_initialized;
	if(!paging_initialized)
		return;

	uint32_t index = address;
	extern page_directory_t *current_directory;
	while(index < heap_end) {
		paging_alloc_frame(paging_get_page(index, 1, current_directory), 0, 1);
		index += 0x1000;
	}
}

void heap_install(uint32_t address) {
	// Set placement address and block information address
	heap_start = address;
	block_location = heap_start;

	// Prepare block information address
	uint32_t blocksize = sizeof(memory_block_t) * HEAP_BLOCKS;
	heap_start += blocksize;
	heap_end = heap_start + (1024 * 1024 * HEAP_INITIAL_SIZE);

	if(heap_end % 0x1000 != 0) {
		heap_end &= 0xFFFFF000;
		heap_end += 0x1000;
	}

	// Initialise memory blocks
	memset((void *) block_location, 0, blocksize);
	blocks = (memory_block_t *) block_location;
}