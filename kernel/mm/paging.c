#include <system.h>

page_directory_t *kernel_directory;
page_directory_t *current_directory;
uint32_t *frames;
uint32_t nframes;
uint8_t paging_initialized = 0;

#define INDEX_FROM_BIT(b)  (b / 0x20)
#define OFFSET_FROM_BIT(b) (b % 0x20)

void paging_pagefault(registers_t *r) {
	uint32_t address;
	__asm__ __volatile__("mov %%cr2, %0" : "=r" (address));

	// Get information of pagefault from the error code...
	uint8_t present		=	!(r->err_code & 1);
	uint8_t rw			=	r->err_code & 2;
	uint8_t user		=	r->err_code & 4;
	uint8_t reserved	=	r->err_code & 8;
	uint8_t id			=	r->err_code & 16;

	kprintf("\nPage fault:\n\nPresent: %d\nR/W: %d\nUser: %d\nReserved: %d\nID: %d\n@ %x\n\n", present, rw, user, reserved, id, address);

	if(current_directory == kernel_directory)
		kprintf("Caused by kernel_directory\n");

	for(;;);
}

void paging_set_frame(uint32_t frameAddress) {
	uint32_t frame = frameAddress / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);

	frames[index] |= (1 << offset);
}

uint32_t paging_test_frame(uint32_t frameAddress) {
	uint32_t frame = frameAddress / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);

	return (frames[index] & (1 << offset));
}

void paging_clear_frame(uint32_t frameAddress) {
	uint32_t frame = frameAddress / 0x1000;
	uint32_t index = INDEX_FROM_BIT(frame);
	uint32_t offset = OFFSET_FROM_BIT(frame);

	frames[index] &= ~(1 << offset);
}

uint32_t paging_first_frame() {
	uint32_t i = 0;
	uint32_t index = INDEX_FROM_BIT(nframes);
	
	for(; i < index; i++) {
		if(frames[i] != 0xFFFFFFFF) {
			uint32_t j = 0;
			for(; j < 0x20; j++) {
				if(!(frames[i] & (1 << j)))
					return (i * 0x20) + j;
			}
		}
	}

	PANIC("System claims to be out of memory!");
	
	return 0;
}

void paging_direct_mem_access(page_t* page, uint8_t kernel, uint8_t writable, uint32_t address) {
	page->present 	= 1;
	page->rw 		= writable;
	page->user 		= !kernel;
	page->frame 	= address / 0x1000;
}

void paging_alloc_frame(page_t *page, uint8_t kernel, uint8_t writable) {
	if(page->frame != 0) {
		page->present	= 1;
		page->rw		= writable;
		page->user		= !kernel;
	} else {
		uint32_t index = paging_first_frame();
		if(index == (uint32_t)-1) {
			PANIC("Out of frames!");
		}
		
		paging_set_frame(index * 0x1000);
		
		page->present	= 1;
		page->rw		= writable;
		page->user		= !kernel;
		page->frame		= index;
	}
}

void paging_free_frame(page_t *page) {
	uint32_t frame = page->frame;
	if(!frame)
		return;
	
	paging_clear_frame(frame * 0x1000);
	page->frame = 0;
}

uint32_t memory_total() {
	return nframes * 4;
}

uint32_t memory_used() {
	uint32_t ret = 0;
	uint32_t i = 0, index = INDEX_FROM_BIT(nframes);
	
	for(; i < index; i++) {
		uint32_t j = 0;
		for(; j < 0x20; j++) {
			if(frames[i] & (1 << j))
				ret++;
		}
	}
	
	return ret * 4;
}

page_t* paging_get_page(uint32_t address, uint8_t create, page_directory_t *dir) {
	address /= 0x1000;
	uint32_t table_index = address / 1024;
	
	if(dir->tables[table_index]) {
		return &dir->tables[table_index]->pages[address % 1024];
	} else if(create) {
		uint32_t temp;
		
		dir->tables[table_index] = (page_table_t *) malloc_ap(sizeof(page_table_t), &temp);
		memset(dir->tables[table_index], 0, sizeof(page_table_t));

		// Set it as: PRESENT, RW, USER
		dir->tables_physical[table_index] = temp | 0x07;
		
		return &dir->tables[table_index]->pages[address % 1024];
	}
	
	return NULL;
}

void paging_invalidate(uint32_t addr) {
	__asm__ __volatile__("invlpg (%0)" :: "r" (addr) : "memory");
}

void paging_map(uint32_t phys, uint32_t virt, uint8_t kernel, uint8_t writable, page_directory_t *dir) {
	phys /= 0x1000;
	virt /= 0x1000;

	// Calculate indexes in tables
	uint32_t virt_table_index = virt / 1024;

	// No page table?
	if(!dir->tables[virt_table_index]) {
		uint32_t temp;
		
		dir->tables[virt_table_index] = (page_table_t *) malloc_ap(sizeof(page_table_t), &temp);
		memset(dir->tables[virt_table_index], 0, sizeof(page_table_t));

		// Set it as: PRESENT, RW, USER
		dir->tables_physical[virt_table_index] = temp | 0x07;
	}

	// Get page table
	page_table_t *table = dir->tables[virt_table_index];

	// Get page
	page_t *page = &table->pages[virt % 1024];

	// Uh oh! Page already present!
	if(page->present) {
		debug_print(DEBUG_WARNING, "Virtual memory already mapped, wants: virt=%x phys=%x", virt * 0x1000, phys * 0x1000);
		return;
	}

	page->present = 1;
	page->rw = writable;
	page->user = !kernel;
	page->frame = phys;

	//paging_invalidate(virt * 0x1000);
}

uint32_t paging_get_phys_from_virt(uint32_t virt) {
	uint32_t frame = virt / 0x1000;
	uint32_t table_index = frame / 1024;

	if(current_directory->tables[table_index]) {
		page_table_t *table = current_directory->tables[table_index];
		page_t *page = &table->pages[frame % 1024];
		return page->frame * 0x1000 + (virt % 0x1000);
	}

	return 0;
}

void paging_switch_directory(page_directory_t *dir) {
	current_directory = dir;
	__asm__ __volatile__("mov %0, %%cr3" :: "r" (dir->tables_physical));
	paging_enable();
}

void paging_disable() {
	uint32_t cr0;

	__asm__ __volatile__("mov %%cr0, %0" : "=r" (cr0));
	cr0 &= ~(1 << 31);
	__asm__ __volatile__("mov %0, %%cr0" :: "r" (cr0));
}

void paging_enable() {
	uint32_t cr0;

	__asm__ __volatile__("mov %%cr0, %0" : "=r" (cr0));
	cr0 |= 1 << 31;
	__asm__ __volatile__("mov %0, %%cr0" :: "r" (cr0));
}

page_table_t *paging_clone_table(page_table_t *src, uint32_t *phys) {
	page_table_t *table = (page_table_t *) malloc_ap(sizeof(page_table_t), phys);
	memset(table, 0, sizeof(page_table_t));

	uint32_t i = 0;
	while(i < 1024) {
		page_t *page_src = &src->pages[i];
		if(!page_src->present) {
			i++;
			continue;
		}

		page_t *page_dest = &table->pages[i];
		paging_alloc_frame(page_dest, 0, 0);

		page_dest->present = page_src->present;
		page_dest->rw = page_src->rw;
		page_dest->user = page_src->user;
		page_dest->accessed = page_src->accessed;
		page_dest->dirty = page_src->dirty;

		paging_copy_page_phys(page_src->frame * 0x1000, page_dest->frame * 0x1000);

		i++;
	}

	return table;
}

page_directory_t *paging_clone_directory(page_directory_t *dir) {
	uint32_t phys;
	page_directory_t *dest = (page_directory_t *) malloc_ap(sizeof(page_directory_t), &phys);
	memset(dest, 0, sizeof(page_directory_t));

	uint32_t i = 0;
	while(i < 1024) {
		if(!dir->tables[i]) {
			i++;
			continue;
		}

		if(kernel_directory->tables[i] == dir->tables[i]) {
			dest->tables[i] = dir->tables[i];
			dest->tables_physical[i] = dir->tables_physical[i];
		} else {
			uint32_t phys_table;
			dest->tables[i] = paging_clone_table(dir->tables[i], &phys_table);
			dest->tables_physical[i] = phys_table | 0x07;
		}

		i++;
	}

	dest->physical_address = phys;
	return dest;
}

void paging_free_directory(page_directory_t *dir) {
	if(dir == kernel_directory)
		return;

	uint32_t i = 0;
	while(i < 1024) {
		if(!dir->tables[i]) {
			i++;
			continue;
		}

		page_table_t *table = dir->tables[i];
		if(kernel_directory->tables[i] != table) {
			uint32_t j = 0;
			while(j < 1024) {
				if(table->pages[j].present)
					paging_free_frame(&table->pages[j]);

				j++;
			}

			free(table);
		}

		i++;
	}
}

void paging_init(uint32_t mem) {
	nframes = mem / 4;
	frames = (uint32_t *) malloc(INDEX_FROM_BIT(nframes * 8));
	memset(frames, 0, INDEX_FROM_BIT(nframes * 8));

	uint32_t phys;
	kernel_directory = (page_directory_t *) malloc_ap(sizeof(page_directory_t), &phys);
	memset(kernel_directory, 0, sizeof(page_directory_t));

	// Heap and kernel etc.
	uint32_t i = 0;
	extern uint32_t heap_end;
	while(i < heap_end) {
		paging_alloc_frame(paging_get_page(i, 1, kernel_directory), 0, 1);
		i += 0x1000;
	}

	kernel_directory->physical_address = (uint32_t) kernel_directory->tables_physical;
	paging_initialized = 1;
	paging_switch_directory(kernel_directory);
}