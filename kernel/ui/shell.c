#include <system.h>

typedef struct {
	char *command;
	void *pointer;
	char *help;
} shell_command_t;

char *user;
char *hostname;

int8_t run_program(char *filepath, int argc, char **argv) {
	FILE *file = fopen(filepath);
	if(file != NULL) {
		// Get file size
		fseek(file, 0, SEEK_END);
		uint32_t size = ftell(file);
		fseek(file, 0, SEEK_SET);
		
		// Allocate a buffer for the file
		uint8_t *buf = (uint8_t *) malloc(size);
		fread(buf, size, file);

		// TODO: this is an ugly workaround to clear the keyboard buffer...
		//		 because in the shell, we "abuse" the keyboard,
		//		 this needs to be fixed when the keyboard is read by a program
		//		 this fix will not be needed anymore when the shell is a program
		uint8_t *temp = (uint8_t *) malloc(1);
		extern fifo_t *keyboard_pipe;
		keyboard_pipe->wait = 0;
		while(fs_read(stdin, 0, 1, temp));
		keyboard_pipe->wait = 1;
		free(temp);
		// ---
		
		uint32_t pid;
		
		if((pid = loader_exec(buf, argc, argv)) == 0) { 
			return 0;
		} else {
			// NOTE: it is not possible to use HLT in usermode
			while(tasking_running(pid));
			
			return 1;
		}
		
		free(buf);
		fclose(file);
	} else {
		return -1;
	}
}

void command_pwd(int argc, char **argv) {
	kprintf("%s\n", get_current_dir_name());
}

void command_whoami(int argc, char **argv) {
	kprintf("%s\n", user);
}

void command_ls(int argc, char **argv) {
	char *dirname = get_current_dir_name();
	kprintf("Listing contents of %s:\n\n", dirname);
	
	// Get the directory
	DIR *directory = opendir(dirname);
	if(directory == NULL) {
		kprintf("FATAL: can't read directory.\n");
		return;
	}

	int colw = 20;
	int leftpad = (80 / colw) * 2;

	// Loop
	direntry_t *entry;
	uint8_t cleared = 0;
	while((entry = readdir(directory)) != NULL) {
		// Yellow	symlink / mountpoint
		// Green	directories
		// Red		files
		// Blue		char devices

		if(entry->type == DT_LINK)
			screen_change_color(CONSOLE_YELLOW, CONSOLE_BLACK);
		else if(entry->type == DT_DIR)
			screen_change_color(CONSOLE_GREEN, CONSOLE_BLACK);
		else if(entry->type == DT_CHR)
			screen_change_color(CONSOLE_LIGHTCYAN, CONSOLE_BLACK);
		else
			screen_change_color(CONSOLE_RED, CONSOLE_BLACK);

		// Left padding and write name
		for(int i = 0; i < leftpad; i++)
			putch(' ');
		screen_write(entry->name);


		// How much space is left?
		if(screen_get_x() + colw >= 80) {
			putch('\n');
			cleared = 1;
		} else {
			cleared = 0;
			int to = colw - strlen(entry->name);
			for(int i = 0; i < to; i++)
				putch(' ');
		}
	}

	// Close
	closedir(directory);

	screen_change_color(CONSOLE_LIGHTGRAY, CONSOLE_BLACK);
	putch('\n');
	if(!cleared)
		putch('\n');
}

void command_mem_total(int argc, char **argv) {
	kprintf("%d MB\n", memory_total() / 1024 / 1024);
}

void command_write(int argc, char **argv) {
	if(argc <= 2) {
		kprintf("Usage: write filename message\n");
		return;
	}

	char *filename = argv[1];
	char *filepath = get_path(filename);
	
	FILE *file = fopen(filepath);
	if(file != NULL) {
		int length = argc - 1;
		int i;
		
		for(i = 2; i < length; i++) {
			uint32_t size = strlen(argv[i]) + 2;
			uint8_t *buf = (uint8_t *) malloc(size);
			strcpy((char *)buf, argv[i]);

			buf[size - 2] = ' ';
			buf[size - 1] = '\0';
			fwrite(buf, size, file);
			
			free(buf);
		}
	
		uint32_t size = strlen(argv[length]) + 1;
		uint8_t *buf = (uint8_t *) malloc(size);
		buf[size - 1] = '\0';
		strcpy((char *)buf, argv[length]);
		fwrite(buf, size, file);
		
		free(buf);
		fclose(file);
	} else {
		kprintf("File not found.\n");
	}
	
	free(filepath);
}

void command_cd(int argc, char **argv) {
	if(argc <= 1) {
		kprintf("Usage: cd dirname\n");
		return;
	}

	char *new_dir = argv[1];
	int8_t status = chdir(new_dir);
	if(status != 0) {
		kprintf("%s: no such directory\n", new_dir);
		return;
	}
}

void command_time(int argc, char **argv) {
	kprintf("%d:%d:%d\n", cmos_get_real(CMOS_HOURS), cmos_get_real(CMOS_MINUTES), cmos_get_real(CMOS_SECONDS));
}

void command_echo(int argc, char **argv) {
	if(argc <= 1) {
		kprintf("Usage: echo message\n");
		return;
	}
	
	uint32_t length = argc - 1;
	uint32_t i;
	for(i = 1; i <= length; i++)
		kprintf("%s ", argv[i]);
	
	putch('\n');
}

void command_reboot(int argc, char **argv) {
	acpi_reset();
}

void command_clear(int argc, char **argv) {
	screen_clear();
}

void command_tasks(int argc, char **argv) {
	tasking_print();
}

void command_taskkill(int argc, char **argv) {
	if(argc <= 1) {
		kprintf("Usage: taskkill pid\n");
		return;
	}

	char *pidc = argv[1];
	if(!isi(pidc)) {
		kprintf("PID must be an number.\n");
		return;
	}
	
	uint32_t pid = atoi(pidc);
	if(tasking_exit(pid))
		kprintf("Task with PID %d killed\n", pid);
	else
		kprintf("Could not kill task with PID %d\n", pid);
}

void command_help(int argc, char **argv);
uint32_t shell_commands = 13;
shell_command_t shell_command[] = {
	{ "cd",			&command_cd,		"Changes current working directory" },
	{ "clear",		&command_clear,		"Clears the screen" },
	{ "echo",		&command_echo,		"Prints the given text" },
	{ "help",		&command_help,		"Displays the help" },
	{ "ls",			&command_ls,		"Lists the current directory" },
	{ "memtot",     &command_mem_total, "Displays the systems total available memory" },
	{ "pwd",		&command_pwd,		"Shows the current working directory" },
	{ "reboot",		&command_reboot,	"Reboots the system" },
	{ "tasks",		&command_tasks,		"Displays a list of tasks" },
	{ "taskkill",	&command_taskkill,	"Kills a task with given pid" },
	{ "time",		&command_time,		"Displays the current time" },
	{ "whoami",		&command_whoami,	"Shows your username" },
	{ "write",		&command_write,		"Write given string to file" }
};

void command_help(int argc, char **argv) {
	uint32_t i = 0;
	while(i < shell_commands) {
		shell_command_t cmd = shell_command[i];

		kprintf("  %s", cmd.command);
		screen_move_cursor_to(30, screen_get_y());
		kprintf("%s\n", cmd.help);

		i++;
	}
}

void prompt_command() {
	// Buffers for commands
	char *command;
	char buffer[1024];

	// Welcome message
	kprintf("Welcome %s, you are on %s!\n", user, hostname);
	kprintf("Type help for a list of commands.\n\n");

	// Keep requesting commands
	while(1) {
		// Prompt
		screen_change_color(CONSOLE_RED, CONSOLE_BLACK);
		screen_write(user);
		screen_change_color(CONSOLE_DARKGRAY, CONSOLE_BLACK);
		putch('@');
		screen_change_color(CONSOLE_CYAN, CONSOLE_BLACK);
		screen_write(hostname);
		screen_change_color(CONSOLE_LIGHTGRAY, CONSOLE_BLACK);
		kprintf(" %s", get_current_dir_name());
		screen_change_color(CONSOLE_GREEN, CONSOLE_BLACK);
		kprintf("$ ");
		screen_change_color(CONSOLE_LIGHTGRAY, CONSOLE_BLACK);
		
		// Get command
		char *buffer_pointer = (char *) &buffer;
		uint32_t size = get_keys(buffer_pointer, 1023);
		if(size > 0) {
			int argc = str_count(buffer_pointer, ' ') + 1;
			
			char **argv = (char **) malloc(sizeof(char *) * argc);
			if(argc == 1) {
				command = buffer_pointer;
			} else {
				command = strtok(buffer_pointer, ' ');
			}
			
			argv[0] = command;

			uint32_t i;
			for(i = 1; i < argc; i++)
				argv[i] = strtok(argv[i - 1], ' ');

			for(i = 0; i < argc; i++)
				argv[i] = strdup(argv[i]);

			i = 0;
			uint8_t found = 0;

			// Look for an internal command with that name
			while(i < shell_commands) {
				if(strcmp(command, shell_command[i].command) == 0) {
					void *func = shell_command[i].pointer;
					typedef void (*shell_cmd) (int argc, char **argv);
					((shell_cmd) func)(argc, argv);
					
					found = 1;
					break;
				}

				i++;
			}

			// No internal command found with that name? Try a program.
			if(!found) {
				// Search for a file named like this in the current directory
				char *path = get_path(command);
				int8_t status = run_program(path, argc, argv);
				
				if(status != 1) {
					// Try /initrd/bin/*command*
					
					free(path);
					path = get_path_absolute("/initrd/bin", command);
					status = run_program(path, argc, argv);
					
					if(status != 1) {
						// No, nothing found that matches this name
						kprintf("%s: Command not found\n", command);
					}
				}
				
				free(path);
			}
			
			for(uint32_t i = 0; i < argc; i++)
				free(argv[i]);
			free(argv);
		}
	}
}

int8_t shell_login() {
	// TODO: implement "x" as password looking in /etc/shadow
	// TODO: use scanf (?)

	char *login_file_location = "initrd/etc/passwd";
	FILE *login_file = fopen(login_file_location);

	// Uh oh, no login file??
	if(!login_file)
		return 1;

	// Use this for later
	uint32_t username_length;
	uint32_t input_length;
	char *username;
	char *password;

	// Input buffer to store inputed data
	char input[1024];
	char *input_pointer = (char *)&input;

	// Get total file size
	fseek(login_file, 0, SEEK_END);
	uint32_t size = ftell(login_file);
	fseek(login_file, 0, SEEK_SET);

	// Empty?
	if(size == 0) {
		fclose(login_file);
		return 1;
	}

	// Read in
	char *buffer = (char *) malloc(size + 1);
	fread(buffer, size, login_file);
	buffer[size] = '\0';

	// Structure of one line:
	// username:password:uid:gid:commentfield:home:command (command, eg. shell)

	// How many users?
	uint32_t user_count = str_count(buffer, '\n') + 1;

	// Create temporary buffer
	char *buffer_temp = (char *) malloc(size + 1);

	uint8_t correct = 0;
	while(!correct) {
		// Get username
		kprintf("Username: ");
		get_keys(input_pointer, 1023);
		input_length = strlen(input_pointer);
		username = (char *)malloc(input_length + 1);
		strcpy(username, input_pointer);
		username[input_length] = '\0';
		username_length = strlen(username);

		// Get password
		kprintf("Password: ");
		get_keys(input_pointer, 1023);
		input_length = strlen(input_pointer);
		password = (char *)malloc(input_length + 1);
		strcpy(password, input_pointer);
		password[input_length] = '\0';

		// Encrypt password
		char *encryped_password = sha256_encrypt(password);

		// Setup temporary buffer
		memcpy(buffer_temp, buffer, size);
		buffer_temp[size] = '\0';

		uint32_t i = 0;
		while(i < user_count) {
			// Only one line, that means no newline!
			char *line = (user_count == 1) ? buffer_temp : strtok(buffer_temp, '\n');

			// Get username length
			uint32_t i = 0;
			uint32_t max = strlen(line);

			// Max username length is 64 in a passwd file
			for(; i < 64 && i < max; i++) {
				if(line[i] == ':' || line[i] == '\0')
					break;
			}

			// Is this NOT the user we need?
			if(strncmp(line, username, i) != 0)
				break;

			// Loop through data
			line = strtok(line, ':'); // Username
			char *read_password = strtok(line, ':');

			// Convert to uppercase
			uint32_t k = 0;
			for(; k < 64; k++)
				read_password[k] = toupper(read_password[k]);

			if(strcmp(encryped_password, read_password) == 0) {
				correct = 1;
				break;
			}

			i++;
		}

		if(!correct) {
			kprintf("Username / password mismatch\n\n");
			free(username);
			free(password);
		}
		
		free(encryped_password);
	}

	// Frees!
	free(buffer_temp);

	// Store username
	user = (char *) malloc(username_length + 1);
	strcpy(user, username);
	user[username_length] = '\0';

	// Free stuff
	fclose(login_file);
	free(buffer);
	free(username);
	free(password);

	return !correct;
}

void start_shell()
{
	// Get hostname
	char *hostname_file_location = "initrd/etc/hostname";
	FILE *hostname_file = fopen(hostname_file_location); 
	if(hostname_file == NULL) {
		screen_change_color(CONSOLE_RED, CONSOLE_BLACK);
		kprintf("/%s does not exist!", hostname_file_location);
		screen_change_color(CONSOLE_LIGHTGRAY, CONSOLE_BLACK);

		hostname = strdup("local");
	} else {
		// Get file length
		fseek(hostname_file, 0, SEEK_END);
		uint32_t size = ftell(hostname_file);
		fseek(hostname_file, 0, SEEK_SET);

		// Allocate place for hostname
		hostname = (char *) malloc(size + 1);
		fread((void *)hostname, size, hostname_file);
		hostname[size] = '\0';

		fclose(hostname_file);
	}
	
	// Login
	int8_t status = shell_login();

	// No problem?
	if(status == 0) {
		// Ask for commands
		screen_clear();
		prompt_command();
	}
	
	free(hostname);
	free(user);
}