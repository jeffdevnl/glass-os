#include <system.h>

void assert_failure(char *file, uint32_t line, char *description) {
	__asm__ __volatile__("cli");

	kprintf("ASSERTION FAILURE!\n\n");
	kprintf("Error message:\t%s\n", description);
	kprintf("File:\t\t%s\n", file);
	kprintf("Line:\t\t%d\n\n", line);
	kprintf("System has now halted.");

	while(1)
		__asm__ __volatile("hlt");
}

void panic(char *file, uint32_t line, char *description) {
	__asm__ __volatile__("cli");

	screen_change_color(CONSOLE_WHITE, CONSOLE_RED);
	screen_clear();
	
	screen_move_cursor_to(3, 1);
	kprintf("     |                                 \n   ,---.|    ,---.,---.,---.    ,---.,---.\n   |   ||    ,---|`---.`---.    |   |`---.\n   `---|`---'`---^`---'`---'    `---'`---'\n   `---'                                  ");
	screen_move_cursor_to(3, 7);
	kprintf("KERNEL PANIC!");
	screen_move_cursor_to(3, 9);
	kprintf("The system will hang now because it has encountered an unexpected problem.\n\n");
	
	screen_move_cursor_to(3, 15);
	kprintf("Error message:\t%s", description);
	screen_move_cursor_to(3, 16);
	kprintf("File:\t\t%s", file);
	screen_move_cursor_to(3, 17);
	kprintf("Line:\t\t%d", line);

	while(1)
		__asm__ __volatile("hlt");
}