#include <system.h>

// Structure of a TSS entry
typedef struct tss_entry_struct {
    uint32_t prev_tss;      // The previous TSS
    uint32_t esp0;          // The stack pointer of RING0
    uint32_t ss0;           // The Stack Segment of RING0
    uint32_t esp1;          // The stack pointer of RING1
    uint32_t ss1;           // The Stack Segment of RING1
    uint32_t esp2;          // The stack pointer of RING2
    uint32_t ss2;           // The Stack Segment of RING2
    uint32_t cr3;           // CR3 prior to switching
    uint32_t eip;           // EIP prior to switching
    uint32_t eflags;        // EFlags prior to switching
    uint32_t eax;           // EAX prior to switching
    uint32_t ecx;           // ECX prior to switching
    uint32_t edx;           // EDX prior to switching
    uint32_t ebx;           // EBX prior to switching
    uint32_t esp;           // ESP prior to switching
    uint32_t ebp;           // EBP prior to switching
    uint32_t esi;           // ESI prior to switching
    uint32_t edi;           // EDI prior to switching
    uint32_t es;            // The ES to load
    uint32_t cs;            // The CS to load
    uint32_t ss;            // The SS to load
    uint32_t ds;            // The DS to load
    uint32_t fs;            // The FS to load
    uint32_t gs;            // The GS to load
    uint32_t ldt;           // LDT prior to switching
    uint16_t trap;          // Set if switch should cause a Debug Exception
    uint16_t iomap;         // Offset in structure to IOMAP
} __attribute__((packed)) tss_entry_t;

// One GDT entry structure
typedef struct gdt_entry {
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t  base_middle;
    uint8_t  access;
    uint8_t  granularity;
    uint8_t  base_high;
} __attribute__((packed)) gdt_entry_t;

// GDT pointer structure
typedef struct {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed)) gdt_ptr_t;

// 6 GDT entries
gdt_entry_t gdt[6];
gdt_ptr_t   gptr;

// One TSS entry to switch to kernel mode from software interrupt
tss_entry_t tss_entry;

/**
 *
 * Sets a GDT entry
 * @param num the gdt entry number
 * @param base the base
 * @param limit the limit
 * @param access access mode
 * @param granularity the granularity
 *
**/
void gdt_set_entry(int32_t num, uint64_t base, uint64_t limit, uint8_t access, uint8_t granularity) {
    // Set address
    gdt[num].base_low    = (base & 0xFFFF);
    gdt[num].base_middle = (base >> 16) & 0xFF;
    gdt[num].base_high   = (base >> 24) & 0xFF;

    // Set descriptor limit
    gdt[num].limit_low    = (limit & 0xFFFF);
    gdt[num].granularity  = ((limit >> 16) & 0x0F);

    // Set granularity and access
    gdt[num].granularity |= (granularity & 0xF0);
    gdt[num].access       = access;
}

/**
 *
 * Writes a TSS into a GDT entry and sets the TSS entry
 * @param num the index of the GDT entry
 * @param tss the TSS
 * @param ss the kernel Stack Selector
 *
**/
void write_tss(int32_t num, tss_entry_t *tss, uint16_t ss) {
    // Calculate base and limit of this entry
    uint32_t base  = (uint32_t) tss;
    uint32_t limit = base + sizeof(tss_entry_t);

    // Clear out the TSS
    memset(tss, 0, sizeof(tss_entry_t));

    // Set stack selector and stack
    tss->ss0  = ss;
    tss->esp0 = 0;

    // Set IOMAP
    tss->iomap = sizeof(tss_entry_t);

    // Set selectors
    // Kernel DS = 0x10 and CS = 0x08
    tss->cs = 0x08;
    tss->ds = 0x10;
    tss->es = 0x10;
    tss->fs = 0x10;
    tss->gs = 0x10;
    tss->ss = 0x10;

    // Add the TSS descriptor to the GDT
    gdt_set_entry(num, base, limit,
                                    // Access
                                    GDT_DATA_EA                 |
                                    GDT_DESCRIPTOR_TYPE(0)      |
                                    GDT_PRIVILEGE(3)            |
                                    GDT_PRESENT(1),

                                    // Granularity
                                    0
                                    );
}

/**
 *
 * Flushes a TSS entry
 * @param index the index of the GDT entry of this TSS
 * @param privilege the privilege level
 *
**/
void tss_flush(int32_t index, uint8_t privilege) {
    // LTR ((index * 8) | privilege)
    // Why 8? Each entry of GDT is 8 bytes long
    uint16_t ax = (index * 8) | privilege;

    // Use inline ASM to do the LTR
    asm volatile("ltr %%ax" : : "a" (ax) : "memory");
}

/**
 *
 * Sets a new location for the TSS0 (kernel) stack
 * @param stack the stack location
 *
**/
void set_kernel_stack(uintptr_t stack) {
   tss_entry.esp0 = stack;
}

/**
 *
 * Install the Global Descriptor Table
 *
**/
void gdt_install() {
    // Configure GDT pointer
    gptr.limit = (6 * sizeof(gdt_entry_t)) - 1;
    gptr.base  = (uintptr_t) &gdt;

    // NULL segment
    gdt_set_entry(0, 0, 0, 0, 0);

    // Kernel Code Segment
    gdt_set_entry(1, 0, 0xFFFFFFFF,
                                    // Access
                                    GDT_DATA_ER                 |
                                    GDT_DESCRIPTOR_TYPE(1)      |
                                    GDT_PRIVILEGE(0)            |
                                    GDT_PRESENT(1),

                                    // Granularity
                                    GDT_SIZE(1)                 |
                                    GDT_GRANULARITY(1)
                                    );

    // Kernel Data Segment
    gdt_set_entry(2, 0, 0xFFFFFFFF,
                                    // Access
                                    GDT_DATA_RW                 |
                                    GDT_DESCRIPTOR_TYPE(1)      |
                                    GDT_PRIVILEGE(0)            |
                                    GDT_PRESENT(1),

                                    // Granularity
                                    GDT_SIZE(1)                 |
                                    GDT_GRANULARITY(1)
                                    );

    // User Code Segment
    gdt_set_entry(3, 0, 0xFFFFFFFF,
                                    // Access
                                    GDT_DATA_ER                 |
                                    GDT_DESCRIPTOR_TYPE(1)      |
                                    GDT_PRIVILEGE(3)            |
                                    GDT_PRESENT(1),

                                    // Granularity
                                    GDT_SIZE(1)                 |
                                    GDT_GRANULARITY(1)
                                    );

    // User Data Segment
    gdt_set_entry(4, 0, 0xFFFFFFFF,
                                    // Access
                                    GDT_DATA_RW                 |
                                    GDT_DESCRIPTOR_TYPE(1)      |
                                    GDT_PRIVILEGE(3)            |
                                    GDT_PRESENT(1),

                                    // Granularity
                                    GDT_SIZE(1)                 |
                                    GDT_GRANULARITY(1)
                                    );

    // TSS
    write_tss(5, &tss_entry, 0x10);

    // Apply the GDT
    gdt_flush((uintptr_t) &gptr);

    // Write the TSS (index 5, privilege level 3)
    tss_flush(5, 3);
}