#include <system.h>

void gpf_handler(registers_t *r) {
	kprintf("GENERAL PROTECTION FAULT OCCURED\n");
	kprintf("ERRCODE: %d\n", r->err_code);
	kprintf("EBP: %x\tESI: %x\n", r->ebp, r->esi);
	kprintf("EBX: %x\tEAX: %x\n", r->ebx, r->eax);
	kprintf("EDX: %x\tECX: %x\n", r->edx, r->ecx);
	kprintf("DS:  %x\tFS:  %x\n", r->ds, r->fs);
	kprintf("GS:  %x\tES:  %x\n", r->gs, r->es);
	
	for(;;);
}

void gpf_install_handler() {
	interrupt_install_handler(0x0D, gpf_handler);
}