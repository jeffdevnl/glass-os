#include <system.h>

struct idt_entry {
    uint16_t base_lo;
    uint16_t sel;
    uint8_t  zero;
    uint8_t  flags;
    uint16_t base_hi;
} __attribute__((packed));

struct idt_ptr {
    uint16_t limit;
    uint32_t base;
} __attribute__((packed));

struct idt_entry idt[256];
struct idt_ptr idtptr;

extern void idt_flush(uintptr_t idt_pointer); 

void idt_set_gate(uint8_t num, uint64_t base, uint8_t sel, uint8_t flags) {
	idt[num].base_lo = (base & 0xFFFF);
	idt[num].base_hi = ((base >> 16) & 0xFFFF);
	idt[num].zero = 0;
	idt[num].sel = sel;
	idt[num].flags = flags | 0x60;
}

void idt_install() {
	idtptr.limit = (sizeof(struct idt_entry) * 256) - 1;
    idtptr.base = (uint32_t) &idt;
	
    memset(&idt, 0, (uint32_t)(sizeof(struct idt_entry) * 256));
	
    idt_flush((uintptr_t) &idtptr);
}