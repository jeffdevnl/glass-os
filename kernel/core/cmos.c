#include <system.h>

uint8_t cmos_get_in_progress_flag() {
	outportb(cmos_address, CMOS_SRA);
	return (inportb(cmos_data) & 0x80);
}

uint8_t cmos_get(uint8_t reg) {
	while(cmos_get_in_progress_flag());
	outportb(cmos_address, reg);
	return inportb(cmos_data);
}

uint8_t cmos_get_real(uint8_t reg) {
	uint8_t value = cmos_get(reg);
	
	if (!(cmos_get(CMOS_SRB) & 0x04)) {
		if(reg == CMOS_SECONDS) value = BDC_TO_DECIMAL(value);
		else if(reg == CMOS_MINUTES) value = BDC_TO_DECIMAL(value);
		else if(reg == CMOS_HOURS) value = ( (value & 0x0F) + (((value & 0x70) / 16) * 10) ) | (value & 0x80);
		else if(reg == CMOS_WEEKDAY) value = BDC_TO_DECIMAL(value);
		else if(reg == CMOS_MONTH) value = BDC_TO_DECIMAL(value);
		else if(reg == CMOS_YEAR) value = BDC_TO_DECIMAL(value);
	}
	
	return value;
}