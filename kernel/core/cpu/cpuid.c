#include <system.h>

char *cpuid_get_brand() {
	uint32_t ebx, garbage;

	cpuid(0, garbage, ebx, garbage, garbage);

	char *ret;
	switch(ebx) {
		case CPUID_INTEL_MAGIC:
			ret = "Intel";
			break;

		case CPUID_AMD_MAGIC:
			ret = "AMD";
			break;

		default:
			ret = "Unknown";
			break;
	}

	return ret;
}