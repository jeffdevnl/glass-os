#include <system.h>

#define PIT_A           0x40
#define PIT_B           0x41
#define PIT_C           0x42
#define PIT_CONTROL     0x43
#define PIT_MASK        0xFF
#define PIT_SCALE       1193180
#define PIT_SET         0x36

volatile uint64_t timer_ticks = 0;

uint32_t timer_handler(registers_t *r) {
    timer_ticks++;
    return tasking_switch_task((uint32_t) r);
}

void timer_phase(int32_t hertz) {
    int32_t divisor = PIT_SCALE / hertz;

    outportb(PIT_CONTROL, PIT_SET);
    outportb(PIT_A, divisor & PIT_MASK);
    outportb(PIT_A, (divisor >> 8) & PIT_MASK);
}

void timer_install() {
    // There's a custom interrupt handler for this because this is also used by tasking
    //interrupt_install_handler(0, timer_handler);
    timer_phase(100);
}

void timer_wait(uint64_t ticks) {
    uint64_t eticks = timer_ticks + ticks;
    while(timer_ticks < eticks)
        __asm__ __volatile__("hlt");
}