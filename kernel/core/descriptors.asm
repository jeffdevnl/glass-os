global gdt_flush
global idt_flush

; GDT
gdt_flush:
	; Pointer to GDT passed as parameter to this function
	mov eax, [esp + 4]
	lgdt [eax]

	; Set segments
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	jmp 0x08:.flush2
.flush2:
	ret

; IDT
idt_flush:
	; Pointer to IDT passed as parameter to this function
	mov eax, [esp + 4]
	lidt [eax]

	ret