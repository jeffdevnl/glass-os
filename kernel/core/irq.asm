extern timer_handler
extern irq_handler

irq_common_stub:
    pusha
    push ds
    push es
    push fs
    push gs

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    mov eax, esp
    push eax
    mov eax, irq_handler
    call eax

    pop eax
    pop gs
    pop fs
    pop es
    pop ds
    popa

    add esp, 8
    iret

; Custom IRQ0 (timer) handler
global irq0
irq0:
	cli
    pusha
    push ds
    push es
    push fs
    push gs

    mov al, 0x20
    out 0x20, al

    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    ; Task switching
    mov eax, esp
    push eax
    call timer_handler
    mov esp, eax

    pop gs
    pop fs
    pop es
    pop ds
    popa

    iret

global get_esp
get_esp:
    mov eax, esp
    ret

%macro IRQ 2
    global irq%1
    irq%1:
        cli
        push byte 0
        push byte %2
        jmp irq_common_stub

%endmacro

IRQ 1,  33
IRQ 2,  34
IRQ 3,  35
IRQ 4,  36
IRQ 5,  37
IRQ 6,  38
IRQ 7,  39
IRQ 8,  40
IRQ 9,  41
IRQ 10, 42
IRQ 11, 43
IRQ 12, 44
IRQ 13, 45
IRQ 14, 46
IRQ 15, 47