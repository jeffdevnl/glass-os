global switch_to_user_mode
switch_to_user_mode:
	; Get return address
	mov eax, [esp]
	mov [.save_eax], eax

	mov ax, 0x23	; User mode data selector is 0x20, also sets RPL -> 3
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	push dword 0x23	; SS Selector
	push esp		; ESP
	pushfd			; EFlags
	push dword 0x1B	; CS, user mode selector is 0x23, with RPL3 it's 0x1B
	lea eax, [.a]	; EIP to jump to
	push eax

	iretd
.a:
	add esp, 4		; Fix the stack
	
	pop eax
	or eax, 0x200
	push eax
	
	mov eax, [.save_eax]
	jmp eax
.save_eax dd 0