#include <system.h>

#define SYS_EXIT				0x01
#define SYS_PRINT				0x04

void sys_printf(registers_t *r) {
	// EBX = Descriptor
	// ECX = String
	// EDX = Length
	screen_writel((char *)r->ecx, r->edx);
}

void sys_exit(registers_t *r) {
	tasking_exit(tasking_get_pid());
}

typedef void (*syscall_func)(registers_t *r);

syscall_func syscalls[] = {
	NULL,
	sys_exit,
	NULL,
	NULL,
	sys_printf
};

uint32_t syscalls_num = sizeof(syscalls) / sizeof(syscall_func) - 1;

void syscalls_handler(registers_t *r) {
	if(r->eax > syscalls_num)
		return;
		
	uint32_t *location = (uint32_t *)syscalls[r->eax];
	if (!location) {
		return;
	}
	
	syscall_func function = (syscall_func) location;
	function(r);
}

void syscalls_setup() {
	interrupt_install_handler(0x80, syscalls_handler);
}