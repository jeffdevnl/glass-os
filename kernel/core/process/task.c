#include <system.h>

task_t *current_task = NULL;
task_t *start_task = NULL;
node_t *current_task_node = NULL;
node_t *start_task_node = NULL;

uint32_t next_pid = 0;
list_t *task_list;

void tasking_init() {
	task_list = list_create();
}

void tasking_print() {
	kprintf("PID  |  PARENT  |  NAME\n");
	foreach_in_list(node, task_list) {
		task_t *task = (task_t *)node->value;

		kprintf("%d", task->pid);
		screen_move_cursor_to(8, screen_get_y());
		kprintf("%d", task->parent_pid);
		screen_move_cursor_to(19, screen_get_y());
		kprintf("%s\n", task->name);
	}
}

char *getwd(char *buf) {
	strcpy(buf, current_task->wd_name);
	return buf;
}

char *getcwd() {
	int size = strlen(current_task->wd_name);
	char *buf = (char *) malloc(size + 1);
	strcpy(buf, current_task->wd_name);
	
	return buf;
}

char *get_current_dir_name() {
	return current_task->wd_name;
}

task_t *get_current_task() {
	return current_task;
}

int chdir(char *path) {
	char *workdir = current_task->wd_name;
	uint32_t workdir_length = strlen(workdir);

	char *new_path;
	if(*path == '.' && *(path + 1) == '\0') {
		// Change to current dir
		return 0;
	} else if(strcmp(path, "..") == 0) {
		// Change to parent dir
		uint32_t slash_count = str_count(workdir, '/');
		uint32_t len = 0;
		while(slash_count > 0 && len < workdir_length) {
			if(workdir[len] == '/')
				slash_count--;
			
			len++;
		}

		len--;
		if(len == 0) {
			new_path = (char *) malloc(2);
			*new_path = '/';
			*(new_path + 1) = '\0';
		} else {
			new_path = (char *) malloc(len + 1);
			strncpy(new_path, workdir, len);
			new_path[len] = '\0';
		}
	} else {
		uint32_t path_length = strlen(path);
		if(strcmp(workdir, "/") == 0) {
			new_path = (char *) malloc(1 + path_length + 1);
			*new_path = '/';
			strcpy(new_path + 1, path);
		} else {
			new_path = (char *) malloc(workdir_length + 1 + path_length + 1);
			strcpy(new_path, workdir);
			*(new_path + workdir_length) = '/';
			strcpy(new_path + workdir_length + 1, path);
		}
	}

	// Well, does it exist? If so, get us the node
	fs_node_t *node = fs_node_open(new_path);
	if(node == NULL) {
		free(new_path);
		return ENOENT;
	}

	// What, this is not a folder??
	if((node->flags & FS_DIRECTORY) != FS_DIRECTORY) {
		// Cleanup
		free(new_path);
		fs_close(node);

		return ENOTDIR;
	}

	// Cleanup working directory node
	fs_close(current_task->wd_node);
	free(current_task->wd_name);

	// Update working directory and its node
	current_task->wd_name = new_path;
	current_task->wd_node = node;

	return 0;
}

task_t *get_task_by_pid(uint32_t pid) {
	foreach_in_list(node, task_list) {
		task_t *task = (task_t *) node->value;
		if(task->pid == pid)
			return task;
	}

	return NULL;
}

uint32_t tasking_get_pid() {
	if(!start_task)
		return 0;

	return current_task->pid;
}

uint8_t tasking_running(uint32_t pid) {
	foreach_in_list(node, task_list) {
		task_t *task = (task_t *) node->value;
		if(task->pid == pid) {
			return 1;
		}
	}
	
	return 0;
}

void tasking_clean(task_t *task) {
	// Free cwd stuff
	free(task->wd_name);
	fs_close(task->wd_node);

	// Free memory if needed
	uint32_t *to_free = task->to_free;
	uint32_t count = task->free_count;
	for(uint32_t i = 0; i < count; i++)
		free((void *) to_free[i]);
	
	free(to_free);

	// Free memory
	if(task->initial_stack)
		free((uint32_t *) task->initial_stack);

	// Free page directory
	paging_free_directory(task->page_directory);
}

uint8_t tasking_exit(uint32_t pid) {
	// Can't exit system idle process
	if(pid == 0)
		return 0;

	// Get the node
	node_t *task_node = NULL;
	foreach_in_list(node, task_list) {
		task_t *task = (task_t *) node->value;
		if(task->pid == pid) {
			task_node = node;
			break;
		}
	}

	// Task does not exist?
	if(task_node == NULL)
		return 0;
	
	// Mark the task to remove it
	task_t *task = (task_t *) task_node->value;
	task->remove = 1;

	// Find child tasks
	uint8_t is_child = 0;
	foreach_in_list(node, task_list) {
		task_t *task = (task_t *) node->value;
		if(task->parent_pid == pid) {
			task->remove = 1;
			is_child = 1;
		}
	}

	// Halt process and wait until terminated
	debug_print(DEBUG_INFO, "Task \"%s\" with PID %d terminated", task->name, task->pid);
	if(current_task->pid == pid || is_child) {
		debug_print(DEBUG_INFO, "Current process terminated, waiting for switch", task->name, task->pid);

		// Current status is that the interrupts are cleared, restore them to allow a task switch
		__asm__ __volatile__("sti");

		// Wait until task switch
		while(1)
			__asm__ __volatile__("hlt");
	}

	return 1;
}

void tasking_prepare_fd(task_t *task) {
	// Allocate
	task->fd = (fd_table_t *) malloc(sizeof(fd_table_t));

	// Set
	task->fd->used = 0;
	task->fd->capacity = INITIAL_FD_CAPACITY;

	// Init entries
	task->fd->entries = (fs_node_t **) malloc(sizeof(fs_node_t *) * INITIAL_FD_CAPACITY);
	memset(task->fd->entries, 0, sizeof(fs_node_t *) * INITIAL_FD_CAPACITY);

	// Init entry offsets
	task->fd->entry_offsets = (uint32_t *) malloc(sizeof(uint32_t) * INITIAL_FD_CAPACITY);
	memset(task->fd->entry_offsets, 0, sizeof(uint32_t) * INITIAL_FD_CAPACITY);

	// STDIO
	task->fd->entries[STDOUT_FILENO] = stdout;
	task->fd->entries[STDIN_FILENO] = stdin;
	task->fd->used += 2;
}

task_t *tasking_create(char *name) {
	// Create the new task and its node
	task_t *task = (task_t *) malloc(sizeof(task_t));
	
	// Initialize structure
	task->pid = next_pid++;
	task->parent_pid = tasking_get_pid();

	// Note: page alloc stack because else it can get messed up
	task->initial_stack = malloc_a(INITIAL_STACK_SIZE);
	task->stack = task->initial_stack + INITIAL_STACK_SIZE;
	task->remove = 0;
	task->name = name;
	task->free_count = 0;

	extern page_directory_t *current_directory;
	task->page_directory = paging_clone_directory(current_directory);

	// Copy filesystem node
	fs_node_t *new_node = (fs_node_t *) malloc(sizeof(fs_node_t));
	memcpy(new_node, current_task->wd_node, sizeof(fs_node_t));
	new_node->references = 1;

	// Set filesystem current workdir stuff
	task->wd_name = strdup(current_task->wd_name);
	task->wd_node = new_node;

	// Prepare fd table
	tasking_prepare_fd(task);

	return task;
}

void tasking_init_stack(task_t *task, void *pointer, uint8_t usermode) {
	uint32_t *stack = (uint32_t *) task->stack;

	// Automatically pushed data
	*--stack = 0x202;					// Eflags
	*--stack = 0x08;					// CS
	*--stack = (uint32_t) pointer;		// EIP

	// Pushed by pusha
	*--stack = 0;						// EDI
	*--stack = 0;						// ESI
	*--stack = 0;						// EBP
	*--stack = 0;						// NULL
	*--stack = 0;						// EBX
	*--stack = 0;						// EDX
	*--stack = 0;						// ECX
	*--stack = 0;						// EAX

	// Data segments
	uint32_t segment = usermode ? 0x23 : 0x10;
	*--stack = segment;					// DS
	*--stack = segment;					// ES
	*--stack = segment;					// FS
	*--stack = segment;					// GS

	task->stack = (uint32_t) stack;
}

uint32_t tasking_add(task_t *task) {
	__asm__ __volatile__("cli");

	node_t *task_node = (node_t *) malloc(sizeof(node_t));

	// Prepare node and add node to list
	task_node->value = task;
	list_add_item(task_list, task_node);
	
	debug_print(DEBUG_INFO, "Task \"%s\" with PID %d created", task->name, task->pid);

	__asm__ __volatile__("sti");

	return task->pid;
}

uint32_t tasking_start(char *name, void *pointer, uint8_t usermode) {
	task_t *task = tasking_create(name);
	tasking_init_stack(task, pointer, usermode);
	return tasking_add(task);
}

uint32_t tasking_fork(char *name) {
	__asm__ __volatile__("cli");

	// Create the new task and its node
	task_t *task = (task_t *) malloc(sizeof(task_t));
	node_t *task_node = (node_t *) malloc(sizeof(node_t));

	uint8_t first_task = !start_task;

	// Initialize structure
	task->pid = next_pid++;
	task->parent_pid = tasking_get_pid();
	task->stack = get_esp();
	task->remove = 0;
	task->name = name;
	task->free_count = 0;

	extern page_directory_t *current_directory;
	task->page_directory = (!first_task) ? paging_clone_directory(current_directory) : current_directory;

	fs_node_t *fs_source;
	if(!start_task) {
		extern fs_node_t *fs_root;
		task->wd_name = strdup("/");
		fs_source = fs_root;
	} else {
		task->wd_name = strdup(current_task->wd_name);
		fs_source = current_task->wd_node;
	}

	fs_node_t *new_node = (fs_node_t *) malloc(sizeof(fs_node_t));
	memcpy(new_node, fs_source, sizeof(fs_node_t));
	new_node->references = 1;
	task->wd_node = new_node;

	// Prepare fd table
	tasking_prepare_fd(task);


	// Prepare node and add node to list
	task_node->value = task;
	list_add_item(task_list, task_node);


	// Check
	if(!start_task) {
		start_task = task;
		start_task_node = task_node;
		current_task = task;
		current_task_node = task_node;
	}

	__asm__ __volatile__("sti");

	return task->pid;
}

uint32_t tasking_switch_task(uint32_t stack) {
	// Don't switch tasks when we haven't initialised tasking yet
	if(!current_task)
		return stack;

	// Save stack of current task
	current_task->stack = stack;

	// Find next task
	while(1) {
		if(current_task_node->next)	current_task_node = current_task_node->next;
		else						current_task_node = start_task_node;

		current_task = current_task_node->value;

		// Remove?? Clean it up!
		if(current_task->remove) {
			free(current_task_node->value);
			list_delete_node(task_list, current_task_node);

			// Okay we can clean it up
			tasking_clean(current_task);
		} else {
			// Ah, we found a task!
			break;
		}
	}
	
	// Switch page directory
	paging_switch_directory(current_task->page_directory);

	// Switch stack to switch the context
	return current_task->stack;
}