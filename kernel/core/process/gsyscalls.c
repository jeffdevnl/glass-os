#include <system.h>

/**
 *
 * Powers off the system
 *
**/
void gsys_poweroff(registers_t *r) {
	acpi_shutdown();
}

/**
 *
 * Exits a process
 * EBX = exit code
 *
**/
void gsys_exit(registers_t *r) {
	tasking_exit(tasking_get_pid());
}

/**
 *
 * Prints a string
 * EBX = pointer to string
 *
**/
void gsys_print(registers_t *r) {
	char *str = (char *) r->ebx;
	screen_write(str);
}


/**
 *
 * Opens a filestream
 * EBX = Path to file
 *
**/
void gsys_fopen(registers_t *r) {
	char *relative_path = (char *) r->ebx;
	char *path = get_path(relative_path);
	r->eax = (uint32_t) fopen(path);
	free(path);
}

/**
 *
 * Closes a stream
 * EBX = stream
 * 
**/
void gsys_fclose(registers_t *r) {
	FILE *stream = (FILE *) r->ebx;
	fclose(stream);
}

/**
 *
 * Reads a file
 * EBX = buffer
 * ECX = size
 * EDX = stream
 *
 * RETURN = length
 * 
**/
void gsys_fread(registers_t *r) {
	FILE *stream = (FILE *) r->edx;
	char *buffer = (char *) r->ebx;
	size_t size = r->ecx;
	
	r->eax = fread(buffer, size, stream);
}

/**
 *
 * Writes a file
 * EBX = buffer
 * ECX = size
 * EDX = stream
 * 
**/
void gsys_fwrite(registers_t *r) {
	FILE *stream = (FILE *) r->edx;
	char *buffer = (char *) r->ebx;
	size_t size = r->ecx;
	
	fwrite((void *)buffer, size, stream);
}

/**
 *
 * Allocates memory
 * EBX = size
 *
 * RETURN = memory address
 *
**/
void gsys_malloc(registers_t *r) {
	r->eax = malloc(r->ebx);
}

/**
 *
 * Frees memory allocated with malloc
 * EBX = pointer
 *
**/
void gsys_free(registers_t *r) {
	// TODO: (maybe) check if we don't free another processes' memory?
	free((void *)r->ebx);
}

/**
 *
 * Read current working directory into buffer
 * EBX = pointer to buffer
 *
**/
void gsys_getwd(registers_t *r) {
	char *buf = (char *) r->ebx;
	getwd(buf);
}

/**
 *
 * Read current working directory
 * EBX = pointer to buffer
 *
 * RETURN = pointer to path
 *
**/
void gsys_getcwd(registers_t *r) {
	// TODO: getcwd is implemented wrongly!!!
	r->eax = (uint32_t) getcwd();
}

/**
 *
 * Change current working directory
 * EBX = Path
 *
 * RETURN = status
 *
**/
void gsys_chdir(registers_t *r) {
	char *path = (char *) r->eax;
	r->eax = chdir(path);
}

/**
 *
 * Puts a character on the screen
 * EBX = character
 *
**/
void gsys_putch(registers_t *r) {
	putch((char) r->ebx);
}

/**
 *
 * Calls ftell for a file
 * EBX = File stream
 *
 * RETURN = offset
 *
**/
void gsys_ftell(registers_t *r) {
	FILE *stream = (FILE *)r->ebx;
	r->eax = ftell(stream);
}

/**
 *
 * Calls fseek for a file
 * EBX = File stream
 * ECX = offset
 * EDX = origin
 *
 * RETURN = status
 *
**/
void gsys_fseek(registers_t *r) {
	FILE *stream = (FILE *)r->ebx;
	int32_t offset = r->ecx;
	int32_t origin = r->edx;

	r->eax = fseek(stream, offset, origin);
}

/**
 *
 * Request a video mode
 * EBX = mode number
 *
 * RETURN = lfb address
 *
**/
void gsys_request_video(registers_t *r) {
	r->eax = (uint32_t) vesa_set_mode(r->ebx, 1, 1);
}

/**
 *
 * Finds a video mode
 * EBX = min width
 * ECX = min height
 * EDX = max width
 * ESI = max height
 * EDI = min bbp
 *
 * RETURN = mode number
 *
**/
void gsys_find_video_mode(registers_t *r) {
	r->eax = (uint16_t) vesa_find_mode(r->ebx, r->ecx, r->edx, r->esi, r->edi);
}

fs_node_t *get_node_from_fd(int file) {
	// Check
	fd_table_t *fd = get_current_task()->fd;
	if(file >= fd->capacity)
		return NULL;

	// Get node
	fs_node_t *node = fd->entries[file];
	if(!node)
		return NULL;

	return node;
}

int stat_fs_node(fs_node_t *node, stat_t *stat) {
	stat->st_dev = 0;
	stat->st_inode = node->inode;
	stat->st_uid = node->uid;
	stat->st_gid = node->gid;
	stat->st_rdev = 0;
	stat->st_size = node->length;

	if((node->flags & FS_FILE) == FS_FILE)					stat->st_mode |= S_IFREG;
	if((node->flags & FS_DIRECTORY) == FS_DIRECTORY)		stat->st_mode |= S_IFDIR;
	if((node->flags & FS_BLOCKDEVICE) == FS_BLOCKDEVICE)	stat->st_mode |= S_IFBLK;
	if((node->flags & FS_CHARDEVICE) == FS_CHARDEVICE)		stat->st_mode |= S_IFCHR;
	if((node->flags & FS_SYMLINK) == FS_SYMLINK)			stat->st_mode |= S_IFLNK;

	// TODO: nlink, atime, mtime, ctime

	return 0;
}

/**
 *
 * fstat
 * EBX = file int
 * ECX = pointer to stat structure
 *
 * RETURN = errorcode
 *
**/
void gsys_fstat(registers_t *r) {
	// Get values
	fs_node_t *node = get_node_from_fd(r->ebx);
	stat_t *st = (stat_t *) r->ecx;

	// Check
	if(!node) {
		r->eax = 1;
		return;
	}

	// Do it!
	r->eax = stat_fs_node(node, st);
}

/**
 *
 * sbrk
 * EBX = increase amount
 *
 * RETURN = end of heap address
 *
**/
void gsys_sbrk(registers_t *r) {
	extern uint32_t heap_end;
	r->eax = heap_end;

	uint32_t incr = r->ebx;
	sbrk(incr);
}

/*
 *
 * get process id
 *
 * RETURN = process id
 *
**/
void gsys_getpid(registers_t *r) {
	r->eax = tasking_get_pid();
}

/**
 *
 * write to a fd
 * EBX = file descriptor
 * ECX = pointer to write buffer
 * EDX = length
 *
**/
void gsys_write(registers_t *r) {
	// Get values
	uint32_t file = r->ebx;
	uint32_t ptr = r->ecx;
	uint32_t len = r->edx;

	// Get values
	fs_node_t *node = get_node_from_fd(r->ebx);
	fd_table_t *fd = get_current_task()->fd;

	// Check
	if(!node) {
		r->eax = 1;
		return;
	}

	// Write!
	uint32_t ret = fs_write(node, fd->entry_offsets[file], len, (uint8_t *) ptr);

	// New offset
	fd->entry_offsets[file] += len;
	r->eax = ret;
}

/**
 *
 * read a fd
 * EBX = file descriptor
 * ECX = pointer to read buffer
 * EDX = length
 *
**/
void gsys_read(registers_t *r) {
	uint32_t file = r->ebx;
	uint32_t ptr = r->ecx;
	uint32_t len = r->edx;
	
	// Get values
	fs_node_t *node = get_node_from_fd(file);
	fd_table_t *fd = get_current_task()->fd;

	// Check
	if(!node) {
		r->eax = 1;
		return;
	}
	
	// Read!
	uint32_t ret = fs_read(node, fd->entry_offsets[file], len, (uint8_t *) ptr);

	// New offset
	fd->entry_offsets[file] += len;
	r->eax = ret;
}

/**
 *
 * Closes a fd
 * EBX = file descriptor
 *
**/
void gsys_close(registers_t *r) {
	// Get values
	fs_node_t *node = get_node_from_fd(r->ebx);

	// Check
	if(!node) {
		r->eax = 1;
		return;
	}

	// Clean this up
	fd_table_t *fd = get_current_task()->fd;
	fd->entries[r->ebx] = NULL;
	fd->entry_offsets[r->ebx] = 0;
	fd->used--;

	// Close
	fs_close(node);
}

/**
 *
 * Sets the position (offset) in a fd
 * EBX = file
 * ECX = offset
 * EDX = dir / whence
 *
 * RETURN = errorcode
 *
**/
void gsys_lseek(registers_t *r) {
	// Get values
	uint32_t file = r->ebx;
	uint32_t offset = r->ecx;
	uint32_t whence = r->edx;

	// Get the table
	fd_table_t *fd = get_current_task()->fd;

	// Check
	if(!fd->entries[file]) {
		r->eax = -1;
		return;
	}

	// Set
	if(whence == SEEK_SET) {
		fd->entry_offsets[file] = offset;
	} else if(whence == SEEK_CUR) {
		fd->entry_offsets[file] += offset;
	} else if(whence == SEEK_END) {
		fd->entry_offsets[file] = fd->entries[file]->length - offset;
	}

	// Success!
	r->eax = fd->entry_offsets[file];
}

/**
 *
 * stat
 * EBX = filename
 * ECX = pointer to stat structure
 *
 * RETURN VALUE = errorcode
 *
**/
void gsys_stat(registers_t *r) {
	// Get values
	char *filename = (char *) r->ebx;
	stat_t *st = (stat_t *) r->ecx;

	// Open
	char *path = get_path(filename);
	fs_node_t *node = fs_node_open(path);
	free(path);
	if(!node) {
		r->eax = 1;
		return;
	}

	// Stat
	r->eax = stat_fs_node(node, st);

	// Close
	fs_close(node);
}

/**
 *
 * Open a file using filename
 * EBX = filename
 * ECX = flags
 * EDX = mode
 *
 * RETURN VALUE = fd
 *
**/
void gsys_open(registers_t *r) {
	// TODO: implement create

	// Get the values
	char *filename = (char *) r->ebx;
	//int flags = r->ecx;
	//int mode = r->edx;

	fd_table_t *fd = get_current_task()->fd;

	// Do we even have a free fd?
	if(fd->used >= fd->capacity) {
		r->eax = 1;
		return;
	}

	// Get the node
	char *path = get_path(filename);
	fs_node_t *node = fs_node_open(path);
	free(path);
	if(!node) {
		r->eax = 2;
		return;
	}

	// Search free entry id
	uint32_t i = 0;
	while(i < fd->capacity) {
		if(fd->entries[i] == NULL)
			break;

		i++;
	}

	// Set entry
	fd->entries[i] = node;
	fd->entry_offsets[i] = 0;
	fd->used++;

	// Don't close because it will be used later!
	// Set return
	r->eax = i;
}


// Syscall lookup table
typedef void (*gsyscall_func)(registers_t *r);
gsyscall_func gsyscalls[] = {
	gsys_exit,					// 0
	gsys_poweroff,
	gsys_print,
	gsys_fopen,
	gsys_fread,					// 4
	gsys_fwrite,
	gsys_fclose,
	gsys_malloc,
	gsys_free,					// 8
	gsys_getwd,
	gsys_getcwd,
	gsys_chdir,
	gsys_putch,					// 12
	gsys_ftell,
	gsys_fseek,
	gsys_request_video,
	gsys_find_video_mode,		// 16
	gsys_fstat,
	gsys_sbrk,
	gsys_getpid,
	gsys_write,					// 20
	gsys_read,
	gsys_close,
	gsys_lseek,
	gsys_stat,					// 24
	gsys_open
};

uint32_t gsyscalls_num = sizeof(gsyscalls) / sizeof(gsyscall_func);

void gsyscalls_handler(registers_t *r) {
	if(r->eax >= gsyscalls_num)
		return;
	
	gsyscall_func function = (gsyscall_func) gsyscalls[r->eax];
	if(!function)
		return;
	
	function(r);
}

void gsyscalls_setup() {
	interrupt_install_handler(0x7F, gsyscalls_handler);
}