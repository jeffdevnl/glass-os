#include <system.h>

loader_t **loaders;
uint32_t last_loader;

void loader_init() {
	loaders = (loader_t **) malloc(sizeof(loader_t) * LOADER_MAX);
	memset(loaders, 0, sizeof(loader_t) * LOADER_MAX); // Make sure it's empty
}

void loader_register(loader_t *loader) {
	if(last_loader + 1 > LOADER_MAX) {
		kprintf("No more loaders available.\n");
		return;
	}
	
	loaders[last_loader] = loader;
	last_loader++;
}

uint8_t loader_exec(uint8_t *buffer, int argc, char **argv) {
	uint32_t i = 0;
	
	while(i < LOADER_MAX) {
		if(!loaders[i]) break; // U_U
		
		loader_t *load = loaders[i];
		uint8_t probe = load->probe(buffer);
		
		if(probe) 
			return load->start(buffer, argc, argv);
		
		i++;
	}
	
	return 0;
}