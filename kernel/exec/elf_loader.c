#include <system.h>

uint8_t elfloader_probe(uint8_t *buf) {
	// Is it an ELF file?
	return (buf[0] == 0x7F && buf[1] == 'E' && buf[2] == 'L' && buf[3] == 'F');
}

uint8_t elfloader_start(uint8_t *buf, int argc, char **argv) {
	elf_header_t *header = (elf_header_t *) buf;
	uint32_t elf_addr = (uint32_t) header;
	uint32_t string_table = 0;

	// Is it marked as executable?
	if(header->e_type != ELF_EXEC) {
		kprintf("File is not executable!\n");
		return 0;
	}

	// Is it 32bit?
	if(header->e_ident[EI_CLASS] != ELF_CLASS_32) {
		kprintf("Not a valid 32bit ELF file!\n");
		return 0;
	}

	// Correct machine type?
	if(header->e_machine != ELF_MACH_386) {
		kprintf("This file is not for a intel 386!\n");
		return 0;
	}

	// Correct ELF version?
	if(header->e_version != ELF_VER_CURRENT) {
		kprintf("This file has another ELF version!\n");
		return 0;
	}

	// Now we can load it, copy the argc and argv
	char **argv_copy = (char **) malloc(sizeof(char *) * argc);
	for(uint32_t i = 0; i < argc; i++)
		argv_copy[i] = strdup(argv[i]);

	// Create the new task structure
	task_t *task = tasking_create(argv_copy[0]);
	page_directory_t *pdir = task->page_directory;

	// Things needed for virtual memory stuff
	uint32_t start_address = 0;
	uint32_t least_virtual_address = 0;
	uint32_t total_size = 0;

	// Find out total size and start address
	for(uint32_t i = 0; i < header->e_shnum; i++) {
		elf_section_header_t *section = (elf_section_header_t *) (elf_addr + (i * header->e_shentsize + header->e_shoff));

		// Loadable?
		if(section->sh_addr) {
			if(!start_address)
				start_address = section->sh_addr - section->sh_offset;

			if(least_virtual_address == 0 || least_virtual_address > section->sh_addr)
				least_virtual_address = section->sh_addr;

			total_size = section->sh_addr + section->sh_size - start_address;
		}

		// Also try to find the string table
		if(section->sh_type == SHT_STRTAB && i == header->e_shstrndx)
			string_table = elf_addr + section->sh_offset;
	}

	// Total size should be "page aligned"
	if(total_size % 0x1000 != 0) {
		total_size &= 0xFFFFF000;
		total_size += 0x1000;
	}

	// Allocate memory for a list of addresses that need to be freed
	uint32_t *to_free = (uint32_t *) malloc(sizeof(uint32_t) * (argc + 2));

	// Please give me some memory for the ELF
	uint32_t placement_address = malloc_a(total_size);

	// Find entrypoint and copy sections
	// If there's a .init section, use that as entrypoint
	uint32_t entrypoint = 0;
	uint32_t virtual_entrypoint = 0;
	uint32_t init = 0;
	for(uint32_t i = 0; i < header->e_shnum; i++) {
		elf_section_header_t *section = (elf_section_header_t *) (elf_addr + (i * header->e_shentsize + header->e_shoff));

		// Loadable?
		if(section->sh_addr) {
			// Get the name
			char *section_name = (char *) (string_table + section->sh_name);

			// Location of this section in physical memory
			uint32_t section_mem = placement_address + section->sh_offset;

			// Executable code?
			if(section->sh_type == SHT_PROGBITS) {
				// .init? We found our entrypoint!
				if(strcmp(section_name, ".init") == 0)
					init = section_mem;

				// Store the lowest entry point of the ELF or .init
				if(entrypoint == 0 || section->sh_addr < virtual_entrypoint) {
					entrypoint = section_mem;
					virtual_entrypoint = section->sh_addr;
				}
			}

			// .BSS? Make it all zero
			if(section->sh_type == SHT_NOBITS)
				memset((void *) section_mem, 0, section->sh_size);
			else
				memcpy((void *) section_mem, (void *) (elf_addr + section->sh_offset), section->sh_size);
		}
	}

	// Let us map that memory
	for(uint32_t x = 0; x < total_size; x += 0x1000)
		paging_map(placement_address + x, least_virtual_address + x, 0, 1, pdir);

	// Also add the addresses of argv and address of argv itself
	for(uint32_t i = 0; i < argc; i++)
		to_free[i] = (uint32_t) argv_copy[i];

	to_free[argc] = (uint32_t) argv_copy;
	to_free[argc + 1] = placement_address;

	// Does the ELF contain a .init ?
	if(init)
		entrypoint = init;

	// Set freeing stuff
	task->to_free = to_free;
	task->free_count = argc;
	
	// argc, argv
	uint32_t *stack = (uint32_t *) task->stack;
	*--stack = (uint32_t) argv_copy;
	*--stack = argc;
	task->stack = (uint32_t) stack;

	// Update stack with entrypoint and other data
	tasking_init_stack(task, (void *) entrypoint, 1);

	// Finally, schedule the task
	return tasking_add(task);
}

void elfloader_init() {
	static loader_t loader;
	loader.probe = (void *) elfloader_probe;
	loader.start = (void *) elfloader_start;
	
	loader_register(&loader);
}