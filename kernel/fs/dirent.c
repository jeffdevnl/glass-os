#include <system.h>

#define CHECK_IS_DIR_PTR(d)		(((__dirstream_t *) d)->magic == DIR_MAGIC)

DIR *opendir(const char *path) {
	// Get the node
	fs_node_t *dir_node = fs_node_open(path);
	if(!dir_node)
		return NULL;

	// Allocate space
	__dirstream_t *stream = (__dirstream_t *) malloc(sizeof(__dirstream_t));

	// Count nodes
	direntry_t *node;
	uint32_t entries_count = 0;
	while((node = fs_readdir(dir_node, entries_count++)) != NULL);
	entries_count--;

	// Set options
	stream->magic = DIR_MAGIC;
	stream->entries_count = entries_count;
	stream->offset = 0;
	stream->node = dir_node;
	stream->entries = (direntry_t **) malloc(sizeof(direntry_t *) * entries_count);

	// Set entries
	entries_count = 0;
	while((node = fs_readdir(dir_node, entries_count++)) != NULL) {
		fs_node_t *lookup_node = fs_finddir(dir_node, node->name);

		if((lookup_node->flags & FS_MOUNTPOINT) == FS_MOUNTPOINT)
			node->type = DT_LINK;
		else if((lookup_node->flags & FS_DIRECTORY) == FS_DIRECTORY)
			node->type = DT_DIR;
		else if((lookup_node->flags & FS_CHARDEVICE) == FS_CHARDEVICE)
			node->type = DT_CHR;
		else if((lookup_node->flags & FS_FILE) == FS_FILE)
			node->type = DT_REG;

		stream->entries[entries_count - 1] = node;
		fs_close(lookup_node);
	}

	// Opaque data type please
	return (DIR *) stream;
}

int closedir(DIR *dp) {
	if(!CHECK_IS_DIR_PTR(dp))
		return EINVAL;

	// No opaque data type please
	__dirstream_t *stream = (__dirstream_t *) dp;

	// Goodbye entries
	uint64_t count = stream->entries_count;
	for(uint64_t i = 0; i < count; i++)
		free(stream->entries[i]);
	
	// Cleanup the node
	free(stream->entries);
	fs_close(stream->node);

	return 0;
}

direntry_t *readdir(DIR *dp) {
	if(!CHECK_IS_DIR_PTR(dp))
		return NULL;

	// No opaque data type please
	__dirstream_t *stream = (__dirstream_t *) dp;

	// Valid cursor?
	if(stream->offset < 0 || stream->offset >= stream->entries_count)
		return NULL;

	return stream->entries[stream->offset++];
}

void rewinddir(DIR *dp) {
	if(!CHECK_IS_DIR_PTR(dp))
		return;

	// No opaque data type please
	__dirstream_t *stream = (__dirstream_t *) dp;

	// Let's get us back to the start!
	stream->offset = 0;
}

void seekdir(DIR *dp, int64_t offset) {
	if(!CHECK_IS_DIR_PTR(dp))
		return;

	// No opaque data type please
	__dirstream_t *stream = (__dirstream_t *) dp;

	// Set
	if(offset >= 0 && offset < stream->entries_count)
		stream->offset = offset;
}

int64_t telldir(DIR *dp) {
	if(!CHECK_IS_DIR_PTR(dp))
		return 0;

	// No opaque data type please
	__dirstream_t *stream = (__dirstream_t *) dp;

	return stream->offset;
}