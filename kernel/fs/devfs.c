#include <system.h>

list_t *devices_list;

direntry_t *devfs_readdir(fs_node_t *node, uint32_t index) {
	if(index >= devices_list->length)
		return NULL;

	node_t *vfs_list_node = list_get(devices_list, index);
	if(!vfs_list_node)
		return NULL;

	device_fsnode_t *fs_node = (device_fsnode_t *) vfs_list_node->value;

	direntry_t *entry = (direntry_t *) malloc(sizeof(direntry_t));
	strcpy(entry->name, fs_node->name);
	entry->ino = index;

	return entry;
}

fs_node_t *devfs_finddir(fs_node_t *node, char *name) {
	foreach_in_list(vfs_list_node, devices_list) {
		device_fsnode_t *fs_node = (device_fsnode_t *) vfs_list_node->value;

		if(fs_node && strcmp(name, fs_node->name) == 0) {
			fs_node_t *node = fs_node->root;
			node->references++;
			return node;
		}
	}

	return NULL;
}

fs_node_t *devfs_init() {
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = &devfs_readdir;
	node->finddir = &devfs_finddir;
	node->read = 0;
	node->write = 0;
	node->inode = 0;
	node->open = 0;
	node->close = 0;
	node->flags = FS_DIRECTORY;
	node->references = 1;

	devices_list = list_create();
	return node;
}

void devfs_add(char *name, fs_node_t *root) {
	device_fsnode_t *mount = (device_fsnode_t *) malloc(sizeof(device_fsnode_t));
	node_t *fs_node = (node_t *) malloc(sizeof(node_t));

	root->references++;
	mount->name = name;
	mount->root = root;
	fs_node->value = mount;

	list_add_item(devices_list, fs_node);
}