#include <system.h>

extern fs_node_t *fs_root;

char *get_path_absolute(char *basedir, char *filename) {
	uint32_t dirlen = strlen(basedir);
	uint32_t filenamelen = strlen(filename);

	char *filepath;
	if(filename[0] == '/') {
		filepath = (char *) malloc(filenamelen + 1);
		strcpy(filepath, filename);
	} else {
		filepath = (char *) malloc(dirlen + filenamelen + 1 + 1);
		strcpy(filepath, basedir);
		*(filepath + dirlen) = '/';
		strcpy(filepath + dirlen + 1, filename);
	}

	return filepath;
}

char *get_path(char *filename) {
	return get_path_absolute(get_current_dir_name(), filename);
}

fs_node_t *fs_node_open(const char *path) {
	fs_node_t *node = fs_root;

	// Points to root?
	if(strcmp(path, "/") == 0) {
		node->references++;
		return node;
	}

	char *filen = strdup(path);
	char *filebegin = filen;
	if(filen[0] == '/')
		filen++;
	
	uint32_t parts = str_count(filen, '/') + 1;
	uint32_t i = 0;
	
	while(i < parts) {
		if(parts > 1)
			filen = strtok(filen, '/');

		node = fs_finddir(node, filen);
		if(node == NULL) {
			free(filebegin);
			return NULL;
		}
	
		i++;
	}

	free(filebegin);

	node->references++;
	return node;
}

FILE *fopen(const char *filename) {
	fs_node_t *node = fs_node_open(filename);
	if(node == NULL)
		return NULL;

	if((node->flags & FS_FILE) != FS_FILE)
		return NULL;
	
	FILE *file = (FILE *) malloc(sizeof(FILE));
	file->offset = 0;	// Open at the beginning of the file
	file->node = node;  // Store the node for reading
	
	return file;
}

int fseek(FILE *stream, int offset, int origin) {
	if(origin == SEEK_SET) {
		stream->offset = offset;
	} else if(origin == SEEK_CUR) {
		stream->offset += offset;
	} else if(origin == SEEK_END) {
		stream->offset = stream->node->length - offset;
	}
	
	return 0;
}

int ftell(FILE *stream) {
	return stream->offset;
}

int fread(void *buffer, int size, FILE *stream) {
	uint32_t sz = fs_read(stream->node, stream->offset, size, buffer);
	stream->offset += sz;
	
	return sz;
}

int fwrite(void *buffer, int size, FILE *stream) {
	uint32_t sz = fs_write(stream->node, stream->offset, size, buffer);
	stream->offset += sz;
	
	return sz;
}

void fclose(FILE *file) {
	fs_close(file->node);
	free(file);
}