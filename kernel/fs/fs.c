#include <system.h>

fs_node_t *fs_root = NULL;

// STDIO
fs_node_t *stdout = NULL;
fs_node_t *stdin = NULL;
fs_node_t *stderr = NULL;

uint32_t fs_read(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	if(!node->read)
		return 0;

	return node->read(node, offset, size, buffer);
}

uint32_t fs_write(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	if(!node->write)
		return 0;

	return node->write(node, offset, size, buffer);
}

void fs_open(fs_node_t *node, uint8_t read, uint8_t write) {
	// TODO: Look at the read and write flags instead of simply ignoring them
	if(node->open != NULL) node->open(node);
}

void fs_close(fs_node_t *node) {
	node->references--;
	if(node->references > 0)
		return;

	if(node->close != NULL)
		node->close(node);

	free(node);
}

direntry_t *fs_readdir(fs_node_t *node, uint32_t index) {
	if(!node->readdir)
		return NULL;

	return node->readdir(node, index);
}

fs_node_t *fs_finddir(fs_node_t *node, char *name) {
	if(!node->finddir)
		return NULL;
	
	return node->finddir(node, name);
}