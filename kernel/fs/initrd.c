#include <system.h>

initrd_header_t *initrd_header = NULL;
initrd_dir_entry_t *initrd_root = NULL;
initrd_dir_index_t *initrd_index = NULL;
uint32_t *initrd_fileptr;

uint32_t initrd_indexfind(uint32_t id) {
	uint32_t i;
	for(i = 0; i < initrd_header->num_index_entries; i++) 
		if(initrd_index[i].id == id)
			return initrd_index[i].offset;

	return 0xFFFFFFFF;
}

direntry_t *initrd_readdir(fs_node_t *node, uint32_t index) {
	uint32_t rindex = 0;
	if(node->inode != 0) {
		rindex = initrd_indexfind(node->inode);
		if(rindex == 0xFFFFFFFF)
			return NULL;
	}
	
	rindex += index;
	
	if(rindex >= initrd_header->num_tables) // just to be safe
		return NULL;

	if(node->inode == 0) {
		if(initrd_root[rindex].parentId != node->inode) 
			return NULL;
	} else {
		if(initrd_root[rindex].parentId != node->inode + 1) 
			return NULL;
	}
	
	direntry_t *entry = (direntry_t *) malloc(sizeof(direntry_t));
	strcpy(entry->name, initrd_root[rindex].name);
	entry->ino = rindex;
	
	return entry;
}

uint32_t initrd_read(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	initrd_dir_entry_t *dirent = &initrd_root[node->inode];
	
	if(size + offset > dirent->size) size = dirent->size - offset;
	memcpy(buffer, (void *) ((uint32_t) initrd_fileptr + dirent->offset + offset), size);
	
	return size;
}

fs_node_t *initrd_finddir(fs_node_t *node, char *name) {
	uint32_t i = node->inode;
	while(i < initrd_header->num_tables) {
		uint32_t par = initrd_root[i].parentId;
		
		if(node->inode != 0)
			par--;
		
		if(strcmp(name, initrd_root[i].name) == 0 && par == node->inode) {
			fs_node_t *node_ptr = (fs_node_t *) malloc(sizeof(fs_node_t));
			
			node_ptr->mask = node_ptr->uid = node_ptr->gid = 0;
			node_ptr->flags = initrd_root[i].flags;
			node_ptr->inode = i;
			node_ptr->read = 0;
			node_ptr->write = 0;
			node_ptr->open = 0;
			node_ptr->length = initrd_root[i].size;
			node_ptr->close = 0;
			node_ptr->impl = 0;
			node_ptr->references = 1;

			if((node_ptr->flags & FS_DIRECTORY) == FS_DIRECTORY) {
				node_ptr->readdir = &initrd_readdir;
				node_ptr->finddir = &initrd_finddir;
				node_ptr->read = 0;
			} else {
				node_ptr->read = &initrd_read;
				node_ptr->readdir = 0;
				node_ptr->finddir = 0;
			}

			return node_ptr;
		}
		
		i++;
	}
	
	return NULL;
}

fs_node_t *initrd_init(uint32_t location) {
	initrd_header = (initrd_header_t *) location;
	initrd_index = (initrd_dir_index_t *) (location + sizeof(initrd_header_t));
	initrd_root = (initrd_dir_entry_t *) ((uint32_t) initrd_index + (initrd_header->num_index_entries * sizeof(initrd_dir_index_t)));
		
	initrd_indexfind(0);

	// Mounting
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = &initrd_readdir;
	node->finddir = &initrd_finddir;
	node->read = 0;
	node->write = 0;
	node->inode = 0;
	node->open = 0;
	node->close = 0;
	node->flags = FS_DIRECTORY;
	node->references = 1;
	
	initrd_fileptr = (uint32_t *) ((uint32_t) initrd_root + (sizeof(initrd_dir_entry_t) * initrd_header->num_tables));

	return node;
}

void initrd_info() {
	kprintf("Initrd info\n\nMagic:\t\t\t%x\nNumber of headers:\t%d\n", initrd_header->magic, initrd_header->num_tables);
}