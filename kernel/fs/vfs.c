#include <system.h>

extern fs_node_t *fs_root;
list_t *vfs_list;

direntry_t *vfs_readdir(fs_node_t *node, uint32_t index) {
	if(index >= vfs_list->length)
		return NULL;

	node_t *vfs_list_node = list_get(vfs_list, index);
	if(!vfs_list_node)
		return NULL;

	mount_t *mount_node = (mount_t *) vfs_list_node->value;

	direntry_t *entry = (direntry_t *) malloc(sizeof(direntry_t));
	strcpy(entry->name, mount_node->name);
	entry->ino = index;

	return entry;
}

fs_node_t *vfs_finddir(fs_node_t *node, char *name) {
	foreach_in_list(vfs_list_node, vfs_list) {
		mount_t *mount_node = (mount_t *) vfs_list_node->value;

		if(mount_node && strcmp(name, mount_node->name) == 0) {
			fs_node_t *node = mount_node->root;
			node->references++;
			return node;
		}
	}

	return NULL;
}

void vfs_init() {
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = &vfs_readdir;
	node->finddir = &vfs_finddir;
	node->read = 0;
	node->write = 0;
	node->inode = 0;
	node->open = 0;
	node->close = 0;
	node->flags = FS_DIRECTORY;
	node->references = 1;

	vfs_list = list_create();
	fs_root = node;
}

void vfs_mount(char *path, fs_node_t *root) {
	mount_t *mount = (mount_t *) malloc(sizeof(mount_t));
	node_t *mount_node = (node_t *) malloc(sizeof(node_t));

	root->references++;
	root->flags |= FS_MOUNTPOINT;
	mount->name = path;
	mount->root = root;
	mount_node->value = mount;

	list_add_item(vfs_list, mount_node);
}

void vfs_unmount(char *path) {
	node_t *node = NULL;
	mount_t *mount_node = NULL;

	foreach_in_list(vfs_list_node, vfs_list) {
		mount_node = (mount_t *) vfs_list_node->value;
		if(strcmp(mount_node->name, path) == 0) {
			node = vfs_list_node;
			break;
		}
	}

	if(!node)
		return;

	fs_close(mount_node->root);
	free(mount_node);
	list_delete_node(vfs_list, node);
}