#include <system.h>

uint32_t nullfs_read(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	if(size == 0)
		return 0;

	*buffer = 0;
	return 1;
}

uint32_t nullfs_write(fs_node_t *node, uint32_t offset, uint32_t size, uint8_t *buffer) {
	return size;
}

fs_node_t *nullfs_init() {
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = 0;
	node->finddir = 0;
	node->read = &nullfs_read;
	node->write = &nullfs_write;
	node->inode = 0;
	node->open = 0;
	node->close = 0;
	node->flags = FS_FILE;
	node->references = 1;

	return node;
}