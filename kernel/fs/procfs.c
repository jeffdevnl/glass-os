#include <system.h>

char *entries[] = {
	"cwd"
};
uint32_t indices = sizeof(entries) / sizeof(char *);

direntry_t *procfs_proc_readdir(fs_node_t *node, uint32_t index) {
	if(index >= indices)
		return NULL;

	direntry_t *entry = (direntry_t *) malloc(sizeof(direntry_t));
	strcpy(entry->name, entries[index]);
	entry->ino = index;

	return entry;
}

fs_node_t *procfs_proc_finddir(fs_node_t *node, char *name) {
	if(strcmp(name, "cwd") == 0) {
		fs_node_t *wd_node = get_task_by_pid(node->inode)->wd_node;
		wd_node->references++;

		return wd_node;
	}

	return NULL;
}

direntry_t *procfs_readdir(fs_node_t *node, uint32_t index) {
	task_t *task = get_task_by_pid(index);
	if(task == NULL)
		return NULL;

	direntry_t *entry = (direntry_t *) malloc(sizeof(direntry_t));
	uint32_t len = sprintf(entry->name, "%d", index);
	entry->name[len] = '\0';
	entry->ino = index;

	return entry;
}

fs_node_t *procfs_finddir(fs_node_t *node, char *name) {
	if(!isi(name))
		return NULL;

	uint32_t pid = atoi(name);
	task_t *task = get_task_by_pid(pid);
	if(task == NULL)
		return NULL;

	fs_node_t *fs_node = (fs_node_t *) malloc(sizeof(fs_node_t));
	fs_node->gid = 0;
	fs_node->length = 0;
	fs_node->impl = 0;
	fs_node->readdir = &procfs_proc_readdir;
	fs_node->finddir = &procfs_proc_finddir;
	fs_node->read = 0;
	fs_node->write = 0;
	fs_node->inode = 0;
	fs_node->open = 0;
	fs_node->close = 0;
	fs_node->flags = FS_DIRECTORY;
	fs_node->references = 1;

	return fs_node;
}

fs_node_t *procfs_init() {
	fs_node_t *node = (fs_node_t *) malloc(sizeof(fs_node_t));
	node->gid = 0;
	node->length = 0;
	node->impl = 0;
	node->readdir = &procfs_readdir;
	node->finddir = &procfs_finddir;
	node->read = 0;
	node->write = 0;
	node->inode = 0;
	node->open = 0;
	node->close = 0;
	node->flags = FS_DIRECTORY;
	node->references = 1;

	return node;
}