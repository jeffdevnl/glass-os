#include <system.h>

tree_t *tree_create() {
	tree_t *tree = (tree_t *) malloc( sizeof(tree_t) );
	
	return tree;
}

void tree_set_root(tree_t *tree, void *value) {
	tree_node_t *root = (tree_node_t *) malloc( sizeof(tree_node_t) );
	root->children = list_create();
	root->value = value;
	tree->root = root;
	tree->size = 1;
}

void tree_add_child(tree_t *tree, tree_node_t *node, void *value) {
	tree_node_t *child_node = (tree_node_t *) malloc( sizeof(tree_node_t) );
	child_node->children = list_create();
	
	
	node_t *child_nodelist = (node_t *) malloc( sizeof(node_t) );
	child_nodelist->value = child_node;
	
	// Fix for something WTF
	list_t *backup = node->children;
	
	list_add_item(node->children, child_nodelist);
	
	node->children = backup;
	tree->size++;
}

void tree_node_destroy(tree_node_t *node) {
	
	/*foreach_in_list (child, node->children) {
		tree_node_destroy((tree_node_t *) (child->value));
	}*/
	
	free(node->value);
	list_destroy(node->children);
	free(node);
}

void tree_destroy(tree_t *tree) {
	tree_node_destroy(tree->root);
	free(tree);
}