#include <system.h>

char toupper(char c) {
	if(c >= 'a' && c <= 'z')
		return c + ('A' - 'a');

	return c;
}

char tolower(char c) {
	if(c >= 'A' && c <= 'Z')
		return c + ('a' - 'A');

	return c;
}

uint32_t strlen(const char *text) {
	uint32_t i = 0;
	
	while(*(text++) != '\0')
		i++;
	
	return i;
}

char *sprintf_write_dec(char *str, uint32_t val) {
	if(val == 0) {
		*str++ = '0';
		return str;
	}

	uint32_t v = val / 10;
	uint32_t n = val % 10;
	
	if(n < 0) {
		n--;
		v += 10;
	}

	if(val >= 10)
		sprintf_write_dec(str, v);
	
	*str++ = (char)(n + '0');
	return str;
}

char *sprintf_write_hex(char *str, uint32_t hex) {
	if(hex == 0) {
		*str++ = '0';
		return str;
	}

	uint16_t tmp;
	char noZeroes = 1;

	int32_t j = 28;
	for(; j >= 0; j -= 4) {
		tmp = (hex >> j) & 0x0F;
		if(tmp == 0 && noZeroes != 0)
			continue;
		
		noZeroes = 0;
		if(tmp >= 0x0A)
			*str++ = tmp - 0x0A + 'A';
		else
			*str++ = tmp + '0';
	}

	return str;
}

char *sprintf_write(char *str, char *s) {
	uint32_t length = strlen(s);
	uint32_t j = 0;
	for(; j < length; j++)
		*str++ = s[j];

	return str;
}

uint32_t vssprintf(char *str, const char *format, va_list args) {
	uint32_t i = 0;
	uint32_t start = (uint32_t)str;
	
	
	for(; format[i] != '\0'; i++) {
		if(format[i] != '%') {
			*str++ = format[i];
		} else {
			i++;
			switch(format[i]) {
				case 's':
					str = sprintf_write(str, va_arg(args, char *));
					break;

				case 'x':
					str = sprintf_write_hex(str, (uint64_t)va_arg(args, uint64_t));
					break;

				case 'd':
					str = sprintf_write_dec(str, (uint64_t)va_arg(args, uint64_t));
					break;

				case 'c':
					*str++ = (char)va_arg(args, int);
					break;

				case '%':
					*str++ = '%';
					break;

				default:
					*str++ = format[i];
					break;
			}
		}
	}
	

	uint32_t end = (uint32_t)str;
	return end - start;
}

uint32_t sprintf(char *str, const char *format, ...) {
	va_list args;
	va_start(args, format);
	uint32_t ret = vssprintf(str, format, args);
	va_end(args);
	
	return ret;
}

char *strcpy(char *s1, const char *s2) {
	char *tmp = s1;
	while((*s1++ = *s2++));
	return tmp;
}

char *strncpy(char *s1, const char *s2, uint32_t length) {
	uint32_t i = 0;
	for(; i < length && s2[i] != '\0'; i++)
		s1[i] = s2[i];

	for(; i < length; i++)
		s1[i] = '\0';

	return s1;
}

uint32_t str_count(char *in, char cmp) {
	uint32_t count = 0;
	for(; *in != '\0'; count += (*in++ == cmp));
	return count;
}

char *strtok(char *str, const char delim) {
	if(str == NULL)
		return NULL;

	uint32_t i = 0;
	uint8_t first = 0;
	uint32_t length = strlen(str);
	while(i < length) {
		if(str[i++] == delim) {
			str[i - 1] = '\0';
			first = 1;
		}
	}

	if(first)
		return str;

	i = 0;
	while(1) {
		char ch = str[i++];
		if(ch == '\0')
			return &str[i];
	}

	return str;
}

int8_t strcmp(const char *a, const char *b) {
	uint32_t index = 0;
	while(1) {
		if(a[index] > b[index])
			return -1;
		else if(b[index] > a[index])
			return 1;
		else if(a[index] == '\0')
			return 0;

		index++;
	}

	return 0;
}

int8_t strncmp(const char *a, const char *b, uint32_t length) {
	uint32_t index = 0;
	while(index < length) {
		if(a[index] > b[index])
			return -1;
		else if(b[index] > a[index])
			return 1;
		else if(a[index] == '\0')
			return 0;

		index++;
	}

	return 0;
}

uint8_t strstr(char *in, char *cmp) {
	if(strncmp(in, cmp, strlen(cmp)) == 0)
		return 1;

	return 0;
}

char *strdup(const char *s) {
	uint32_t len = strlen(s);
	char *d = (char *) malloc(len + 1);
	if(d == NULL)
		return NULL;

	strcpy(d, s);

	return d;
}