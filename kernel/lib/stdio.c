#include <stdio.h>

int atoi(char *str)
{
    int res = 0;
    int sign = 1;
     
    if(str[0] == '-') {
        sign = -1;
		str++;
	}
	
	char c;
	while((c = *str++) != '\0') {
		if(c >= '0' && c <= '9')
			res = res * 10 + c - '0';
		else 
			return 0;
	}
   
    return sign * res;
}

int isi(char *str)
{
    if(str[0] == '-')
        str++;
	
	char c;
	while((c = *str++) != '\0') {
		if(c < '0' || c > '9')
			return 0;
			
	}
   
    return 1;
}