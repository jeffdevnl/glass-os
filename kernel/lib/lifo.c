#include <system.h>

lifo_t *lifo_create() {
	lifo_t *lifo = (lifo_t *) malloc ( sizeof(lifo) );

	return lifo;
}

void *lifo_pull(lifo_t *lifo) {
	// Check if lifo is empty D=
	if(!lifo->next) return NULL;

	lifo_item_t *item = lifo->next;
	lifo->next = item->next;
	void *ret = item->value;
	free(item); // bye bye

	return ret; 	
}

void lifo_push(void *value, lifo_t *lifo) {
	lifo_item_t *item = (lifo_item_t *) malloc ( sizeof(lifo_item_t) );
	item->value = value;
	item->next = lifo->last;
	lifo->last = item;
}

