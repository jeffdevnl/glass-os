#include <system.h>

// Current seed
uint32_t srseed_cur = 304343232;

/**
 *
 * Seeds the RNG
 * @param seed the new seed
 *
**/
void srand(uint32_t seed) {
    srseed_cur = seed;
}

/**
 *
 * Generates a random number
 * @return random number
 *
**/
uint32_t rand() {
    srseed_cur = srseed_cur * 1103515245 + 12345;
    return (uint32_t) (srseed_cur / 65536) % 32768;
}