#include <system.h>
#include <serialshell.h>

FILE *serial_file;


void serialshell_run() {
	serial_file = fopen("/serial/COM2");
	
	if(!serial_file) {
		kprintf("Serialshell could not be started: serialport not found\n");
		for(;;);
	}
	
	while(1)
		serialshell_readinput();
}

void serialshell_print(char *message, ...) {
	char *buff = (char *) malloc( 500 );
	
	// Format input message in buff
	va_list args;
	va_start(args, message);
	vssprintf(buff, message, args);
	va_end(args);
	
	serial_writestring(buff, 0x2F8);
	
	free(buff);
}

typedef struct {
	char *command;
	void *pointer;
	char *help;
} serial_command_t;

void serialcommand_time(int argc, char **argv) {
	serialshell_print("%d:%d:%d\n", cmos_get_real(CMOS_HOURS), cmos_get_real(CMOS_MINUTES), cmos_get_real(CMOS_SECONDS));
}

uint32_t serial_commands = 1;
serial_command_t serial_command[] = {
	{ "time",			&serialcommand_time,		"Changes current working directory" }
};

void serialshell_process(char *buffer_pointer) {
	char *command;
	int argc = str_count(buffer_pointer, ' ') + 1;
	char **argv = (char **)malloc(sizeof(char *) * argc);
	
	if(argc == 1) {
		command = buffer_pointer;
	} else {
		command = strtok(buffer_pointer, ' ');
	}
	
	argv[0] = command;

	uint32_t i;
	for(i = 1; i < argc; i++) {
		argv[i] = strtok(argv[i - 1], ' ');
	}
	
	int found = 0;

	i = 0;
	
	while(i < serial_commands) {
		if(strcmp(command, serial_command[i].command) == 0) {
			void *func = serial_command[i].pointer;
			typedef void (*shell_cmd) (int argc, char **argv);
			((shell_cmd) func)(argc, argv);
			
			found = 1;
			break;
		}

		i++;
	}
	
	if(!found) {
		serialshell_print("Command not found");
	}
}

void serialshell_readinput() {
	int buf_size = 1024;
	char *buf = (char *)malloc(buf_size + 1);
	memset(buf, 0, buf_size + 1);

	// Read
	fread((void *)buf, buf_size, serial_file);

	if(*buf != '\0' || *buf != 0) {
		serialshell_process(buf);
	}

	// Free stuff
	free(buf);
}