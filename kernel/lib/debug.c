#include <system.h>

int debug_mode = DEBUG_NO; // 0 = no logging, 1 = Serial logging, 2 = screen logging
int debug_serial = 0; // Serial port when serial debugging

char *debug_levels[] = {
	"[INFO]",
	"[NOTICE]",
	"[WARNING]",
	"[ERROR]"
};

void debug_set_mode(int mode) {
	debug_mode = mode;
}

void debug_set_serial_port(int port) {
	debug_serial = port;
}

void debug_printstring(char *string) {
	if(debug_mode == DEBUG_NO)
		return;
		
	if(debug_mode == DEBUG_SERIAL) {
		serial_writestring(string, debug_serial);
	} else {
		kprintf(string);
	}
}

void debug_print(int level, char *message, ...) {
	if(debug_mode == DEBUG_NO)
		return;

	// Please do not interrupt us
	__asm__ __volatile__("cli");
		
	char *buff = (char *) malloc(1024);
	
	// Format input message in buff
	va_list args;
	va_start(args, message);
	vssprintf(buff, message, args);
	va_end(args);

	int lvl_size = strlen(debug_levels[level]);
	int msg_size = strlen(buff);
	
	// Create output string
	char *msg = (char *) malloc(lvl_size + msg_size + 3);
	strcpy(msg, debug_levels[level]);
	strcpy(msg + lvl_size + 1, buff);
	msg[lvl_size] = ' ';
	msg[lvl_size + 1 + msg_size] = '\n';
	
	debug_printstring(msg);
	
	free(msg);
	free(buff);

	// We may be interrupted
	__asm__ __volatile__("sti");
}