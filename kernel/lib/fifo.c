#include <system.h>

fifo_t *fifo_create(uint32_t size, uint8_t wait) {
	fifo_t *fifo = (fifo_t *) malloc(sizeof(fifo_t));
	fifo->buffer = (char *) malloc(size);
	fifo->size = size;
	fifo->head = 0;
	fifo->tail = 0;
	fifo->wait = wait;
	
	return fifo;
}

uint32_t fifo_read(fifo_t *fifo, void *buffer, uint32_t size){
	uint32_t i;
	char *current;
	current = buffer;

	// Wait for data?
	if(fifo->wait) {
		while(fifo->tail == fifo->head) {
			// Wait for next tick...
			__asm__ __volatile__("sti");
			__asm__ __volatile__("hlt");
		}
	}

	for(i = 0; i < size; i++) {
		// Is there data?
		if(fifo->tail != fifo->head) {
			*current++ = fifo->buffer[fifo->tail];
			fifo->tail++;

			// Time to flip the fifo tail?
			if(fifo->tail >= fifo->size)
				fifo->tail = 0;
		} else {
			// We may return if we shouldn't wait
			return i;
		}
	}

	return size;
}

uint32_t fifo_write(fifo_t *f, const void *buffer, uint32_t size) {
	uint32_t i;
	const char *current;
	current = buffer;
	for(i = 0; i < size; i++) {
		if(fifo_write_byte(f, *current++) == 0) 
			return i;
	}
	
	return size;
}

uint32_t fifo_write_byte(fifo_t *fifo, char byte) {
	// Is there any room?
	if((fifo->head + 1 == fifo->tail) || ((fifo->head + 1 == fifo->size) && (fifo->tail == 0))) {
		return 0;
	} else {
		fifo->buffer[fifo->head] = byte;
		fifo->head++;
		
		if(fifo->head >= fifo->size)
			fifo->head = 0;
	}
	
	return 1;
}

void fifo_destroy(fifo_t *fifo) {
	free(fifo);
}