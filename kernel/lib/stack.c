#include <system.h>

stack_t *stack_create() {
	stack_t *stack = (stack_t *) malloc(sizeof(stack_t));
	stack->length = 0;
	stack->top = NULL;
	
	return stack;
}

void stack_push(stack_t *stack, void *value) {
	stack_node_t *node = (stack_node_t *) malloc(sizeof(stack_node_t));
	node->next = stack->top;
	node->value = value;
	
	stack->top = node;
	stack->length++;
}

uint32_t stack_size(stack_t *stack) {
	return stack->length;
}

int stack_is_empty(stack_t *stack) {
	return (stack->length == 0);
}

void *stack_pop(stack_t *stack) {
	stack_node_t *node = stack->top;
	
	stack->top = node->next;
	stack->length--;
	
	void *value = node->value;
	free(node);
	
	return value;
}

void stack_swap(stack_t *x, stack_t *y) {
	stack_node_t *top = y->top;
	y->top = x->top;
	x->top = top;
}

void stack_destroy(stack_t *stack) {
	stack_node_t *node;
	while((node = stack->top) != NULL) {
		stack->top = node->next;
		free(node->value);
		free(node);
	}
	
	free(stack);
}