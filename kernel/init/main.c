#include <system.h>
#include <elf_loader.h>

multiboot_t *multiboot_header;

extern void switch_to_user_mode();

/**
 *
 * Initializes the FPU (Floating Point Unit)
 *
**/
void fpu_init() {
	uint32_t cr4;
	__asm__ __volatile__("mov %%cr4, %0" : "=r" (cr4));
	cr4 |= 0x200;
	__asm__ __volatile__("mov %0, %%cr4" :: "r" (cr4));

	__asm__ __volatile__("fninit");
}

/**
 *
 * Kernel main entrypoint
 * @param stack the initial stack
 * @param multiboot the multiboot header
 * @param magic the multiboot magic
 *
**/
void kmain(uint32_t stack, multiboot_t *multiboot, uint32_t magic) {
	// Store multiboot header
	multiboot_header = multiboot;

	// Info
	screen_clear();
	kprintf("Build date: %s %s\n", __DATE__, __TIME__);
	kprintf("Starting Glass OS...\n\n");

	// Please let these be right
	ASSERT(magic == 0x2BADB002, "Magic provided by multiboot bootloader is incorrect");
	ASSERT(multiboot->mods_count > 0, "No GRUB modules loaded.");

	// Find initrd
	uint32_t initrd_location = *((uint32_t *) multiboot->mods_address);
	uint32_t initrd_end = *(uint32_t *) (multiboot->mods_address + 4);
	
	// Stuff
	gdt_install();
	idt_install();
	isr_install();
	gpf_install_handler();
	irq_install();
	__asm__ __volatile__ ("sti");
	install_keyboard();
	mouse_install();
	acpi_init();
	serial_init();
	fpu_init();

	// Heap
	heap_install(initrd_end);
for(;;);
	// VESA
	vesa_init();

	// Paging
	paging_init((multiboot_header->lower_memory + multiboot_header->upper_memory) * 1024);
	acpi_enable_paging();

	// VFS
	vfs_init();
	vfs_mount("initrd", initrd_init(initrd_location));
	vfs_mount("proc", procfs_init());
	vfs_mount("dev", devfs_init());

	// Devices with DevFS
	stdin = keyboard_pipe_init();
	stdout = stdout_fs_init();

	devfs_add("kbd", stdin);
	devfs_add("mouse", mouse_pipe_init());
	devfs_add("null", nullfs_init());

	devfs_add("ttyS0", serial_port_node(0));
	devfs_add("ttyS1", serial_port_node(1));
	devfs_add("ttyS2", serial_port_node(2));
	devfs_add("ttyS3", serial_port_node(3));

	devfs_add("stdout", stdout);
	devfs_add("stdin", stdin);

	// Initialize tasking
	timer_install();
	tasking_init();

	// Syscalls and loaders
	syscalls_setup();
	gsyscalls_setup();
	loader_init();
	elfloader_init();

	// Debugger
	debug_set_mode(DEBUG_NO);
	debug_set_serial_port(0x2F8);
	debug_print(DEBUG_INFO, "Debugger enabled");

	// First task!
	tasking_fork("System");
	
	// Start the shell
	tasking_start("Shell", start_shell, 0);
	
	//extern void serialshell_run();
	//tasking_start("SerialShell", serialshell_run, 0);

	// Stack size is 8192
	set_kernel_stack(stack + 8192);

	// Please change kernel stuff to kernelmode back later in paging
	switch_to_user_mode();
	
	// No HLT in usermode
	for(;;);
}