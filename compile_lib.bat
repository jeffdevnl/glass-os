@echo off

echo Building syscall system...
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/syscall.o user/lib/syscall.c

echo Compiling...
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/stdlib.o user/lib/stdlib.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/unistd.o user/lib/unistd.c
"tools/gcc/bin/i586-elf-gcc.exe" -std=gnu99 -m32 -Wall -O -fstrength-reduce -fomit-frame-pointer -finline-functions -fno-builtin -c -o user/lib/string.o user/lib/string.c

echo Linking...
"tools/gcc/bin/i586-elf-ld.exe" -m elf_i386 -T user/lib/link.ld -o user/lib/lib.o user/lib/stdlib.o user/lib/unistd.o user/lib/string.o

echo Cleanup
del user\lib\stdlib.o
del user\lib\syscall.o
del user\lib\unistd.o
del user\lib\string.o