#ifndef __SERIALPORT_H
#define __SERIALPORT_H

#define SERIAL_FIFO_SIZE 1024

void serial_init();
void serial_send(char d, int port);
char serial_read(int port);
void serial_writestring(char *string, int port);
fs_node_t *serial_port_node(int port);

#endif