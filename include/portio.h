#ifndef __PORTIO_H
#define __PORTIO_H

uint8_t inportb(uint16_t _port);
void outportb(uint16_t _port, uint8_t _data);
uint16_t inportw(uint16_t _port);
void outportw(uint16_t _port, uint16_t _data);

#endif