#ifndef __ISR_H
#define __ISR_H

void isr_install();
void isr_fault_handler(registers_t *r);

#endif