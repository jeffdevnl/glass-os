#ifndef __DIRENT_H
#define __DIRENT_H

#define DT_UNKNOWN	0
#define DT_FIFO		1
#define DT_CHR		2
#define DT_DIR		4
#define DT_BLK		6
#define DT_REG		8
#define DT_LINK		10
#define DT_SOCK		12
#define DT_WHT		14

#define DIR_MAGIC	0xE270C6FA

typedef struct direntry {
    uint32_t ino;
    int32_t off;
    uint16_t reclen;
    uint8_t type;

    char name[MAX_FILE_LENGTH + 1];
} direntry_t;

typedef struct {
	uint32_t magic;
	uint64_t entries_count;
	int64_t offset;

	fs_node_t *node;
	direntry_t **entries;
} __dirstream_t;

// Opaque data structure for users
typedef struct __dirstream DIR;

DIR *opendir(const char *path);
int closedir(DIR *dp);
direntry_t *readdir(DIR *dp);
void rewinddir(DIR *dp);
void seekdir(DIR *dp, int64_t offset);
int64_t telldir(DIR *dp);

#endif