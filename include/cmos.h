#ifndef __CMOS_H
#define __CMOS_H

#define CMOS_SECONDS	0x00
#define CMOS_MINUTES	0x02
#define CMOS_HOURS		0x04
#define CMOS_WEEKDAY	0x06
#define CMOS_DOM		0x07
#define CMOS_MONTH		0x08
#define CMOS_YEAR		0x09
#define CMOS_CENTURY	0x32
#define CMOS_SRA		0x0A
#define CMOS_SRB		0x0B

enum {
	cmos_address = 0x70,
	cmos_data = 0x71
};

uint8_t cmos_get(uint8_t reg);
uint8_t cmos_get_real(uint8_t reg); 

#endif