#ifndef __VESA_H
#define __VESA_H

typedef struct {
   char signature[4];                   // "VESA" / "VBE2" / ...
   uint16_t version;                    // Version of VBE extension
   uint32_t oemname;                    // Pointer to OEM name
   uint32_t capabilities;               // Capabilities flags
   uint32_t supported_modes;            // Pointer to list of supported VESA and OEM modes
   uint16_t total_memory;               // Total memory in 64KB blocks
   // VBE 2.0
   uint16_t oem_software_version;       // OEM software version (BCD, hi = major, lo = minor)
   uint32_t vendor_name;                // Pointer to vendor name
   uint32_t product_name;               // Pointer to product name
   uint32_t product_revision;           // Pointer to product revision string
   uint16_t vbe_af_version;             // If capabilities bit 3 set: VBE/AF version (BCD)
   uint32_t accelerated_modes;          // Pointer to list of accelerated video modes
   char reserved[216 + 256];
} __attribute__((packed)) vbe_info_block_t;

typedef struct {
    uint16_t attributes;                // Mode attributes
    uint8_t window_a_attribs;           // Window attributes of A
    uint8_t window_b_attribs;           // Window attributes of B
    uint16_t window_gran;               // Window granularity in KB
    uint16_t window_size;               // Window size in KB
    uint16_t window_a_segstart;         // Start segment of window A
    uint16_t window_b_segstart;         // Start segment of window B
    uint32_t window_far_pos;            // Far window positioning function
    uint16_t bytes_per_scanline;        // Bytes per scanline
    uint16_t width;                     // Width
    uint16_t height;                    // Height
    uint8_t charcell_width;             // Width of character cell in pixels
    uint8_t charcell_height;            // Height of character cell in pixels
    uint8_t memory_planes;              // Number of memory planes
    uint8_t bits_per_pixel;             // Number of bits per pixel
    uint8_t banks;                      // Number of banks
    uint8_t mem_model_type;             // Memory model type
    uint8_t bank_size;                  // Size of a bank in KB
    uint8_t image_pages;                // Number of image pages that fits in video memory
    uint8_t reserved;
    // VBE 1.2+
    uint8_t red_mask_size;              // Red mask size
    uint8_t red_field_pos;              // Red field pos
    uint8_t green_mask_size;            // Green mask size
    uint8_t green_field_pos;            // Green field pos
    uint8_t blue_mask_size;             // Blue mask size
    uint8_t blue_field_pos;             // Blue field pos
    uint8_t reserved_mask_size;
    uint8_t reserved_mask_pos;
    uint8_t direct_color_mode_info;     // Direct color mode info
    // VBE 2.0+
    uint32_t lfb_address;               // Physical address of linear video buffer
    uint32_t offscreen_mem;             // Pointer to start of offscreen memory
    uint16_t offscreen_mem_size;        // Size of offscreen memory in KB
    // VBE 3.0+
    uint16_t bytes_per_scanline_linear; // Bytes per scanline in linear mode
    uint8_t images_bank;                // Number of images for banked video mode
    uint8_t images_linear;              // Number of images for linear video modes
    uint32_t max_pixel_clock;           // Maximum pixel clock in HZ

    uint8_t reserved2[190];
} __attribute__((packed)) vbe_mode_info_t;

void vesa_init();
vbe_mode_info_t *vesa_get_mode(uint16_t mode, uint8_t lfb);
uint16_t vesa_find_mode(uint16_t min_width, uint16_t min_height, uint16_t max_width, uint16_t max_height, uint8_t min_bbp);
vbe_mode_info_t *vesa_set_mode(uint16_t mode, uint8_t lfb, uint8_t page);

#endif