#ifndef __ELF_H
#define __ELF_H

typedef uint16_t Elf32_Half;
typedef uint16_t Elf64_Half;
typedef uint32_t Elf32_Word;
typedef	int32_t  Elf32_Sword;
typedef uint32_t Elf64_Word;
typedef	int32_t  Elf64_Sword;
typedef uint64_t Elf32_Xword;
typedef	int64_t  Elf32_Sxword;
typedef uint64_t Elf64_Xword;
typedef	int64_t  Elf64_Sxword;
typedef uint32_t Elf32_Addr;
typedef uint64_t Elf64_Addr;
typedef uint32_t Elf32_Off;
typedef uint64_t Elf64_Off;
typedef uint16_t Elf32_Section;
typedef uint16_t Elf64_Section;
typedef uint32_t Elf32_Symndx;
typedef uint64_t Elf64_Symndx;

enum {
	EI_MAG0,
	EI_MAG1,
	EI_MAG2,
	EI_MAG3,
	EI_CLASS,
	EI_DATA,
	EI_VERSION,
	EI_OSABI,
	EI_ABIVERSION,
	EI_PAD
};

#define ELF_RELOC		1
#define ELF_EXEC		2
#define ELF_SHARED		3
#define ELF_CORE		4

#define ELF_CLASS_32	1
#define ELF_CLASS_64	2

#define ELF_MACH_NONE	0
#define ELF_MACH_386	3

#define ELF_VER_CURRENT	1

#define SHT_NULL		0
#define SHT_PROGBITS	1
#define SHT_SYMTAB		2
#define SHT_STRTAB		3
#define SHT_RELA		4
#define SHT_HASH		5
#define SHT_DYNAMIC		6
#define SHT_NOTE		7
#define SHT_NOBITS		8
#define SHT_REL			9
#define SHT_SHLIB		10
#define SHT_DYNSYM		11
#define SHT_LOPROC		0x70000000
#define SHT_HIPROC		0x7FFFFFFF
#define SHT_LOUSER		0x80000000
#define SHT_HIUSER		0xFFFFFFFF

#define EI_NIDENT		16

typedef struct {
	uint8_t		e_ident[EI_NIDENT];	/* Magic number and other info */
	Elf32_Half	e_type;				/* Object file type */
	Elf32_Half	e_machine;			/* Architecture */
	Elf32_Word	e_version;			/* Object file version */
	Elf32_Addr	e_entry;			/* Entry point virtual address */
	Elf32_Off	e_phoff;			/* Program header table file offset */
	Elf32_Off	e_shoff;			/* Section header table file offset */
	Elf32_Word	e_flags;			/* Processor-specific flags */
	Elf32_Half	e_ehsize;			/* ELF header size in bytes */
	Elf32_Half	e_phentsize;		/* Program header table entry size */
	Elf32_Half	e_phnum;			/* Program header table entry count */
	Elf32_Half	e_shentsize;		/* Section header table entry size */
	Elf32_Half	e_shnum;			/* Section header table entry count */
	Elf32_Half	e_shstrndx;			/* Section header string table index */
} elf_header_t;

typedef struct {
	Elf32_Word	p_type;
	Elf32_Off	p_offset;
	Elf32_Addr	p_vaddr;
	Elf32_Addr	p_paddr;
	Elf32_Word	p_filesz;
	Elf32_Word	p_memsz;
	Elf32_Word	p_flags;
	Elf32_Word	p_align;
} elf_program_header_t;

typedef struct {
	uint32_t sh_name;
	uint32_t sh_type;
	uint32_t sh_flags;
	uint32_t sh_addr;
	uint32_t sh_offset;
	uint32_t sh_size;
	uint32_t sh_link;
	uint32_t sh_info;
	uint32_t sh_addralign;
	uint32_t sh_entsize;
} elf_section_header_t;

uint8_t elfloader_probe(uint8_t *buf);
uint8_t elfloader_start(uint8_t *buf, int argc, char **argv);
void elfloader_init();

#endif