#ifndef __IRQ_H
#define __IRQ_H

void irq_install();
void irq_handler(registers_t *r);
void interrupt_install_handler(uint8_t num, void (*handler)(registers_t *r));
void interrupt_uninstall_handler(uint8_t num);

#endif