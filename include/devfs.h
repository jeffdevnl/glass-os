#ifndef __DEVFS_C
#define __DEVFS_C

typedef struct {
	char *name;
	fs_node_t *root;
} device_fsnode_t;

fs_node_t *devfs_init();
void devfs_add(char *name, fs_node_t *root);

#endif