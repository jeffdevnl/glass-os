#ifndef __INITRD_H
#define __INITRD_H

typedef struct {
	uint16_t magic;
	char volume_name[64];
	uint32_t num_tables;
	uint32_t num_index_entries;
} initrd_header_t;

typedef struct {
	uint32_t id;
	uint32_t offset;
} initrd_dir_index_t;

typedef struct initrd_dir_entry {
	char name[255];
	uint32_t parentId;
	uint8_t flags;
	uint32_t offset;
	uint32_t size;
} initrd_dir_entry_t;

fs_node_t *initrd_init(uint32_t location);
void initrd_info();

#endif