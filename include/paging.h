#ifndef __PAGING_H
#define __PAGING_H

typedef struct page {
   uint32_t present    : 1;   // Page present in memory
   uint32_t rw         : 1;   // Read-only if clear, readwrite if set
   uint32_t user       : 1;   // Supervisor level only if clear
   uint32_t accessed   : 1;   // Has the page been accessed since last refresh?
   uint32_t dirty      : 1;   // Has the page been written to since last refresh?
   uint32_t unused     : 7;   // Unused and reserved bits
   uint32_t frame      : 20;  // Frame address (shifted right 12 bits)
} page_t;

typedef struct page_table {
   page_t pages[1024];
} page_table_t;

typedef struct page_directory {
   page_table_t *tables[1024];
   uint32_t tables_physical[1024];
   uint32_t physical_address;
} page_directory_t;

void paging_free_directory(page_directory_t *dir);
void paging_copy_page_phys(uint32_t src, uint32_t dest);
void paging_init(uint32_t mem);
void paging_alloc_frame(page_t *page, uint8_t kernel, uint8_t writable);
uint32_t paging_first_frame();
void paging_free_frame(page_t *page);
void switch_page_directory(page_directory_t *new);
page_t *paging_get_page(uint32_t address, uint8_t make, page_directory_t *dir);
page_directory_t *paging_clone_directory(page_directory_t *dir);
void paging_switch_directory(page_directory_t *dir);
void paging_pagefault(registers_t *regs);
uint32_t memory_used();
uint32_t memory_total();
void paging_direct_mem_access(page_t* page, uint8_t kernel, uint8_t writable, uint32_t address);
void paging_map(uint32_t phys, uint32_t virt, uint8_t kernel, uint8_t writable, page_directory_t *dir);
uint32_t paging_get_phys_from_virt(uint32_t virt);
void paging_enable();
void paging_disable();
void paging_invalidate(uint32_t addr);

#endif