#ifndef __GDT_H
#define __GDT_H

// Macros for settings flags
#define GDT_DESCRIPTOR_TYPE(a)          (a << 0x04) // Descriptor type, 0 = system, 1 = code / data
#define GDT_PRIVILEGE(a)                (a << 0x05) // Privilege level (ring)
#define GDT_PRESENT(a)                  (a << 0x07) // If the entry is present
#define GDT_AVAILABLE(a)                (a << 0x0C) // If the entry is available for system use
#define GDT_SIZE(a)                     (a << 0x06) // The size, 0 = 16bit, 1 = 32bit
#define GDT_GRANULARITY(a)              (a << 0x07) // Granularity, 0 = 1B, 1 = 4KB

// Defined constants
#define GDT_DATA_R                      0x00        // Read only
#define GDT_DATA_RA                     0x01        // Read only, accessed
#define GDT_DATA_RW                     0x02        // Read, write
#define GDT_DATA_RWA                    0x03        // Read, write, accessed
#define GDT_DATA_RED                    0x04        // Read only, expand down
#define GDT_DATA_REDA                   0x05        // Read only, expand down, accessed
#define GDT_DATA_RWED                   0x06        // Read, write, expand down
#define GDT_DATA_RWEDA                  0x07        // Read, write, expand down, accessed
#define GDT_DATA_E                      0x08        // Execute only
#define GDT_DATA_EA                     0x09        // Execute, accessed
#define GDT_DATA_ER                     0x0A        // Execute, read
#define GDT_DATA_ERA                    0x0B        // Execute, read, accessed
#define GDT_DATA_EC                     0x0C        // Execute, conforming
#define GDT_DATA_ECA                    0x0D        // Execute, conforming, accessed
#define GDT_DATA_ERC                    0x0E        // Execute, read, conforming
#define GDT_DATA_ERCA                   0x0F        // Execute, read, conforming, accessed

extern void gdt_flush(uintptr_t gdt_pointer);

void gdt_install();
void set_kernel_stack(uintptr_t stack);

#endif