#ifndef __TASK_H
#define __TASK_H

#define INITIAL_STACK_SIZE		4096
#define INITIAL_FD_CAPACITY		12

typedef struct {
	uint32_t			used;
	uint32_t			capacity;
	fs_node_t			**entries;
	uint32_t			*entry_offsets;
} fd_table_t;

typedef struct {
	char				*name;
	uint32_t			pid;
	uint32_t			parent_pid;

	uint32_t			stack;
	uint32_t			initial_stack;
	page_directory_t	*page_directory;

	fd_table_t			*fd;
	fs_node_t			*wd_node;
	char				*wd_name;

	uint32_t			*to_free;
	uint32_t			free_count;

	uint8_t remove;
} task_t;

extern uint32_t get_esp();

void tasking_init();
uint32_t tasking_fork(char *name);
uint32_t tasking_get_pid();
uint8_t tasking_exit(uint32_t pid);
uint32_t tasking_switch_task(uint32_t stack);
uint32_t tasking_start(char *name, void *pointer, uint8_t usermode);
uint8_t tasking_running(uint32_t pid);
void tasking_print();
task_t *get_task_by_pid(uint32_t pid);
task_t *get_current_task();
char *get_current_dir_name();
char *getwd(char *buf);
char *getcwd();
int chdir(char *path);
uint32_t tasking_add(task_t *task);
task_t *tasking_create(char *name);
void tasking_init_stack(task_t *task, void *pointer, uint8_t usermode);

#endif