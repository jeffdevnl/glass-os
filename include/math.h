#ifndef __MATH_H
#define __MATH_H

uint32_t rand();
void srand(uint32_t seed);

#define min(a, b)			((a < b) ? a : b)
#define max(a, b)			((a < b) ? b : a)
#define abs(a)				(a < 0 ? -a : a)

#endif