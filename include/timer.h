#ifndef __TIMER_H
#define __TIMER_H

uint32_t timer_handler(registers_t *r);
void timer_install();
void timer_wait(uint64_t ticks);
void timer_phase(int32_t hertz);

#endif