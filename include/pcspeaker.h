#ifndef __PCSPEAKER_H
#define __PCSPEAKER_H

void pcspeaker_play(uint32_t freq);
void pcspeaker_stop();

#endif