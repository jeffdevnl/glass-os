#ifndef __IDT_H
#define __IDT_H

void idt_install();
void idt_set_gate(uint8_t num, uint64_t base, uint8_t sel, uint8_t flags);

#endif