#ifndef __MEMORY_H
#define __MEMORY_H

void memcpy(void* dest, void* src, int count);
void memset(void* dest, const uint8_t val, int count);
void memsetw(void* dest, const uint16_t val, int count);
void memsetd(void *dest, const uint32_t val, int count);
int memcmp(void *a, void *b, int count);

#endif