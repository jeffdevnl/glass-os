#ifndef __MOUSE_H
#define __MOUSE_H

#define MOUSE_NONE		0x00
#define	MOUSE_LEFT		0x01
#define	MOUSE_RIGHT		0x02

#define MOUSE_PIPE_SIZE	250

typedef struct {
	int32_t x;
	int32_t y;
	uint8_t button;
} mouse_packet_t;

void mouse_install();
fs_node_t *mouse_pipe_init();
void mouse_handle();

#endif