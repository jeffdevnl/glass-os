#ifndef __ACPI_H
#define __ACPI_H

typedef struct rsdpdescr {
	char Signature[8];
	uint8_t Checksum;
	char OEMID[6];
	uint8_t Revision;
	uint32_t RsdtAddress;
} RSDP_t;

typedef struct ACPISDTHeader {
	char Signature[4];
	uint32_t Length;
	uint8_t Revision;
	uint8_t Checksum;
	char OEMID[6];
	char OEMTableID[8];
	uint32_t OEMRevision;
	uint32_t CreatorID;
	uint32_t CreatorRevision;
} rsdth_t;

typedef struct RSDT {
	rsdth_t header;
	uint32_t firstSDT;
} RSDT_t;

typedef struct GenericAddressStructur
{
	uint8_t AddressSpace;
	uint8_t BitWidth;
	uint8_t BitOffset;
	uint8_t AccessSize;
	uint64_t Address;
} GenericAddressStructure;

typedef struct FADT
{
	rsdth_t header;
	uint32_t FirmwareCtrl;
	uint32_t Dsdt;

	// compatibility with ACPI 1.0
	uint8_t  Reserved;

	uint8_t  PreferredPowerManagementProfile;
	uint16_t SCI_Interrupt;
	uint32_t SMI_CommandPort;
	uint8_t  AcpiEnable;
	uint8_t  AcpiDisable;
	uint8_t  S4BIOS_REQ;
	uint8_t  PSTATE_Control;
	uint32_t PM1aEventBlock;
	uint32_t PM1bEventBlock;
	uint32_t PM1aControlBlock;
	uint32_t PM1bControlBlock;
	uint32_t PM2ControlBlock;
	uint32_t PMTimerBlock;
	uint32_t GPE0Block;
	uint32_t GPE1Block;
	uint8_t  PM1EventLength;
	uint8_t  PM1ControlLength;
	uint8_t  PM2ControlLength;
	uint8_t  PMTimerLength;
	uint8_t  GPE0Length;
	uint8_t  GPE1Length;
	uint8_t  GPE1Base;
	uint8_t  CStateControl;
	uint16_t WorstC2Latency;
	uint16_t WorstC3Latency;
	uint16_t FlushSize;
	uint16_t FlushStride;
	uint8_t  DutyOffset;
	uint8_t  DutyWidth;
	uint8_t  DayAlarm;
	uint8_t  MonthAlarm;
	uint8_t  Century;

	// reserved in ACPI 1.0; used since ACPI 2.0+
	uint16_t BootArchitectureFlags;

	uint8_t  Reserved2;
	uint32_t Flags;

	// 12 byte structure; see below for details
	GenericAddressStructure ResetReg;

	uint8_t  ResetValue;
	uint8_t  Reserved3[3];

	// 64bit pointers - Available on ACPI 2.0+
	uint64_t X_FirmwareControl;
	uint64_t X_Dsdt;

	GenericAddressStructure X_PM1aEventBlock;
	GenericAddressStructure X_PM1bEventBlock;
	GenericAddressStructure X_PM1aControlBlock;
	GenericAddressStructure X_PM1bControlBlock;
	GenericAddressStructure X_PM2ControlBlock;
	GenericAddressStructure X_PMTimerBlock;
	GenericAddressStructure X_GPE0Block;
	GenericAddressStructure X_GPE1Block;
} FADT_t;

void acpi_init();
void acpi_power_off();
uint8_t acpi_check_sum(void *address, uint32_t length);
void *acpi_find_entry(char *id);
void acpi_enable();
void acpi_disable();
void acpi_shutdown();
void acpi_enable_paging();
void acpi_print();
void acpi_reset();

#endif