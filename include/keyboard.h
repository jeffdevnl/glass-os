#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#define KEYBOARD_PIPE_SIZE	256

void install_keyboard();
fs_node_t *keyboard_pipe_init();
uint32_t get_keys(char *buffer, uint32_t max);
char get_char();
char get_chard();

#endif