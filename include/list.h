#ifndef __LIST_H
#define __LIST_H

typedef struct node {
	struct node *previous;
	struct node *next;
	void *value;
} __attribute__((packed)) node_t;

typedef struct {
	node_t *begin;
	node_t *end;
	uint32_t length;
} __attribute__((packed)) list_t;

list_t *list_create();
void list_add_item(list_t *list, node_t *node);
node_t *list_find(list_t *list, void *value);
void list_delete_node(list_t *list, node_t *node);
void list_delete_item(list_t *list, uint32_t index);
void list_destroy(list_t *list);
node_t *list_get(list_t *list, uint32_t index);

#define foreach_in_list(i, list) 	for(node_t *i = list->begin; i != NULL; i = i->next)

#endif