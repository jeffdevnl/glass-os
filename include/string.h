#ifndef __STRING_H
#define __STRING_H

char toupper(char c);
char tolower(char c);
uint8_t strstr(char *in, char *cmp);
uint32_t strlen(const char *text);
char *get_string();
char *strcpy(char *s1, const char *s2);
char *strncpy(char *s1, const char *s2, uint32_t length);
int8_t strcmp(const char *a, const char *b);
int8_t strncmp(const char *a, const char *b, uint32_t length);
uint32_t str_count(char *in, char cmp);
char *strtok(char *str, const char delim);
char *strdup(const char *s);
uint32_t vssprintf(char *str, const char *format, va_list args);
uint32_t sprintf(char *str, const char *format, ...);


#endif