#ifndef __STACK_H
#define __STACK_H

typedef struct stack_node {
	struct stack_node *next;
	void *value;
} stack_node_t;

typedef struct {
	stack_node_t *top;
	uint32_t length;
} stack_t;


stack_t *stack_create();
void stack_push(stack_t *stack, void *value);
uint32_t stack_size(stack_t *stack);
void *stack_pop(stack_t *stack);
int stack_is_empty(stack_t *stack);
void stack_swap(stack_t *x, stack_t *y);
void stack_destroy(stack_t *stack);

#endif