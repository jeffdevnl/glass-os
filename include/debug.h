#ifndef __DEBUG_H
#define __DEBUG_H

#define	DEBUG_NO		0
#define	DEBUG_SERIAL	1
#define	DEBUG_SCREEN	2

#define DEBUG_INFO		0
#define DEBUG_NOTICE	1
#define DEBUG_WARNING	2
#define DEBUG_ERROR		3

void debug_set_mode(int mode);
void debug_set_serial_port(int port); 
void debug_print(int level, char *string, ...); 

#endif