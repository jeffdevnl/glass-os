#ifndef __TREE_H
#define __TREE_H

typedef struct tree_node {
	list_t *children;
	struct tree_node *parent;
	void *value;
} tree_node_t;

typedef struct tree {
	uint32_t size;
	tree_node_t *root;
} tree_t;


tree_t *tree_create(); 
void tree_set_root(tree_t *tree, void *value);
void tree_add_child(tree_t *tree, tree_node_t *node, void *value);
void tree_node_destroy(tree_node_t *node);
void tree_destroy(tree_t *tree);

#endif