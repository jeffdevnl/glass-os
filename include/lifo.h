#ifndef __LIFO_H
#define __LIFO_H

typedef struct lifo_item {
	void *value;
	struct lifo_item *next; 
} lifo_item_t;


typedef struct lifo {
	lifo_item_t *next;
	lifo_item_t *last;
} lifo_t;


lifo_t *lifo_create();
void *lifo_pull(lifo_t *lifo);
void lifo_push(void *value, lifo_t *lifo);


#endif
