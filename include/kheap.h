#ifndef __KHEAP_H
#define __KHEAP_H

uint32_t malloc(int size);
uint32_t malloc_a(int size);
uint32_t malloc_p(int size, uint32_t *physical);
uint32_t malloc_ap(int size, uint32_t *physical);
void free(void *ptr);

void sbrk(uint32_t increment);
void heap_install(uint32_t address);

typedef struct memory_block {
	uint8_t used : 1;
	uint32_t address;
	uint32_t size;
} memory_block_t;

#endif