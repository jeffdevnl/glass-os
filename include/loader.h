#ifndef __LOADER_H
#define __LOADER_H

#define LOADER_MAX 100

typedef struct {
	char *name;
	uint8_t (*probe)(uint8_t *buf);
	uint8_t (*start)(uint8_t *buf, int argc, char **argv);
} loader_t;

void loader_init();
void loader_num();
void loader_register(loader_t *loader);
uint8_t loader_exec(uint8_t *buffer, int argc, char **argv);

#endif