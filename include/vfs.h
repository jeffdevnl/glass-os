#ifndef __VFS_C
#define __VFS_C

typedef struct {
	char *name;
	fs_node_t *root;
} mount_t;

void vfs_init();
void vfs_mount(char *path, fs_node_t *root);
void vfs_unmount(char *path);

#endif