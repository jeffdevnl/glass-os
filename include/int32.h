#ifndef __INT32_H
#define __INT32_H

typedef struct {
	unsigned short di, si, bp, sp, bx, dx, cx, ax;
	unsigned short gs, fs, es, ds, eflags;
} __attribute__((packed)) regs16_t;

void int32(unsigned char intnum, regs16_t *regs);

#endif