#ifndef __SCREEN_H
#define __SCREEN_H

#define CONSOLE_BLACK			0x00
#define CONSOLE_BLUE			0x01
#define CONSOLE_GREEN			0x02
#define CONSOLE_CYAN			0x03
#define CONSOLE_RED				0x04
#define CONSOLE_MAGENTA			0x05
#define CONSOLE_BROWN			0x06
#define CONSOLE_LIGHTGRAY		0x07
#define CONSOLE_DARKGRAY		0x08
#define CONSOLE_LIGHTBLUE		0x09
#define CONSOLE_LIGHTGREEN		0x0A
#define CONSOLE_LIGHTCYAN		0x0B
#define CONSOLE_LIGHTRED		0x0C
#define CONSOLE_LIGHTMAGENTA	0x0D
#define CONSOLE_YELLOW			0x0E
#define CONSOLE_WHITE			0x0F

void putch(char c);
void screen_clear();
void screen_write(char *c);
void screen_write_hex(uint32_t val);
void screen_write_dec(uint32_t val);
void screen_writeline(char *text);
void kprintf(const char *str, ...);
void screen_change_color(uint8_t fg, uint8_t bg);
void screen_move_cursor_to(uint16_t x, uint16_t y);
uint16_t screen_get_x();
uint16_t screen_get_y();
void screen_writel(char *src, uint32_t len);
fs_node_t *stdout_fs_init();

#endif 