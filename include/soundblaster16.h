#ifndef __SOUNDBLASTER16_H
#define __SOUNDBLASTER16_H

#define SB16_DSPReset		2x6h
#define SB16_DSPWrite 		2xCh
#define SB16_DSPWriteStatus	2xCh
#define SB16_DSPReadStatus	2xEh 
#define SB16_DSPint			2xFh 
#define SB16_DSPREADY     	0xAA
#define SB16_DSPVersion    	0xE1

void SB16_setup();
void SB16_DSPWrite(uint8_t value);
uint8_t SB16_read();
void SB16_DSPreset();
void SB16_DSPAvailable();
void SB16_DSPWait();

#endif
