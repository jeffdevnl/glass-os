#ifndef __FIFO_H
#define __FIFO_H

typedef struct {
     char *buffer;

     int head;
     int tail;
     int size;

     uint8_t wait;
} fifo_t;

fifo_t *fifo_create(uint32_t size, uint8_t wait);
uint32_t fifo_read(fifo_t *f, void *buf, uint32_t size);
uint32_t fifo_write_byte(fifo_t *f, char byte);
uint32_t fifo_write(fifo_t *f, const void *buf, uint32_t size);
void fifo_destroy(fifo_t *fifo);

#endif