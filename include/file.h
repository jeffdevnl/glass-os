#ifndef __FILE_H
#define __FILE_H

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2

typedef struct file {
	uint32_t offset;
	fs_node_t *node;
} FILE;

char *get_path_absolute(char *basedir, char *filename);
char *get_path(char *filename);
FILE *fopen(const char *filename);
fs_node_t *fs_node_open(const char *path);
int fread(void *ptr, int size, FILE *stream);
int fwrite(void *ptr, int size, FILE *stream);
int fseek(FILE *stream, int offset, int origin);
int ftell(FILE *stream);
void fclose(FILE *file);

#endif