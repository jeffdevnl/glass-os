#ifndef __SYSTEM_H
#define __SYSTEM_H

#include <stdint.h>

typedef struct {
    uint32_t gs, fs, es, ds;     
    uint32_t edi, esi, ebp, useless, ebx, edx, ecx, eax; 
    uint32_t int_no, err_code; 
    uint32_t eip, cs, eflags, esp, ss;  
} registers_t;

#define NULL				(void *) 0
typedef int size_t;

#define BDC_TO_DECIMAL(n)	(n & 0x0F) | ((n / 16) * 10)

typedef __builtin_va_list	va_list;
#define va_start(ap, last)	__builtin_va_start(ap, last)
#define va_end(ap)			__builtin_va_end(ap)
#define	va_arg(ap, type)	__builtin_va_arg(ap, type)
#define	va_copy(dest, src)	__builtin_va_copy(dest, src)

#include <errno_base.h>

#include <cpuid.h>

#include <gdt.h>
#include <idt.h>
#include <isr.h>
#include <irq.h>
#include <portio.h>
#include <timer.h>
#include <cmos.h>
#include <panic.h>

#include <multiboot.h>
#include <gpf.h>
#include <int32.h>

#include <memory.h>
#include <kheap.h>
#include <paging.h>

#include <vesa.h>

#include <pcspeaker.h>

#include <fdc.h>

#include <acpi.h>

#include <loader.h>
#include <elf_loader.h>

#include <fs.h>
#include <dirent.h>
#include <file.h>
#include <vfs.h>
#include <devfs.h>
#include <nullfs.h>
#include <procfs.h>
#include <initrd.h>

#include <keyboard.h>
#include <mouse.h>
#include <screen.h>
#include <serialport.h>

#include <syscalls.h>
#include <gsyscalls.h>
#include <task.h>

#include <math.h>
#include <stdio.h>
#include <list.h>
#include <string.h>
#include <sha256.h>
#include <tree.h>
#include <stack.h>
#include <fifo.h>
#include <lifo.h>

#include <shell.h>

#include <debug.h>


// Defined in main.c
void print_multiboot_header();

#endif
