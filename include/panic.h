#ifndef __PANIC_H
#define __PANIC_H

void assert_failure(char *file, uint32_t line, char *description);
void panic(char *file, uint32_t line, char *description);

#define ASSERT(check, desc)	(check ? (void)0 : assert_failure(__FILE__, __LINE__, desc))
#define PANIC(desc) 		(panic(__FILE__, __LINE__, desc))

#endif